<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

include_once("Data/Company_Save_Additional/dataCompanySaveAdditional.php");
include_once("Data/URL/dataCreateApiUrl.php");
include_once("Function/commonFunction/commonFunction.php");
include_once("src/Company_Logo/companyLogo.php");

class companySaveAdditional
{
    protected $apiEndpoint = "web/build_company/save_additional";
    protected $dataCreateApiUrl;
    protected $dataCompanySaveAdditional;
    protected $commonFunction;
    protected $companyLogo;
    protected $postFieldsForEnterOnlyRequiredData;
    protected $postFieldsForValidData;
    protected $postFieldsForBlankAllData;

    function __construct()
    {
        $this->dataCreateApiUrl = new dataCreateApiUrl();
        $this->dataCompanySaveAdditional = new dataCompanySaveAdditional();
        $this->commonFunction = new commonFunction();
        $this->companyLogo = new companyLogo();
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForBlankAllData()
    {
        $this->postFieldsForBlankAllData = "";
        return $this->postFieldsForBlankAllData;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForValidData()
    {
        $this->postFieldsForValidData = "additional_public_information=" . $this->dataCompanySaveAdditional->getAdditionalPublicInformationValidData() . "&address=" . $this->dataCompanySaveAdditional->getAddressValidData() . "&state=" . $this->dataCompanySaveAdditional->getStateValidData() . "&city=" . $this->dataCompanySaveAdditional->getCityValidData() . "&zip_code=" . $this->dataCompanySaveAdditional->getZipCodeValidData() . "&country=" . $this->dataCompanySaveAdditional->getCountryValidData() . "&timezone=" . $this->dataCompanySaveAdditional->getTimezoneValidData() . "&headquater_country=" . $this->dataCompanySaveAdditional->getHeadquaterCountryValidData() . "&jurisdiction_incorporation=" . $this->dataCompanySaveAdditional->getJurisdictionIncorporationValidData() . "&phone_number=" . $this->dataCompanySaveAdditional->getPhoneNumberValidData() . "&company_email=" . $this->dataCompanySaveAdditional->getCompanyEmailValidData() . "&website_url=" . $this->dataCompanySaveAdditional->getWebsiteURLValidData() . "&date[]=" . $this->dataCompanySaveAdditional->getDateValidData() . "&event[]=" . $this->dataCompanySaveAdditional->getEventValidData() . "&press_articles[]=" . $this->dataCompanySaveAdditional->getPressAticlesValidData() . "&authorized_behalf_company=" . $this->dataCompanySaveAdditional->getAuthorizedBehalfCompanyValidData() . "&role_in_company=" . $this->dataCompanySaveAdditional->getRoleInCompanyValidData() . "&legal_name=" . $this->dataCompanySaveAdditional->getLegalNameValidData() . "&currency_code_id=" . $this->dataCompanySaveAdditional->getCurrencyCodeIDValidData() . "&company_facebook_url=" . $this->dataCompanySaveAdditional->getCompanyFacebookURLValidData() . "&company_twitter_url=" . $this->dataCompanySaveAdditional->getCompanyTwitterURLValidData() . "&company_linkedin_url=" . $this->dataCompanySaveAdditional->getCompanyLinkedinURLValidData() . "&company_instagram_url=" . $this->dataCompanySaveAdditional->getCompanyInstagramURLValidData() . "&google_analytic_code=" . $this->dataCompanySaveAdditional->getGoogleAnalyticCodeValidData() . "&facebook_pixel_id=" . $this->dataCompanySaveAdditional->getFacebookPixelIDValidData() . "&video_url=" . $this->dataCompanySaveAdditional->getVideoURLValidData();
        return $this->postFieldsForValidData;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForEnterOnlyRequiredData()
    {

        return $this->postFieldsForEnterOnlyRequiredData = "additional_public_information=" . $this->dataCompanySaveAdditional->getAdditionalPublicInformationValidData() . "&address=" . $this->dataCompanySaveAdditional->getAddressValidData() . "&state=" . $this->dataCompanySaveAdditional->getStateValidData() . "&city=" . $this->dataCompanySaveAdditional->getCityValidData() . "&zip_code=" . $this->dataCompanySaveAdditional->getZipCodeValidData() . "&country=" . $this->dataCompanySaveAdditional->getCountryValidData() . "&timezone=" . $this->dataCompanySaveAdditional->getTimezoneValidData() . "&headquater_country=" . $this->dataCompanySaveAdditional->getHeadquaterCountryValidData() . "&jurisdiction_incorporation=" . $this->dataCompanySaveAdditional->getJurisdictionIncorporationValidData() . "&phone_number=" . $this->dataCompanySaveAdditional->getPhoneNumberValidData() . "&company_email=" . $this->dataCompanySaveAdditional->getCompanyEmailValidData() . "&website_url=" . $this->dataCompanySaveAdditional->getWebsiteURLValidData() . "&date[]=" . $this->dataCompanySaveAdditional->getDateValidData() . "&event[]=" . $this->dataCompanySaveAdditional->getEventValidData() . "&press_articles[]=" . $this->dataCompanySaveAdditional->getPressAticlesValidData() . "&authorized_behalf_company=" . $this->dataCompanySaveAdditional->getAuthorizedBehalfCompanyValidData() . "&role_in_company=" . $this->dataCompanySaveAdditional->getRoleInCompanyValidData() . "&legal_name=" . $this->dataCompanySaveAdditional->getLegalNameValidData()."&currency_code_id=" . $this->dataCompanySaveAdditional->getCurrencyCodeIDValidData();
    }

    public function getRequestOptions($token, $PostFields)
    {
        return $this->_requestOptions = array(
            CURLOPT_URL => $this->getCompanySaveAdditionalAPIUrl(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $PostFields,
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded",
                "www-token: " . $token . ""
            ),
        );
    }

    /**
     * @return string
     */
    public function getCompanySaveAdditionalAPIUrl()
    {
        $this->_liveFrontUserLoginAPIUrl = $this->dataCreateApiUrl->getDomainLiveAPIURL() . $this->getApiEndpoint();
        return $this->_liveFrontUserLoginAPIUrl;
    }

    /**
     * @return string
     */
    public function getApiEndpoint()
    {
        return $this->apiEndpoint;
    }

    public function callCompanyLogoAPI()
    {
        $file = $this->dataCompanySaveAdditional->getValidImagePath();
        $token = $this->commonFunction->getLoginToken($this->dataCompanySaveAdditional->getUsername(), $this->dataCompanySaveAdditional->getPassword());
        $this->companyLogo->getRequestOptions($file, $token);
    }
}