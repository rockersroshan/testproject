<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

include_once("Data/URL/dataCreateApiUrl.php");
include_once("Data/Company_Save_Financial/dataCompanySaveFinancial.php");
include_once("src/Company_Financial_Statement/companyFinancialStatement.php");
include_once("Function/callAPI/postAPI.php");

class companySaveFinancial
{
    protected $apiEndpoint = "web/build_company/save_financial";
    protected $dataCreateApiUrl;
    protected $dataCompanySaveFinancial;
    protected $companyFinancialStatement;
    protected $postAPI;
    protected $postFieldsForValidData;
    protected $postFieldsForBlankAllData;

    function __construct()
    {
        $this->dataCreateApiUrl = new dataCreateApiUrl();
        $this->dataCompanySaveFinancial = new dataCompanySaveFinancial();
        $this->companyFinancialStatement = new companyFinancialStatement();
        $this->postAPI = new postAPI();
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForBlankAllData()
    {
        return $this->postFieldsForBlankAllData = "";
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForValidData()
    {
        return $this->postFieldsForValidData = array(
            "financial_public_information" => $this->dataCompanySaveFinancial->getFinancialPublicInformationValidData(),
            "sales_last_year" => $this->dataCompanySaveFinancial->getSalesLastYearValidData(),
            "tax_relief_type" => $this->dataCompanySaveFinancial->getTaxReliefTypeValidData(),
            "financial_review_end_date" => $this->dataCompanySaveFinancial->getFinancialReviewEndDateValidData(),
            "results_of_operation" => $this->dataCompanySaveFinancial->getResultsOfOperationValidData(),
            "financial_milestone" => $this->dataCompanySaveFinancial->getFinancialMilestoneValidData(),
            "liquidity_and_capital_resource" => $this->dataCompanySaveFinancial->getLiquidityAndCapitalResourceValidData(),
            "related_party_transaction" => $this->dataCompanySaveFinancial->getRelatedPartyTransactionValidData(),
            "idebtedness" => $this->dataCompanySaveFinancial->getIdebtednessValidData(),
            "use_of_proceed" => $this->dataCompanySaveFinancial->getUseOfProceedValidData(),
            "current_total_assets" => $this->dataCompanySaveFinancial->getCurrentTotalAssetsValidData(),
            "current_cash_and_cash_equivalents" => $this->dataCompanySaveFinancial->getCurrentCashAndCashEquivalentsValidData(),
            "current_accounts_receivable" => $this->dataCompanySaveFinancial->getCurrentAccountsReceivableValidData(),
            "current_short_term_debt" => $this->dataCompanySaveFinancial->getCurrentShortTermDebtValidData(),
            "current_long_term_debt" => $this->dataCompanySaveFinancial->getCurrentLongTermDebtValidData(),
            "current_revenues_sales" => $this->dataCompanySaveFinancial->getCurrentRevenuesSalesValidData(),
            "current_cost_of_goods_sold" => $this->dataCompanySaveFinancial->getCurrentCostOfGoodsSoldValidData(),
            "current_taxes_paid" => $this->dataCompanySaveFinancial->getCurrentTaxesPaidValidData(),
            "current_net_income" => $this->dataCompanySaveFinancial->getCurrentNetIncomeValidData(),
            "pre_total_assets" => $this->dataCompanySaveFinancial->getPreTotalAssetsValidData(),
            "pre_cash_and_cash_equivalents" => $this->dataCompanySaveFinancial->getPreCashAndCashEquivalentsValidData(),
            "pre_accounts_receivable" => $this->dataCompanySaveFinancial->getPreAccountsReceivableValidData(),
            "pre_short_term_debt" => $this->dataCompanySaveFinancial->getPreShortTermDebtValidData(),
            "pre_long_term_debt" => $this->dataCompanySaveFinancial->getPreLongTermDebtValidData(),
            "pre_revenues_sales" => $this->dataCompanySaveFinancial->getPreRevenuesSalesValidData(),
            "pre_cost_of_goods_sold" => $this->dataCompanySaveFinancial->getPreCostOfGoodsSoldValidData(),
            "pre_taxes_paid" => $this->dataCompanySaveFinancial->getPreTaxesPaidValidData(),
            "pre_net_income" => $this->dataCompanySaveFinancial->getPreNetIncomeValidData());
    }

    public function getRequestOptions($token, $PostFields)
    {
        return $this->_requestOptions = array(
            CURLOPT_URL => $this->getCompanySaveFinancialAPIUrl(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $PostFields,
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: multipart/form-data",
                "www-token: " . $token . ""
            ),
        );
    }

    /**
     * @return string
     */
    public function getCompanySaveFinancialAPIUrl()
    {
        return $this->_liveFrontUserLoginAPIUrl = $this->dataCreateApiUrl->getDomainLiveAPIURL() . $this->getApiEndpoint();
    }

    /**
     * @return string
     */
    public function getApiEndpoint()
    {
        return $this->apiEndpoint;
    }

    public function callCompanyFinancialStatement($token)
    {
        $postFields = $this->companyFinancialStatement->getPostFieldsForValidData();
        $requestOptions = $this->companyFinancialStatement->getRequestOptions($token, $postFields);
        $this->postAPI->callPostAPI($requestOptions);
    }
}