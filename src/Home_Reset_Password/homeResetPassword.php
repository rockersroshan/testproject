<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

include_once("Function/callAPI/postAPI.php");
include_once("Data/URL/dataCreateApiUrl.php");
include_once("src/Home_ForgotPassword/homeForgotPassword.php");
include_once("Data/Home_Reset_Password/dataHomeResetPassword.php");

class homeResetPassword
{
    protected $postAPI;

    protected $dataCreateApiUrl;

    protected $homeForgotPassword;

    protected $apiEndpointForGetIDAPI = "home/reset_password_token";

    protected $apiEndpointForResetPassword = "home/reset_password";

    protected $postFieldsForForgotPasswordID;

    protected $dataHomeResetPassword;

    protected $forgotPasswordEmailInID;

    protected $postFieldsForValidData;

    protected $postFieldsForPasswordAndCPasswordNotSame;

    protected $postFieldsForPasswordMinRange;

    protected $postFieldsForPasswordMaxRange;

    protected $postFieldsForPasswordBlank;

    protected $postFieldsForConfirmPasswordBlank;

    protected $postFieldsForCodeBlank;

    protected $postFieldsForOnlyNumberAndCapital;

    protected $postFieldsOnlySmallAndSpecial;

    function __construct()
    {
        $this->postAPI = new postAPI();
        $this->dataCreateApiUrl = new dataCreateApiUrl();
        $this->homeForgotPassword = new homeForgotPassword();
        $this->dataHomeResetPassword = new dataHomeResetPassword();
    }

    /**
     * @return mixed
     */
    public function getPostFieldsOnlySmallAndSpecial($id)
    {
        $this->postFieldsOnlySmallAndSpecial = "password=" . $this->dataHomeResetPassword->getOnlySmallAndSpecialPassword() . "&cpassword=" . $this->dataHomeResetPassword->getOnlySmallAndSpecialPassword() . "&code=" . $id;
        return $this->postFieldsOnlySmallAndSpecial;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForOnlyNumberAndCapital($id)
    {
        $this->postFieldsForOnlyNumberAndCapital = "password=" . $this->dataHomeResetPassword->getOnlyNumberAndCapitalPassword() . "&cpassword=" . $this->dataHomeResetPassword->getOnlyNumberAndCapitalPassword() . "&code=" . $id;
        return $this->postFieldsForOnlyNumberAndCapital;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForCodeBlank()
    {
        $this->postFieldsForCodeBlank = "password=" . $this->dataHomeResetPassword->getMaxRangePassword() . "&cpassword=" . $this->dataHomeResetPassword->getMaxRangePassword();
        return $this->postFieldsForCodeBlank;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForConfirmPasswordBlank($id)
    {
        $this->postFieldsForConfirmPasswordBlank = "password=" . $this->dataHomeResetPassword->getValidConfirmPassword() . "&code=" . $id;
        return $this->postFieldsForConfirmPasswordBlank;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForPasswordBlank($id)
    {
        $this->postFieldsForPasswordBlank = "cpassword=" . $this->dataHomeResetPassword->getValidConfirmPassword() . "&code=" . $id;
        return $this->postFieldsForPasswordBlank;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForPasswordMaxRange($id)
    {
        $this->postFieldsForPasswordMaxRange = "password=" . $this->dataHomeResetPassword->getMaxRangePassword() . "&cpassword=" . $this->dataHomeResetPassword->getMaxRangePassword() . "&code=" . $id;
        return $this->postFieldsForPasswordMaxRange;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForPasswordMinRange($id)
    {
        $this->postFieldsForPasswordMinRange = "password=" . $this->dataHomeResetPassword->getMinRangePassword() . "&cpassword=" . $this->dataHomeResetPassword->getMinRangePassword() . "&code=" . $id;
        return $this->postFieldsForPasswordMinRange;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForPasswordAndCPasswordNotSame($id)
    {
        $this->postFieldsForPasswordAndCPasswordNotSame = "password=" . $this->dataHomeResetPassword->getValidPassword() . "&cpassword=" . $this->dataHomeResetPassword->getValidConfirmPassword() . "test&code=" . $id;
        return $this->postFieldsForPasswordAndCPasswordNotSame;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForValidData($id)
    {
        $this->postFieldsForValidData = "password=" . $this->dataHomeResetPassword->getValidPassword() . "&cpassword=" . $this->dataHomeResetPassword->getValidConfirmPassword() . "&code=" . $id;
        return $this->postFieldsForValidData;
    }

    /**
     * @return mixed
     */
    public function getForgotPasswordEmailInID()
    {
        $this->callHomeForgotPasswordAPI();

        $postFields = $this->getPostFieldsForForgotPasswordID();
        $requestOptions = $this->getRequestOptionsForID($postFields);
        $responseForForgotPasswordID = $this->postAPI->callPostAPI($requestOptions);

        $json_responseForForgotPasswordID = json_decode($responseForForgotPasswordID);

        $this->forgotPasswordEmailInID = $json_responseForForgotPasswordID->id;

        return $this->forgotPasswordEmailInID;
    }

    public function callHomeForgotPasswordAPI()
    {
        $postFields = $this->getPostFieldsForCallForgotPasswordAPI();
        $token = $this->dataCreateApiUrl->getClientpublickey();
        $requestOptions = $this->homeForgotPassword->getRequestOptions($postFields, $token);
        $this->postAPI->callPostAPI($requestOptions);
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForCallForgotPasswordAPI()
    {
        $this->postFieldsForValidEmail = "femail=" . $this->dataHomeResetPassword->getEmailAddress();
        return $this->postFieldsForValidEmail;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForForgotPasswordID()
    {
        $this->postFieldsForForgotPasswordID = "email=" . $this->dataHomeResetPassword->getEmailAddress() . "&xtoken=" . $this->dataHomeResetPassword->getXtokenForGetForgotPasswordID();
        return $this->postFieldsForForgotPasswordID;
    }

    public function getRequestOptionsForID($PostFields)
    {
        return $this->_requestOptions = array(
            CURLOPT_URL => $this->getLiveHomeForgotPasswordIDAPIUrl(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $PostFields,
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "www-token: " . $this->dataCreateApiUrl->getClientpublickey() . ""
            )
        );
    }

    /**
     * @return string
     */
    public function getLiveHomeForgotPasswordIDAPIUrl()
    {
        $this->_liveFrontUserLoginAPIUrl = $this->dataCreateApiUrl->getDomainLiveAPIURL() . $this->getApiEndpointForGetIDAPI();
        return $this->_liveFrontUserLoginAPIUrl;
    }

    /**
     * @return string
     */
    public function getApiEndpointForGetIDAPI()
    {
        return $this->apiEndpointForGetIDAPI;
    }

    public function getRequestOption($PostFields, $token)
    {
        return $this->_requestOptions = array(
            CURLOPT_URL => $this->getLiveHomeResetPasswordAPIUrl(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $PostFields,
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "www-token: " . $token . ""
            )
        );
    }

    /**
     * @return string
     */
    public function getLiveHomeResetPasswordAPIUrl()
    {
        $this->_liveFrontUserLoginAPIUrl = $this->dataCreateApiUrl->getDomainLiveAPIURL() . $this->getApiEndpointForResetPassword();
        return $this->_liveFrontUserLoginAPIUrl;
    }

    /**
     * @return string
     */
    public function getApiEndpointForResetPassword()
    {
        return $this->apiEndpointForResetPassword;
    }

}