<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

include_once("src/Signup_Register/signupRegister.php");
include_once("Function/callAPI/postAPI.php");
include_once("Data/URL/dataCreateApiUrl.php");
include_once("Data/Signup_Confirm_Account/dataSignupConfirmAccount.php");

class signupConfirmAccount
{
    protected $apiEndpointEmailVerificationToken = "home/email_verification_token";
    protected $apiEndpointSignupConfirmAccount = "signup/confirm_account/";
    protected $signupRegister;
    protected $postAPI;
    protected $dataCreateApiUrl;
    protected $dataSignupConfirmAccount;
    protected $getPostFieldsEmailVerificationTokenWithValidData;

    function __construct()
    {
        $this->signupRegister = new signupRegister();
        $this->postAPI = new postAPI();
        $this->dataCreateApiUrl = new dataCreateApiUrl();
        $this->dataSignupConfirmAccount = new dataSignupConfirmAccount();
    }

    public function getRequestOptionsForSignupConfirmAccount($token, $user_Id, $email_Token)
    {
        return $this->_requestOptions = array(
            CURLOPT_URL => $this->getSignupConfirmAccountAPIUrl($user_Id, $email_Token),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "www-token: " . $token . ""
            ),
        );
    }

    /**
     * @return string
     */
    public function getSignupConfirmAccountAPIUrl($user_Id, $email_Token)
    {
        $this->_liveFrontUserLoginAPIUrl = $this->dataCreateApiUrl->getDomainLiveAPIURL() . $this->getApiEndpointSignupConfirmAccount() . $user_Id . "/" . $email_Token;
        return $this->_liveFrontUserLoginAPIUrl;
    }

    /**
     * @return string
     */
    public function getApiEndpointSignupConfirmAccount()
    {
        return $this->apiEndpointSignupConfirmAccount;
    }

    public function getEmailVerificationToken($token,$email)
    {
        $requestOptions = $this->getRequestOptionsForEmailVerificationToken($token,$email);
        $response = $this->postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        return $json_response->id;
    }

    public function getRequestOptionsForEmailVerificationToken($token,$email)
    {
        return $this->_requestOptions = array(
            CURLOPT_URL => $this->getEmailVerificationTokenAPIUrl(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $this->getGetPostFieldsEmailVerificationTokenWithValidData($email),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: multipart/form-data",
                "www-token: " . $token . "",
            ),
        );
    }

    /**
     * @return string
     */
    public function getEmailVerificationTokenAPIUrl()
    {
        $this->_liveFrontUserLoginAPIUrl = $this->dataCreateApiUrl->getDomainLiveAPIURL() . $this->getApiEndpointEmailVerificationToken();
        return $this->_liveFrontUserLoginAPIUrl;
    }

    /**
     * @return string
     */
    public function getApiEndpointEmailVerificationToken()
    {
        return $this->apiEndpointEmailVerificationToken;
    }

    /**
     * @return mixed
     */
    public function getGetPostFieldsEmailVerificationTokenWithValidData($email)
    {
        return $this->getPostFieldsEmailVerificationTokenWithValidData = array("email" => $email,
            "xtoken" => "Aprty@9643MHKWQ75AA");
    }

    public function getCreatNewUserID($token,$email)
    {
        $postFields = $this->getPostFieldsForValidAllDataCreatNewUser($email);
        $requestOptions = $this->signupRegister->getRequestOptions($postFields, $token);
        $response = $this->postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        return $json_response->data->user_id;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForValidAllDataCreatNewUser($email)
    {
        $this->_postFieldsForValidAllData = "type=" . $this->dataSignupConfirmAccount->getValidType() . "&email=" . $email . "&password=" . $this->dataSignupConfirmAccount->getValidPassword() . "&cpassword=" . $this->dataSignupConfirmAccount->getValidCPassword();
        return $this->_postFieldsForValidAllData;
    }
}