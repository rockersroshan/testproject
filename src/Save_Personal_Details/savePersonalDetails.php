<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

include_once("Data/URL/dataCreateApiUrl.php");
include_once("Data/Save_Personal_Details/dataSavePersonalDetails.php");
include_once("Function/callAPI/postAPI.php");
include_once("src/Front_Login/frontLogin.php");

class savePersonalDetails
{
    protected $apiEndpoint = "web/invest_capital/save_personal_details";
    protected $dataCreateApiUrl;
    protected $postFieldsForValidData;
    protected $dataSavePersonalDetails;
    protected $postAPI;
    protected $frontLogin;
    protected $postFieldsForBlankAllNameAndAddressData;
    protected $postFieldsForBlankAllLocationData;
    protected $postFieldsForOptionalData;

    function __construct()
    {
        $this->dataCreateApiUrl = new dataCreateApiUrl();
        $this->dataSavePersonalDetails = new dataSavePersonalDetails();
        $this->postAPI = new postAPI();
        $this->frontLogin = new frontLogin();
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForOptionalData()
    {
        $this->postFieldsForOptionalData = "first_name=" . $this->dataSavePersonalDetails->getValidFirstName() . "&last_name=" . $this->dataSavePersonalDetails->getValidLastName() . "&address=" . $this->dataSavePersonalDetails->getValidAddress() . "&city=" . $this->dataSavePersonalDetails->getValidCity() . "&state=" . $this->dataSavePersonalDetails->getValidState() . "&country=" . $this->dataSavePersonalDetails->getValidCountry() . "&zip_code=" . $this->dataSavePersonalDetails->getValidZipCode() . "&timezone=" . $this->dataSavePersonalDetails->getValidTimezone();
        return $this->postFieldsForOptionalData;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForBlankAllLocationData()
    {
        $this->postFieldsForBlankAllLocationData = $this->postFieldsForValidData = "first_name=" . $this->dataSavePersonalDetails->getValidFirstName() . "&last_name=" . $this->dataSavePersonalDetails->getValidLastName() . "&address=" . $this->dataSavePersonalDetails->getValidAddress() . "&city=" . $this->dataSavePersonalDetails->getValidCity() . "&state=" . $this->dataSavePersonalDetails->getValidState() . "&country=" . $this->dataSavePersonalDetails->getValidCountry();
        return $this->postFieldsForBlankAllLocationData;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForBlankAllNameAndAddressData()
    {
        $this->postFieldsForBlankAllNameAndAddressData = "zip_code=" . $this->dataSavePersonalDetails->getValidZipCode() . "&timezone=" . $this->dataSavePersonalDetails->getValidTimezone() . "&bio=" . $this->dataSavePersonalDetails->getValidBio() . "&website=" . $this->dataSavePersonalDetails->getValidWebSite();
        return $this->postFieldsForBlankAllNameAndAddressData;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForValidData()
    {
        $this->postFieldsForValidData = "first_name=" . $this->dataSavePersonalDetails->getValidFirstName() . "&last_name=" . $this->dataSavePersonalDetails->getValidLastName() . "&address=" . $this->dataSavePersonalDetails->getValidAddress() . "&city=" . $this->dataSavePersonalDetails->getValidCity() . "&state=" . $this->dataSavePersonalDetails->getValidState() . "&country=" . $this->dataSavePersonalDetails->getValidCountry() . "&zip_code=" . $this->dataSavePersonalDetails->getValidZipCode() . "&timezone=" . $this->dataSavePersonalDetails->getValidTimezone() . "&bio=" . $this->dataSavePersonalDetails->getValidBio() . "&website=" . $this->dataSavePersonalDetails->getValidWebSite();
        return $this->postFieldsForValidData;
    }

    public function getRequestOptions($token, $PostFields)
    {
        return $this->_requestOptions = array(
            CURLOPT_URL => $this->getSavePersonalDetailsAPIUrl(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $PostFields,
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded",
                "www-token: " . $token . "",
            ),
        );
    }

    /**
     * @return string
     */
    public function getSavePersonalDetailsAPIUrl()
    {
        $this->_liveFrontUserLoginAPIUrl = $this->dataCreateApiUrl->getDomainLiveAPIURL() . $this->getApiEndpoint();
        return $this->_liveFrontUserLoginAPIUrl;
    }

    /**
     * @return string
     */
    public function getApiEndpoint()
    {
        return $this->apiEndpoint;
    }

    public function getLoginToken()
    {
        $postFields = $this->getLoginPostFields();
        $requestOptions = $this->frontLogin->getRequestOptions($postFields);
        $responce = $this->postAPI->callPostAPI($requestOptions);

        $json_responce = json_decode($responce);

        return $json_responce->data->token;
    }

    /**
     * @return string
     */
    public function getLoginPostFields()
    {
        $this->_postFields = "email=" . $this->dataSavePersonalDetails->getUsername() . "&password=" . $this->dataSavePersonalDetails->getPassword();
        return $this->_postFields;
    }

}