<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

include_once("Data/URL/dataCreateApiUrl.php");
include_once("src/Company_Save_Founder/companySaveFounder.php");
include_once("Function/callAPI/postAPI.php");
include_once("Data/Company_Save_People/dataCompanySavePeople.php");
include_once("src/Company_Save_Management_Team/companySaveManagementTeam.php");

class companySavePeople
{
    protected $apiEndpoint = "web/build_company/save_people";
    protected $dataCreateApiUrl;
    protected $companySaveFounder;
    protected $postAPI;
    protected $dataCompanySavePeople;
    protected $companySaveManagementTeam;
    protected $postFieldsForValidData;
    protected $postFieldsForBlankAllData;

    function __construct()
    {
        $this->dataCreateApiUrl = new dataCreateApiUrl();
        $this->companySaveFounder = new companySaveFounder();
        $this->postAPI = new postAPI();
        $this->dataCompanySavePeople = new dataCompanySavePeople();
        $this->companySaveManagementTeam = new companySaveManagementTeam();
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForBlankAllData()
    {
        $this->postFieldsForBlankAllData = "";
        return $this->postFieldsForBlankAllData;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForValidData()
    {
        $this->postFieldsForValidData = "business_story=" . $this->dataCompanySavePeople->getBusinessStoryValidData() . "&other_personnel_public_information=" . $this->dataCompanySavePeople->getOtherPersonnelPublicInformationValidData() . "&company_size=" . $this->dataCompanySavePeople->getCompanySizeValidData() . "&further_board_appointment=" . $this->dataCompanySavePeople->getFurtherBoardAppointmentValidData();
        return $this->postFieldsForValidData;
    }

    public function getRequestOptions($token, $PostFields)
    {
        return $this->_requestOptions = array(
            CURLOPT_URL => $this->getCompanySavePeopleAPIUrl(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $PostFields,
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded",
                "www-token: " . $token . ""
            ),
        );
    }

    /**
     * @return string
     */
    public function getCompanySavePeopleAPIUrl()
    {
        $this->_liveFrontUserLoginAPIUrl = $this->dataCreateApiUrl->getDomainLiveAPIURL() . $this->getApiEndpoint();
        return $this->_liveFrontUserLoginAPIUrl;
    }

    /**
     * @return string
     */
    public function getApiEndpoint()
    {
        return $this->apiEndpoint;
    }

    public function callSaveFounderAPI($token)
    {
        $postFields = $this->companySaveFounder->getPostFieldsForValidData();
        //$token = $this->commonFunction->getLoginToken($dataCompanySaveFounder->getUsername(), $dataCompanySaveFounder->getPassword());
        $requestOptions = $this->companySaveFounder->getRequestOptions($token, $postFields);
        $this->postAPI->callPostAPI($requestOptions);
    }

    public function callSaveManagementTeamAPI($token)
    {
        $postFields = $this->companySaveManagementTeam->getPostFieldsForValidData();
        //$token = $commonFunction->getLoginToken($dataCompanySaveManagementTeam->getUsername(), $dataCompanySaveManagementTeam->getPassword());
        $requestOptions = $this->companySaveManagementTeam->getRequestOptions($token, $postFields);
        $this->postAPI->callPostAPI($requestOptions);
    }

}