<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

include_once("Data/URL/dataCreateApiUrl.php");

class investSaveBuildRelationship
{
    protected $apiEndpoint = "web/invest_capital/save_build_relationship";
    protected $dataCreateApiUrl;
    protected $postFieldsForValidData;
    protected $dataInvestSaveBuildRelationship;
    protected $postFieldsForBlankAllData;
    protected $postFieldsForEnterAllValidData;

    /**
     * @return mixed
     */
    public function getPostFieldsForEnterAllValidData()
    {
        $this->postFieldsForEnterAllValidData = "date_of_birth=" . $this->dataInvestSaveBuildRelationship->getDateOfBirthValid() . "&home_town=" . $this->dataInvestSaveBuildRelationship->getHomeTownValid() . "&born_state=" . $this->dataInvestSaveBuildRelationship->getBornStateValid() . "&born_country=" . $this->dataInvestSaveBuildRelationship->getBornCountryValid() ."&birth_order=".$this->dataInvestSaveBuildRelationship->getBirthOrderValid()."&only_child=".$this->dataInvestSaveBuildRelationship->getOnlyChildValid()."&raised_by=".$this->dataInvestSaveBuildRelationship->getRaisedByValid()."&orphaned=".$this->dataInvestSaveBuildRelationship->getOrphaned()."&parental_substance_abuse=".$this->dataInvestSaveBuildRelationship->getParentalAubstanceAbuseValid()."&socio_economic_condition=".$this->dataInvestSaveBuildRelationship->getSocioEconomicConditionValid()."&predominant_environment=".$this->dataInvestSaveBuildRelationship->getPredominantEnvironmentValid()."&employment_youth=".$this->dataInvestSaveBuildRelationship->getEmploymentYouthValid()."&number_of_jobs_youth=".$this->dataInvestSaveBuildRelationship->getNumberOfJobsYouthValid()."&mentor_in_your_youth=".$this->dataInvestSaveBuildRelationship->getMentorInYourYouthValid()."&ethnic_background=".$this->dataInvestSaveBuildRelationship->getEthnicBackgroundValid()."&citizenship=".$this->dataInvestSaveBuildRelationship->getCitizenshipValid()."&gender=".$this->dataInvestSaveBuildRelationship->getGenderValid()."&sexual_orientation=".$this->dataInvestSaveBuildRelationship->getSexualOrientationValid()."&lgbt_supporter=".$this->dataInvestSaveBuildRelationship->getLgbtSupporterValid()."&educational=".$this->dataInvestSaveBuildRelationship->getEducationalValid()."&financial_aid=".$this->dataInvestSaveBuildRelationship->getFinancialAidValid()."&religious_identity=".$this->dataInvestSaveBuildRelationship->getReligiousIdentityValid()."&marital_status=".$this->dataInvestSaveBuildRelationship->getMaritalStatusValid()."&single_parent=".$this->dataInvestSaveBuildRelationship->getSingleParentValid()."&childrens=".$this->dataInvestSaveBuildRelationship->getChildrensValid()."&drug_addiction=".$this->dataInvestSaveBuildRelationship->getDrugAddictionValid()."&illegal_drugs=".$this->dataInvestSaveBuildRelationship->getIllegalDrugsValid()."&physical_abuse=".$this->dataInvestSaveBuildRelationship->getPhysicalAbuseValid()."&sexual_abuse=".$this->dataInvestSaveBuildRelationship->getSexualAbuseValid()."&mental_abuse=".$this->dataInvestSaveBuildRelationship->getMentalAbuseValid()."&political_affiliation=".$this->dataInvestSaveBuildRelationship->getPoliticalAffiliationValid()."&military_experience=".$this->dataInvestSaveBuildRelationship->getMilitaryExperienceValid()."&cancer_survivor=".$this->dataInvestSaveBuildRelationship->getCancerSurvivorValid()."&chronic_disease_sufferer=".$this->dataInvestSaveBuildRelationship->getChronicDiseaseSuffererValid()."&physical_handicap=".$this->dataInvestSaveBuildRelationship->getPhysicalHandicapValid()."&immigrant=".$this->dataInvestSaveBuildRelationship->getImmigrantValid()."&family_refugees=".$this->dataInvestSaveBuildRelationship->getFamilyRefugeesValid()."&form_of_persecution".$this->dataInvestSaveBuildRelationship->getFormOfPersecutionValid()."&languages_spoken_written=".$this->dataInvestSaveBuildRelationship->getLanguagesSpokenWrittenValid()."&charity_work=".$this->dataInvestSaveBuildRelationship->getCharityWorkValid()."&athlete_in_youth=".$this->dataInvestSaveBuildRelationship->getAthleteInYouthValid()."&ever_started_a_company_before=".$this->dataInvestSaveBuildRelationship->getEverStartedACompanyBeforeValid()."&homeless=".$this->dataInvestSaveBuildRelationship->getHomelessValid()."&hours_a_week_work=".$this->dataInvestSaveBuildRelationship->hoursAWeekWorkValid();
        return $this->postFieldsForEnterAllValidData;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForBlankAllData()
    {
        $this->postFieldsForBlankAllData = "";
        return $this->postFieldsForBlankAllData;
    }

    function __construct()
    {
        $this->dataCreateApiUrl = new dataCreateApiUrl();
        $this->dataInvestSaveBuildRelationship = new dataInvestSaveBuildRelationship();

    }

    /**
     * @return mixed
     */
    public function getPostFieldsForValidData()
    {
        $this->postFieldsForValidData = "date_of_birth=" . $this->dataInvestSaveBuildRelationship->getDateOfBirthValid() . "&home_town=" . $this->dataInvestSaveBuildRelationship->getHomeTownValid() . "&born_state=" . $this->dataInvestSaveBuildRelationship->getBornStateValid() . "&born_country=" . $this->dataInvestSaveBuildRelationship->getBornCountryValid();
        return $this->postFieldsForValidData;
    }

    public function getRequestOptions($token, $PostFields)
    {
        return $this->_requestOptions = array(
            CURLOPT_URL => $this->getInvestSaveBuildRelationshipAPIUrl(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $PostFields,
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded",
                "www-token: " . $token . "",
            ),
        );
    }

    /**
     * @return string
     */
    public function getInvestSaveBuildRelationshipAPIUrl()
    {
        $this->_liveFrontUserLoginAPIUrl = $this->dataCreateApiUrl->getDomainLiveAPIURL() . $this->getApiEndpoint();
        return $this->_liveFrontUserLoginAPIUrl;
    }

    /**
     * @return string
     */
    public function getApiEndpoint()
    {
        return $this->apiEndpoint;
    }

}