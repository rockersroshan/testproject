<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

include_once("Data/URL/dataCreateApiUrl.php");
include_once("Data/Signup_Register/dataSignupRegister.php");

class signupRegister
{
    protected $apiEndpoint = "signup/register";
    protected $_postFieldsForValidAllData;
    protected $_postFieldsForInvalidEmail;
    protected $_postFieldsForBlankEmail;
    protected $_postFieldsForMinPassword;
    protected $_postFieldsForMaxPassword;
    protected $_postFieldsForNumberAndCapitalPassword;
    protected $_postFieldsForSmallAndSpecialPassword;
    protected $_postFieldsForPasswordAndCPasswordNotSame;
    protected $_postFieldsForPasswordAndCPasswordBlank;
    protected $dataSignupRegister;
    protected $dataCreateApiUrl;

    function __construct()
    {
        $this->dataCreateApiUrl = new dataCreateApiUrl();
        $this->dataSignupRegister = new dataSignupRegister();
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForPasswordAndCPasswordBlank()
    {
        $this->_postFieldsForPasswordAndCPasswordBlank = "type=" . $this->dataSignupRegister->getValidType() . "&email=" . $this->dataSignupRegister->getValidEmail();
        return $this->_postFieldsForPasswordAndCPasswordBlank;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForPasswordAndCPasswordNotSame()
    {
        $this->_postFieldsForPasswordAndCPasswordNotSame = "type=" . $this->dataSignupRegister->getValidType() . "&email=" . $this->dataSignupRegister->getValidEmail() . "&password=" . $this->dataSignupRegister->getValidPassword() . "&cpassword=" . $this->dataSignupRegister->getValidCPassword() . '1';
        return $this->_postFieldsForPasswordAndCPasswordNotSame;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForSmallAndSpecialPassword()
    {
        $this->_postFieldsForSmallAndSpecialPassword = "type=" . $this->dataSignupRegister->getValidType() . "&email=" . $this->dataSignupRegister->getEmailForNotSuccessReg() . "&password=" . $this->dataSignupRegister->getSmallAndSpecialPassword() . "&cpassword=" . $this->dataSignupRegister->getSmallAndSpecialPassword();
        return $this->_postFieldsForSmallAndSpecialPassword;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForNumberAndCapitalPassword()
    {
        $this->_postFieldsForNumberAndCapitalPassword = "type=" . $this->dataSignupRegister->getValidType() . "&email=" . $this->dataSignupRegister->getEmailForNotSuccessReg() . "&password=" . $this->dataSignupRegister->getNumberAndCapitalPassword() . "&cpassword=" . $this->dataSignupRegister->getNumberAndCapitalPassword();
        return $this->_postFieldsForNumberAndCapitalPassword;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForMaxPassword()
    {
        $this->_postFieldsForMaxPassword = "type=" . $this->dataSignupRegister->getValidType() . "&email=" . $this->dataSignupRegister->getEmailForNotSuccessReg() . "&password=" . $this->dataSignupRegister->getMaxPassword() . "&cpassword=" . $this->dataSignupRegister->getMaxPassword();
        return $this->_postFieldsForMaxPassword;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForMinPassword()
    {
        $this->_postFieldsForMinPassword = "type=" . $this->dataSignupRegister->getValidType() . "&email=" . $this->dataSignupRegister->getEmailForNotSuccessReg() . "&password=" . $this->dataSignupRegister->getInvalidPasswordMin() . "&cpassword=" . $this->dataSignupRegister->getInvalidPasswordMin();
        return $this->_postFieldsForMinPassword;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForBlankEmail()
    {
        $this->_postFieldsForBlankEmail = "type=" . $this->dataSignupRegister->getValidType() . "&password=" . $this->dataSignupRegister->getValidPassword() . "&cpassword=" . $this->dataSignupRegister->getValidCPassword();
        return $this->_postFieldsForBlankEmail;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForInvalidEmail()
    {
        $this->_postFieldsForInvalidEmail = "type=" . $this->dataSignupRegister->getValidType() . "&email=" . $this->dataSignupRegister->getInvalidEmail() . "&password=" . $this->dataSignupRegister->getValidPassword() . "&cpassword=" . $this->dataSignupRegister->getValidCPassword();
        return $this->_postFieldsForInvalidEmail;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForValidAllData()
    {
        $this->_postFieldsForValidAllData = "type=" . $this->dataSignupRegister->getValidType() . "&email=" . $this->dataSignupRegister->getValidEmail() . "&password=" . $this->dataSignupRegister->getValidPassword() . "&cpassword=" . $this->dataSignupRegister->getValidCPassword();
        return $this->_postFieldsForValidAllData;
    }

    public function getRequestOptions($PostFields, $token)
    {
        return $this->_requestOptions = array(
            CURLOPT_URL => $this->getLiveHomeCountriesAPIUrl(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $PostFields,
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/x-www-form-urlencoded",
                "www-token: " . $token . "",
            ),
        );
    }

    /**
     * @return string
     */
    public function getLiveHomeCountriesAPIUrl()
    {
        $this->_liveFrontUserLoginAPIUrl = $this->dataCreateApiUrl->getDomainLiveAPIURL() . $this->getApiEndpoint();
        return $this->_liveFrontUserLoginAPIUrl;
    }

    /**
     * @return string
     */
    public function getApiEndpoint()
    {
        return $this->apiEndpoint;
    }


}