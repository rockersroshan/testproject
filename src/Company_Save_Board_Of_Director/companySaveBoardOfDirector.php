<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

include_once("Data/URL/dataCreateApiUrl.php");
include_once("Data/Company_Save_Board_Of_Director/dataCompanySaveBoardOfDirector.php");

class companySaveBoardOfDirector
{
    protected $apiEndpoint = "web/build_company/save_board_of_director";
    protected $dataCreateApiUrl;
    protected $dataCompanySaveBoardOfDirector;
    protected $postFieldsForValidData;
    protected $postFieldsForRequiredData;
    protected $postFieldsForInvalidFileAdvisor;
    protected $postFieldsForInvalidLinkedinURL;
    protected $postFieldsForBlankAllData;

    function __construct()
    {
        $this->dataCreateApiUrl = new dataCreateApiUrl();
        $this->dataCompanySaveBoardOfDirector = new dataCompanySaveBoardOfDirector();
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForBlankAllData()
    {
        return $this->postFieldsForBlankAllData = "";
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForInvalidLinkedinURL()
    {
        return $this->postFieldsForInvalidLinkedinURL = array(
            "name" => $this->dataCompanySaveBoardOfDirector->getNameValidData(),
            "role" => $this->dataCompanySaveBoardOfDirector->getRoleValidData(),
            "linkedin_url" => $this->dataCompanySaveBoardOfDirector->getLinkedinURLInvalidalidData());
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForInvalidFileAdvisor()
    {
        $cFile = $this->makeCurlFile($this->dataCompanySaveBoardOfDirector->getFileAdvisorInvalidData());
        return $this->postFieldsForInvalidFileAdvisor = array(
            "name" => $this->dataCompanySaveBoardOfDirector->getNameValidData(),
            "file_board_of_director" => $cFile,
            "role" => $this->dataCompanySaveBoardOfDirector->getRoleValidData());
    }

    function makeCurlFile($file)
    {
        $mime = mime_content_type($file);
        $info = pathinfo($file);
        $name = $info['basename'];
        $output = new CURLFile($file, $mime, $name);
        return $output;
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForRequiredData()
    {
        return $this->postFieldsForRequiredData = array(
            "name" => $this->dataCompanySaveBoardOfDirector->getNameValidData(),
            "role" => $this->dataCompanySaveBoardOfDirector->getRoleValidData());
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForValidData()
    {
        $cFile = $this->makeCurlFile($this->dataCompanySaveBoardOfDirector->getFileAdvisorValidData());
        return $this->postFieldsForValidData = array(
            "name" => $this->dataCompanySaveBoardOfDirector->getNameValidData(),
            "file_board_of_director" => $cFile,
            "role" => $this->dataCompanySaveBoardOfDirector->getRoleValidData(),
            "linkedin_url" => $this->dataCompanySaveBoardOfDirector->getLinkedinURLValidData());
    }

    public function getRequestOptions($token, $PostFields)
    {
        return $this->_requestOptions = array(
            CURLOPT_URL => $this->getCompanySaveBoardOfDirectorAPIUrl(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $PostFields,
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: multipart/form-data",
                "www-token: " . $token . ""
            ),
        );
    }

    /**
     * @return string
     */
    public function getCompanySaveBoardOfDirectorAPIUrl()
    {
        return $this->_liveFrontUserLoginAPIUrl = $this->dataCreateApiUrl->getDomainLiveAPIURL() . $this->getApiEndpoint();
    }

    /**
     * @return string
     */
    public function getApiEndpoint()
    {
        return $this->apiEndpoint;
    }
}