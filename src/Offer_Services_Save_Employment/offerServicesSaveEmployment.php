<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

include_once("Data/URL/dataCreateApiUrl.php");
include_once("Data/Offer_Services_Save_Employment/dataOfferServicesSaveEmployment.php");

class offerServicesSaveEmployment
{
    protected $apiEndpoint = "web/offer_services/save_employment";
    protected $dataCreateApiUrl;
    protected $dataOfferServicesSaveEmployment;
    protected $postFieldsForValidData;

    function __construct()
    {
        $this->dataCreateApiUrl = new dataCreateApiUrl();
        $this->dataOfferServicesSaveEmployment = new dataOfferServicesSaveEmployment();
    }

    /**
     * @return mixed
     */
    public function getPostFieldsForValidData()
    {
        return $this->postFieldsForValidData = array("company" => $this->dataOfferServicesSaveEmployment->getCompanyValidData(),
            "city" => $this->dataOfferServicesSaveEmployment->getCityValidData(),
            "country" => $this->dataOfferServicesSaveEmployment->getCountryValidData(),
            "title" => $this->dataOfferServicesSaveEmployment->getTitleValidData(),
            "role" => $this->dataOfferServicesSaveEmployment->getRoleValidData(),
            "from_month" => $this->dataOfferServicesSaveEmployment->getFromMonthValidData(),
            "from_year" => $this->dataOfferServicesSaveEmployment->getFromYearValidData(),
            "to_month" => $this->dataOfferServicesSaveEmployment->getToMonthValidData(),
            "to_year" => $this->dataOfferServicesSaveEmployment->getToYearValidData(),
            "description" => $this->dataOfferServicesSaveEmployment->getDescriptionValidData());
    }

    public function getRequestOptions($token, $PostFields)
    {
        return $this->_requestOptions = array(
            CURLOPT_URL => $this->getOfferServicesSaveEmploymentAPIUrl(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $PostFields,
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: multipart/form-data",
                "www-token: " . $token . ""
            ),
        );
    }

    /**
     * @return string
     */
    public function getOfferServicesSaveEmploymentAPIUrl()
    {
        return $this->offerServicesSaveEmploymentAPIUrl = $this->dataCreateApiUrl->getDomainLiveAPIURL() . $this->getApiEndpoint();
    }

    /**
     * @return string
     */
    public function getApiEndpoint()
    {
        return $this->apiEndpoint;
    }
}