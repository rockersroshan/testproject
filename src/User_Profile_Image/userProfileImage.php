<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

include_once("Data/URL/dataCreateApiUrl.php");
include_once("Data/User_Profile_Image/dataUserProfileImage.php");
include_once("Function/callAPI/postAPI.php");
include_once("src/Front_Login/frontLogin.php");

class userProfileImage
{
    protected $apiEndpoint = "web/account/UserImageAjax";
    protected $dataCreateApiUrl;
    protected $dataUserProfileImage;
    protected $postAPI;
    protected $frontLogin;

    function __construct()
    {
        $this->dataCreateApiUrl = new dataCreateApiUrl();
        $this->dataUserProfileImage = new dataUserProfileImage();
        $this->postAPI = new postAPI();
        $this->frontLogin = new frontLogin();
    }

    public function getRequestOptions($file, $token)
    {
        $cFile = $this->makeCurlFile($file);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->getLiveHomeCountriesAPIUrl());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, array("file1" => $cFile));
        $headers = array();
        $headers[] = "Content-Type: multipart/form-data";
        $headers[] = "www-token: " . $token;
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($curl);
        $info = curl_getinfo($curl);

        //echo "\n\n".$response;

        //echo "Response code : " . $info["http_code"];
        $json_responce = (array)json_decode($response);
        $json_responce['info'] = $info;

        $response = json_encode($json_responce);

        curl_close($curl);

        return $response;
    }

    function makeCurlFile($file)
    {
        $mime = mime_content_type($file);
        $info = pathinfo($file);
        $name = $info['basename'];
        $output = new CURLFile($file, $mime, $name);
        return $output;
    }

    /**
     * @return string
     */
    public function getLiveHomeCountriesAPIUrl()
    {
        $this->_liveFrontUserLoginAPIUrl = $this->dataCreateApiUrl->getDomainLiveAPIURL() . $this->getApiEndpoint();
        return $this->_liveFrontUserLoginAPIUrl;
    }

    /**
     * @return string
     */
    public function getApiEndpoint()
    {
        return $this->apiEndpoint;
    }

    public function getLoginToken()
    {
        $postFields = $this->getLoginPostFields();
        $requestOptions = $this->frontLogin->getRequestOptions($postFields);
        $responce = $this->postAPI->callPostAPI($requestOptions);

        $json_responce = json_decode($responce);

        return $json_responce->data->token;
    }

    /**
     * @return string
     */
    public function getLoginPostFields()
    {
        $this->_postFields = "email=" . $this->dataUserProfileImage->getUsername() . "&password=" . $this->dataUserProfileImage->getPassword();
        return $this->_postFields;
    }
}