<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

use PHPUnit\Framework\TestCase;

include_once("Function/commonFunction/commonFunction.php");
include_once("Data/Invest_Save_Investment/dataInvestSaveInvestment.php");
include_once("src/Invest_Save_Investment/investSaveInvestment.php");
include_once("Function/callAPI/postAPI.php");
include_once("Data/URL/dataCreateApiUrl.php");

class testInvestSaveInvestment extends TestCase
{
    public function testInvestSaveInvestmentWithValidData()
    {
        $commonFunction = new commonFunction();
        $dataInvestSaveInvestment = new dataInvestSaveInvestment();
        $investSaveInvestment = new investSaveInvestment();
        $postAPI = new postAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();

        $postFields = $investSaveInvestment->getPostFieldsForValidData();
        $token = $commonFunction->getLoginToken($dataInvestSaveInvestment->getUsername(), $dataInvestSaveInvestment->getPassword());
        $requestOptions = $investSaveInvestment->getRequestOptions($token, $postFields);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertTrue($json_response->success);

        $dataCount = count($json_response->errors);
        $this->assertEquals($dataCount, 0);

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testInvestSaveInvestmentWithBlankRadioButtonData()
    {
        $commonFunction = new commonFunction();
        $dataInvestSaveInvestment = new dataInvestSaveInvestment();
        $investSaveInvestment = new investSaveInvestment();
        $postAPI = new postAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();


        $postFields = $investSaveInvestment->getPostFieldsForBlankRadioButton();
        $token = $commonFunction->getLoginToken($dataInvestSaveInvestment->getUsername(), $dataInvestSaveInvestment->getPassword());
        $requestOptions = $investSaveInvestment->getRequestOptions($token, $postFields);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertTrue($checkDataType->checkStringData($json_response->success));
        $this->assertEquals($json_response->success, $dataInvestSaveInvestment->getSuccessBlank());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->is_us_citizenError));
        $this->assertEquals($json_response->errors->is_us_citizenError, $dataInvestSaveInvestment->getIsUsCitizenErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->is_us_residentError));
        $this->assertEquals($json_response->errors->is_us_residentError, $dataInvestSaveInvestment->getIsUsResidentErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->accreditedError));
        $this->assertEquals($json_response->errors->accreditedError, $dataInvestSaveInvestment->getAccreditedErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->employment_statusError));
        $this->assertEquals($json_response->errors->employment_statusError, $dataInvestSaveInvestment->getNullStaringData());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->ssnError));
        $this->assertEquals($json_response->errors->ssnError, $dataInvestSaveInvestment->getNullStaringData());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->net_worthError));
        $this->assertEquals($json_response->errors->net_worthError, $dataInvestSaveInvestment->getNullStaringData());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->accreditation_incomeError));
        $this->assertEquals($json_response->errors->accreditation_incomeError, $dataInvestSaveInvestment->getNullStaringData());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->expect_this_yearError));
        $this->assertEquals($json_response->errors->expect_this_yearError, $dataInvestSaveInvestment->getNullStaringData());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->household_income_expect_this_yearError));
        $this->assertEquals($json_response->errors->household_income_expect_this_yearError, $dataInvestSaveInvestment->getNullStaringData());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->jointly_net_worthError));
        $this->assertEquals($json_response->errors->jointly_net_worthError, $dataInvestSaveInvestment->getNullStaringData());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->job_titleError));
        $this->assertEquals($json_response->errors->job_titleError, $dataInvestSaveInvestment->getNullStaringData());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->occupationError));
        $this->assertEquals($json_response->errors->occupationError, $dataInvestSaveInvestment->getNullStaringData());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->employerError));
        $this->assertEquals($json_response->errors->employerError, $dataInvestSaveInvestment->getNullStaringData());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testInvestSaveInvestmentWithBlankEmploymentStatusData()
    {
        $commonFunction = new commonFunction();
        $dataInvestSaveInvestment = new dataInvestSaveInvestment();
        $investSaveInvestment = new investSaveInvestment();
        $postAPI = new postAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();


        $postFields = $investSaveInvestment->getPostFieldsForBlankEmploymentStatus();
        $token = $commonFunction->getLoginToken($dataInvestSaveInvestment->getUsername(), $dataInvestSaveInvestment->getPassword());
        $requestOptions = $investSaveInvestment->getRequestOptions($token, $postFields);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertTrue($checkDataType->checkStringData($json_response->success));
        $this->assertEquals($json_response->success, $dataInvestSaveInvestment->getSuccessBlank());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->is_us_citizenError));
        $this->assertEquals($json_response->errors->is_us_citizenError, $dataInvestSaveInvestment->getNullStaringData());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->is_us_residentError));
        $this->assertEquals($json_response->errors->is_us_residentError, $dataInvestSaveInvestment->getNullStaringData());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->accreditedError));
        $this->assertEquals($json_response->errors->accreditedError, $dataInvestSaveInvestment->getNullStaringData());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->employment_statusError));
        $this->assertEquals($json_response->errors->employment_statusError, $dataInvestSaveInvestment->getEmploymentStatusErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->ssnError));
        $this->assertEquals($json_response->errors->ssnError, $dataInvestSaveInvestment->getNullStaringData());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->net_worthError));
        $this->assertEquals($json_response->errors->net_worthError, $dataInvestSaveInvestment->getNullStaringData());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->accreditation_incomeError));
        $this->assertEquals($json_response->errors->accreditation_incomeError, $dataInvestSaveInvestment->getNullStaringData());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->expect_this_yearError));
        $this->assertEquals($json_response->errors->expect_this_yearError, $dataInvestSaveInvestment->getNullStaringData());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->household_income_expect_this_yearError));
        $this->assertEquals($json_response->errors->household_income_expect_this_yearError, $dataInvestSaveInvestment->getNullStaringData());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->jointly_net_worthError));
        $this->assertEquals($json_response->errors->jointly_net_worthError, $dataInvestSaveInvestment->getNullStaringData());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->job_titleError));
        $this->assertEquals($json_response->errors->job_titleError, $dataInvestSaveInvestment->getNullStaringData());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->occupationError));
        $this->assertEquals($json_response->errors->occupationError, $dataInvestSaveInvestment->getNullStaringData());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->employerError));
        $this->assertEquals($json_response->errors->employerError, $dataInvestSaveInvestment->getNullStaringData());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testInvestSaveInvestmentWithInvalidToken()
    {
        $commonFunction = new commonFunction();
        $dataInvestSaveInvestment = new dataInvestSaveInvestment();
        $investSaveInvestment = new investSaveInvestment();
        $postAPI = new postAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();


        $postFields = $investSaveInvestment->getPostFieldsForBlankEmploymentStatus();
        $token = $commonFunction->getLoginToken($dataInvestSaveInvestment->getUsername(), $dataInvestSaveInvestment->getPassword()) . 'test';
        $requestOptions = $investSaveInvestment->getRequestOptions($token, $postFields);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataInvestSaveInvestment->getInvalidTokenMessage());

        $this->assertTrue($checkDataType->checkIntData($json_response->error_code));
        $this->assertEquals($json_response->error_code, $dataInvestSaveInvestment->getErrorCodeForInvalidToken());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }
}
