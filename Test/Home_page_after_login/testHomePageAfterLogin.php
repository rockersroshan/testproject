<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

use PHPUnit\Framework\TestCase;

include_once("Data/URL/dataCreateApiUrl.php");
include_once("Function/callAPI/getAPI.php");
include_once("src/Home_page_after_login/homePageAfterLogin.php");
include_once("Verification/checkDataType.php");
include_once("Data/Home_page_after_login/dataHomePageAfterLogin.php");

class testHomePageAfterLogin extends TestCase
{
    public function testHomePageAfterLoginWithValidToken()
    {
        $dataCreateApiUrl = new dataCreateApiUrl();
        $getAPI = new getAPI();
        $homePageAfterLogin = new homePageAfterLogin();
        $checkDataType = new checkDataType();
        $dataHomePageAfterLogin = new dataHomePageAfterLogin();

        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $homePageAfterLogin->getRequestOptions($token);
        $response = $getAPI->callGETAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertTrue($json_response->success);

        $this->assertTrue($checkDataType->checkIntData($json_response->error_code));

        $this->assertEquals($json_response->error_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataHomePageAfterLogin->getValidDataMessage());

        $this->assertTrue($checkDataType->checkIntData($json_response->data->hot_equities));

        $this->assertTrue($checkDataType->checkIntData($json_response->data->follower_equities));

        $alphabetically_equitiesList = $json_response->data->alphabetically_equities;
        $alphabetically_equitiesListCount = count($alphabetically_equitiesList);
        if ($alphabetically_equitiesListCount <= 1) {
            for ($i = 0; $i < $alphabetically_equitiesListCount; $i++) {
                $this->verifyAllData($alphabetically_equitiesList[$i]);
            }
        }

        $recent_equitiesList = $json_response->data->recent_equities;
        $recent_equitiesListCount = count($recent_equitiesList);
        if ($recent_equitiesListCount <= 1) {
            for ($i = 0; $i < $recent_equitiesListCount; $i++) {
                $this->verifyAllData($recent_equitiesList[$i]);
            }
        }

        $feature_equitiesList = $json_response->data->feature_equities;
        $feature_equitiesListCount = count($feature_equitiesList);
        if ($feature_equitiesListCount <= 1) {
            for ($j = 0; $j < $feature_equitiesListCount; $j++) {
                $this->verifyAllData($feature_equitiesList[$j]);
            }
        }

        $categories_following_equitiesList = $json_response->data->categories_following_equities;
        $categories_following_equitiesListCount = count($categories_following_equitiesList);
        if ($categories_following_equitiesListCount <= 1) {
            for ($k = 0; $k < $categories_following_equitiesListCount; $k++) {
                $this->verifyAllData($categories_following_equitiesList[$k]);
            }
        }

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function verifyAllData($arrayData)
    {
        $checkDataType = new checkDataType();
        $this->assertTrue($checkDataType->checkIntData($arrayData->equity_id));
        $this->assertTrue($checkDataType->checkIntData($arrayData->company_id));
        $this->assertTrue($checkDataType->checkStringData($arrayData->company_name));
        $this->assertTrue($checkDataType->checkStringData($arrayData->equity_url));
        $this->assertTrue($checkDataType->checkStringData($arrayData->date_added));
        $this->assertTrue($checkDataType->checkStringData($arrayData->end_date));
        $this->assertTrue($checkDataType->checkIntData($arrayData->minimum_raise));
        $this->assertTrue($checkDataType->checkIntData($arrayData->goal));
        $this->assertTrue($checkDataType->checkStringData($arrayData->company_overview));
        $this->assertTrue($checkDataType->checkStringData($arrayData->company_logo));
        $this->assertTrue($checkDataType->checkStringData($arrayData->elevator_pitch));
        $this->assertTrue($checkDataType->checkStringData($arrayData->equity_currency_code));
        $this->assertTrue($checkDataType->checkStringData($arrayData->equity_currency_symbol));
        $this->assertTrue($checkDataType->checkIntData($arrayData->status));
        $this->assertTrue($checkDataType->checkStringData($arrayData->headquater_country));
        $this->assertTrue($checkDataType->checkStringData($arrayData->headquater_state));
        $this->assertTrue($checkDataType->checkStringData($arrayData->headquater_city));
        $this->assertTrue($checkDataType->checkIntData($arrayData->amount_get));
        $this->assertTrue($checkDataType->checkStringData($arrayData->website_url));
        $this->assertTrue($checkDataType->checkStringData($arrayData->deal_highlight1));
        $this->assertTrue($checkDataType->checkStringData($arrayData->deal_highlight2));
        $this->assertTrue($checkDataType->checkStringData($arrayData->deal_highlight3));
        $this->assertTrue($checkDataType->checkStringData($arrayData->deal_highlight4));
        $this->assertTrue($checkDataType->checkStringData($arrayData->deal_type_name));
        $this->assertTrue($checkDataType->checkIntData($arrayData->available_shares));
        $this->assertTrue($checkDataType->checkIntData($arrayData->equity_available));
        $this->assertTrue($checkDataType->checkIntData($arrayData->interest));
        $this->assertTrue($checkDataType->checkIntData($arrayData->term_length));
        $this->assertTrue($checkDataType->checkIntData($arrayData->valuation_cap));
        $this->assertTrue($checkDataType->checkIntData($arrayData->conversation_discount));
        $this->assertTrue($checkDataType->checkIntData($arrayData->warrant_coverage));
        $this->assertTrue($checkDataType->checkStringData($arrayData->payment_start_date));
        $this->assertTrue($checkDataType->checkIntData($arrayData->company_fundraising));
        $this->assertTrue($checkDataType->checkStringData($arrayData->date_round_open));
        $this->assertTrue($checkDataType->checkIntData($arrayData->amount_raise_current_round));
        $this->assertTrue($checkDataType->checkIntData($arrayData->maximum_raise));
        $this->assertTrue($checkDataType->checkStringData($arrayData->funding_type));
        $this->assertTrue($checkDataType->checkIntData($arrayData->executive_summary_status));
        $this->assertTrue($checkDataType->checkStringData($arrayData->executive_summary_file));
        $this->assertTrue($checkDataType->checkIntData($arrayData->term_sheet_status));
        $this->assertTrue($checkDataType->checkStringData($arrayData->term_sheet_file));
        $this->assertTrue($checkDataType->checkStringData($arrayData->project_timezone));
        $this->assertTrue($checkDataType->checkStringData($arrayData->allowed_investor));
        $this->assertTrue($checkDataType->checkStringData($arrayData->tax_relief_type));
        $this->assertTrue($checkDataType->checkIntData($arrayData->routing_number));
        $this->assertTrue($checkDataType->checkIntData($arrayData->bank_account_number));
        $this->assertTrue($checkDataType->checkStringData($arrayData->bank_account_type));
        $this->assertTrue($checkDataType->checkStringData($arrayData->bank_name));
        $this->assertTrue($checkDataType->checkIntData($arrayData->return_investment_interest));
        $this->assertTrue($checkDataType->checkStringData($arrayData->return_starts));
        $this->assertTrue($checkDataType->checkStringData($arrayData->return_payment_frequency));
        $this->assertTrue($checkDataType->checkIntData($arrayData->return_term_length));
        $this->assertTrue($checkDataType->checkStringData($arrayData->contract_copy_file));
        $this->assertTrue($checkDataType->checkStringData($arrayData->pitch_video));
        $this->assertTrue($checkDataType->checkStringData($arrayData->pitch_video_mine));
        $this->assertTrue($checkDataType->checkIntData($arrayData->user_id));
        $this->assertTrue($checkDataType->checkStringData($arrayData->user_name));
        $this->assertTrue($checkDataType->checkStringData($arrayData->last_name));
        $this->assertTrue($checkDataType->checkStringData($arrayData->address));
        $this->assertTrue($checkDataType->checkStringData($arrayData->address));
        $this->assertTrue($checkDataType->checkIntData($arrayData->facebook_wall_post));
        $this->assertTrue($checkDataType->checkStringData($arrayData->email));
        $this->assertTrue($checkDataType->checkStringData($arrayData->profile_slug));
        $this->assertTrue($checkDataType->checkStringData($arrayData->user_occupation));
        $this->assertTrue($checkDataType->checkStringData($arrayData->deal_type_slug));
        $this->assertTrue($checkDataType->checkStringData($arrayData->deal_type_icon));
        $this->assertTrue($checkDataType->checkStringData($arrayData->company_category));
        $this->assertTrue($checkDataType->checkIntData($arrayData->company_category_id));
        $this->assertTrue($checkDataType->checkIntData($arrayData->year_founded));
        $this->assertTrue($checkDataType->checkIntData($arrayData->assets_under_management));
        $this->assertTrue($checkDataType->checkIntData($arrayData->square_footage));
        $this->assertTrue($checkDataType->checkStringData($arrayData->company_email));
        $this->assertTrue($checkDataType->checkStringData($arrayData->company_cover_photo));
        $this->assertTrue($checkDataType->checkStringData($arrayData->company_url));
    }

    public function testHomePageAfterLoginWithInvalidToken()
    {
        $dataCreateApiUrl = new dataCreateApiUrl();
        $getAPI = new getAPI();
        $homePageAfterLogin = new homePageAfterLogin();
        $checkDataType = new checkDataType();
        $dataHomePageAfterLogin = new dataHomePageAfterLogin();

        $token = $dataCreateApiUrl->getClientpublickey() . 'test';
        $requestOptions = $homePageAfterLogin->getRequestOptions($token);
        $response = $getAPI->callGETAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkIntData($json_response->error_code));

        $this->assertEquals($json_response->error_code, $dataHomePageAfterLogin->getErrorCodeForInvalidToken());

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataHomePageAfterLogin->getInvalidTokenMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }
}
