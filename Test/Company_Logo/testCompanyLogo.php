<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

use PHPUnit\Framework\TestCase;

include_once("src/Company_Logo/companyLogo.php");
include_once("Data/URL/dataCreateApiUrl.php");
include_once("Data/Company_Logo/dataCompanyLogo.php");
include_once("Verification/checkDataType.php");
include_once("Function/commonFunction/commonFunction.php");

class testCompanyLogo extends TestCase
{
    public function testCompanyLogoWithValidData()
    {
        $companyLogo = new companyLogo();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $dataCompanyLogo = new dataCompanyLogo();
        $checkDataType = new checkDataType();
        $commonFunction = new commonFunction();

        $file = $dataCompanyLogo->getValidImagePath();
        $token = $commonFunction->getLoginToken($dataCompanyLogo->getUsername(), $dataCompanyLogo->getPassword());
        $response = $companyLogo->getRequestOptions($file, $token);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertTrue($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->error));
        $this->assertEquals($json_response->error, $dataCompanyLogo->getValidImageErrors());

        $this->assertTrue($checkDataType->checkStringData($json_response->image_path));
        $this->assertContains($dataCompanyLogo->getFileUploadValidURL(), $json_response->image_path);

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testCompanyLogoWithInvalidPath()
    {
        $companyLogo = new companyLogo();
        $dataCompanyLogo = new dataCompanyLogo();
        $commonFunction = new commonFunction();

        $file = $dataCompanyLogo->getInvalidImagePath();
        $token = $commonFunction->getLoginToken($dataCompanyLogo->getUsername(), $dataCompanyLogo->getPassword());
        $response = $companyLogo->getRequestOptions($file, $token);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCompanyLogo->getInvalidFilePathHttpCode());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testCompanyLogoWithInvalidImageFormat()
    {
        $companyLogo = new companyLogo();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $dataCompanyLogo = new dataCompanyLogo();
        $checkDataType = new checkDataType();
        $commonFunction = new commonFunction();

        $file = $dataCompanyLogo->getInvalidFormatImagePath();
        $token = $commonFunction->getLoginToken($dataCompanyLogo->getUsername(), $dataCompanyLogo->getPassword());
        $response = $companyLogo->getRequestOptions($file, $token);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->image_path));
        $this->assertEquals($json_response->image_path, $dataCompanyLogo->getInvalidImageFormatProfileImage());

        $this->assertTrue($checkDataType->checkStringData($json_response->error));
        $this->assertEquals($json_response->error, $dataCompanyLogo->getInvalidImageFormatError());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testCompanyLogoWithWithVideo()
    {
        $companyLogo = new companyLogo();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $dataCompanyLogo = new dataCompanyLogo();
        $checkDataType = new checkDataType();
        $commonFunction = new commonFunction();

        $file = $dataCompanyLogo->getVideoPath();
        $token = $commonFunction->getLoginToken($dataCompanyLogo->getUsername(), $dataCompanyLogo->getPassword());
        $response = $companyLogo->getRequestOptions($file, $token);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->image_path));
        $this->assertEquals($json_response->image_path, $dataCompanyLogo->getInvalidImageFormatProfileImage());

        $this->assertTrue($checkDataType->checkStringData($json_response->error));
        $this->assertEquals($json_response->error, $dataCompanyLogo->getInvalidImageFormatError());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testCompanyLogoWithInvalidToken()
    {
        $companyLogo = new companyLogo();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $dataCompanyLogo = new dataCompanyLogo();
        $checkDataType = new checkDataType();
        $commonFunction = new commonFunction();

        $file = $dataCompanyLogo->getVideoPath();
        $token = $commonFunction->getLoginToken($dataCompanyLogo->getUsername(), $dataCompanyLogo->getPassword()) . 'test';
        $response = $companyLogo->getRequestOptions($file, $token);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));
        $this->assertEquals($json_response->message, $dataCompanyLogo->getInvalidTokenMessage());

        $this->assertTrue($checkDataType->checkIntData($json_response->error_code));
        $this->assertEquals($json_response->error_code, $dataCompanyLogo->getInvalidTokenErrorCode());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }
}
