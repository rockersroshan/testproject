<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

use PHPUnit\Framework\TestCase;

include_once("src/Company_Save_Basic/companySaveBasic.php");
include_once("Function/commonFunction/commonFunction.php");
include_once("Data/Company_Save_Basic/dataCompanySaveBasic.php");
include_once("Function/callAPI/postAPI.php");
include_once("Data/URL/dataCreateApiUrl.php");
include_once("Verification/checkDataType.php");

class testCompanySaveBasic extends TestCase
{
    public function testCompanyTypeWithValidData()
    {
        $companySaveBasic = new companySaveBasic();
        $commonFunction = new commonFunction();
        $dataCompanySaveBasic = new dataCompanySaveBasic();
        $postAPI = new postAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();

        $postFields = $companySaveBasic->getPostFieldsForValidData();
        $token = $commonFunction->getLoginToken($dataCompanySaveBasic->getUsername(), $dataCompanySaveBasic->getPassword());
        $requestOptions = $companySaveBasic->getRequestOptions($token, $postFields);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertTrue($json_response->success);

        $dataCount = count($json_response->errors);
        $this->assertEquals($dataCount, 0);

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testCompanyTypeWithBlankPrimaryDetailsData()
    {
        $companySaveBasic = new companySaveBasic();
        $commonFunction = new commonFunction();
        $dataCompanySaveBasic = new dataCompanySaveBasic();
        $postAPI = new postAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();

        $postFields = $companySaveBasic->getPostFieldsForBlankPrimaryDetailsData();
        $token = $commonFunction->getLoginToken($dataCompanySaveBasic->getUsername(), $dataCompanySaveBasic->getPassword());
        $requestOptions = $companySaveBasic->getRequestOptions($token, $postFields);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->user_nameError));
        $this->assertEquals($json_response->errors->user_nameError, $dataCompanySaveBasic->getUserNameErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->last_nameError));
        $this->assertEquals($json_response->errors->last_nameError, $dataCompanySaveBasic->getLastNameErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->company_nameError));
        $this->assertEquals($json_response->errors->company_nameError, $dataCompanySaveBasic->getCompanyNameErrorNullMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->company_urlError));
        $this->assertEquals($json_response->errors->company_urlError, $dataCompanySaveBasic->getCompanyURLErrorNullMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->company_category_idError));
        $this->assertEquals($json_response->errors->company_category_idError, $dataCompanySaveBasic->getCompanyCategoryIdErrorNullMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->short_pitchError));
        $this->assertEquals($json_response->errors->short_pitchError, $dataCompanySaveBasic->getShortPitchErrorNullMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testCompanyTypeWithBlankBasicsData()
    {
        $companySaveBasic = new companySaveBasic();
        $commonFunction = new commonFunction();
        $dataCompanySaveBasic = new dataCompanySaveBasic();
        $postAPI = new postAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();

        $postFields = $companySaveBasic->getPostFieldsForBlankBasicsData();
        $token = $commonFunction->getLoginToken($dataCompanySaveBasic->getUsername(), $dataCompanySaveBasic->getPassword());
        $requestOptions = $companySaveBasic->getRequestOptions($token, $postFields);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->user_nameError));
        $this->assertEquals($json_response->errors->user_nameError, $dataCompanySaveBasic->getUserNameErrorNullMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->last_nameError));
        $this->assertEquals($json_response->errors->last_nameError, $dataCompanySaveBasic->getLastNameErrorNullMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->company_nameError));
        $this->assertEquals($json_response->errors->company_nameError, $dataCompanySaveBasic->getCompanyNameErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->company_urlError));
        $this->assertEquals($json_response->errors->company_urlError, $dataCompanySaveBasic->getCompanyURLErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->company_category_idError));
        $this->assertEquals($json_response->errors->company_category_idError, $dataCompanySaveBasic->getCompanyCategoryIdErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->short_pitchError));
        $this->assertEquals($json_response->errors->short_pitchError, $dataCompanySaveBasic->getShortPitchErrorMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testCompanyTypeWithAllBlankData()
    {
        $companySaveBasic = new companySaveBasic();
        $commonFunction = new commonFunction();
        $dataCompanySaveBasic = new dataCompanySaveBasic();
        $postAPI = new postAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();

        $postFields = $companySaveBasic->getPostFieldsForAllBlankData();
        $token = $commonFunction->getLoginToken($dataCompanySaveBasic->getUsername(), $dataCompanySaveBasic->getPassword());
        $requestOptions = $companySaveBasic->getRequestOptions($token, $postFields);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->generalError));
        $this->assertEquals($json_response->generalError, $dataCompanySaveBasic->getGeneralErrorMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testCompanyTypeWithInvalidToken()
    {
        $companySaveBasic = new companySaveBasic();
        $commonFunction = new commonFunction();
        $dataCompanySaveBasic = new dataCompanySaveBasic();
        $postAPI = new postAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();

        $postFields = $companySaveBasic->getPostFieldsForValidData();
        $token = $commonFunction->getLoginToken($dataCompanySaveBasic->getUsername(), $dataCompanySaveBasic->getPassword()) . 'test';
        $requestOptions = $companySaveBasic->getRequestOptions($token, $postFields);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataCompanySaveBasic->getInvalidTokenMessage());

        $this->assertTrue($checkDataType->checkIntData($json_response->error_code));
        $this->assertEquals($json_response->error_code, $dataCompanySaveBasic->getErrorCodeForInvalidToken());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }
}
