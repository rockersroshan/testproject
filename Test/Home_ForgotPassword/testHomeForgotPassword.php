<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

use PHPUnit\Framework\TestCase;

include_once("Function/callAPI/postAPI.php");
include_once("Data/URL/dataCreateApiUrl.php");
include_once("Verification/checkDataType.php");
include_once("src/Home_ForgotPassword/homeForgotPassword.php");
include_once("Data/Home_ForgotPassword/dataHomeForgotPassword.php");

class testHomeForgotPassword extends TestCase
{
    /**
     * @Description : This test check with valid data.
     */
    public function testForgotPasswordWithValidData()
    {
        $postAPI = new postAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $homeForgotPassword = new homeForgotPassword();
        $dataHomeForgotPassword = new dataHomeForgotPassword();

        $postFields = $homeForgotPassword->getPostFieldsForValidEmail();
        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $homeForgotPassword->getRequestOptions($postFields, $token);
        $responce = $postAPI->callPostAPI($requestOptions);

        $json_responce = json_decode($responce);

        $this->assertEquals($json_responce->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertTrue($json_responce->success);

        $this->assertEquals($json_responce->message, $dataHomeForgotPassword->getValidDataMessage());

        echo "\n\n Response Time : " . $json_responce->info->total_time;
    }

    /**
     * @Description : This test in the token is invalid
     */
    public function testForgotPasswordWithInvalidToken()
    {
        $postAPI = new postAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $homeForgotPassword = new homeForgotPassword();
        $checkDataType = new checkDataType();
        $dataHomeForgotPassword = new dataHomeForgotPassword();

        $postFields = $homeForgotPassword->getPostFieldsForValidEmail();
        $token = $dataCreateApiUrl->getClientpublickey() . "test";
        $requestOptions = $homeForgotPassword->getRequestOptions($postFields, $token);
        $responce = $postAPI->callPostAPI($requestOptions);

        $json_responce = json_decode($responce);

        $this->assertEquals($json_responce->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_responce->success);

        $this->assertTrue($checkDataType->checkStringData($json_responce->message));

        $this->assertEquals($json_responce->message, $dataHomeForgotPassword->getInvalidTokenMessage());

        $this->assertTrue($checkDataType->checkIntData($json_responce->error_code));

        $this->assertEquals($json_responce->error_code, $dataHomeForgotPassword->getErrorCodeWhenInvalidToken());

        echo "\n\n Response Time : " . $json_responce->info->total_time;
    }

    /**
     * @Description : In this test Email address is invalid.
     */
    public function testForgotPasswordWithInvalidEmail()
    {
        $postAPI = new postAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $homeForgotPassword = new homeForgotPassword();
        $checkDataType = new checkDataType();
        $dataHomeForgotPassword = new dataHomeForgotPassword();

        $postFields = $homeForgotPassword->getPostFieldsForInvalidEmail();
        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $homeForgotPassword->getRequestOptions($postFields, $token);
        $responce = $postAPI->callPostAPI($requestOptions);

        $json_responce = json_decode($responce);

        $this->assertEquals($json_responce->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_responce->success);

        $this->assertTrue($checkDataType->checkStringData($json_responce->message));

        $this->assertEquals($json_responce->message, $dataHomeForgotPassword->getNotValidEmailAddressMessage());

        echo "\n\n Response Time : " . $json_responce->info->total_time;
    }

    /**
     * @Description : In this test email address is not register.
     */
    public function testForgotPasswordWithNotRegisterEmail()
    {
        $postAPI = new postAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $homeForgotPassword = new homeForgotPassword();
        $checkDataType = new checkDataType();
        $dataHomeForgotPassword = new dataHomeForgotPassword();

        $postFields = $homeForgotPassword->getPostFieldsForNotRegisterEmail();
        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $homeForgotPassword->getRequestOptions($postFields, $token);
        $responce = $postAPI->callPostAPI($requestOptions);

        $json_responce = json_decode($responce);

        $this->assertEquals($json_responce->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_responce->success);

        $this->assertTrue($checkDataType->checkStringData($json_responce->message));

        $this->assertEquals($json_responce->message, $dataHomeForgotPassword->getNotRegisterEmailAddressMessage());

        echo "\n\n Response Time : " . $json_responce->info->total_time;
    }

    /**
     * @Description : In this test email address and token invalid.
     */
    public function testForgotPasswordWithInvalidTokenAndEmailAddress()
    {
        $postAPI = new postAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $homeForgotPassword = new homeForgotPassword();
        $checkDataType = new checkDataType();
        $dataHomeForgotPassword = new dataHomeForgotPassword();

        $postFields = $homeForgotPassword->getPostFieldsForInvalidEmail();
        $token = $dataCreateApiUrl->getClientpublickey() . "test";
        $requestOptions = $homeForgotPassword->getRequestOptions($postFields, $token);
        $responce = $postAPI->callPostAPI($requestOptions);

        $json_responce = json_decode($responce);

        $this->assertEquals($json_responce->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_responce->success);

        $this->assertTrue($checkDataType->checkStringData($json_responce->message));

        $this->assertEquals($json_responce->message, $dataHomeForgotPassword->getInvalidTokenAndInvalidEmailAddressMessage());

        $this->assertTrue($checkDataType->checkIntData($json_responce->error_code));

        $this->assertEquals($json_responce->error_code, $dataHomeForgotPassword->getErrorCodeWhenInvalidTokenAndEmailAddress());

        echo "\n\n Response Time : " . $json_responce->info->total_time;
    }

}
