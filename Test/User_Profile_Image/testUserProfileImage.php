<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

use PHPUnit\Framework\TestCase;

include_once("src/User_Profile_Image/userProfileImage.php");
include_once("Data/URL/dataCreateApiUrl.php");
include_once("Function/callAPI/postAPI.php");
include_once("Data/User_Profile_Image/dataUserProfileImage.php");

class testUserProfileImage extends TestCase
{
    public function testUserProfileImageWithValidData()
    {
        $userProfileImage = new userProfileImage();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $dataUserProfileImage = new dataUserProfileImage();
        $checkDataType = new checkDataType();

        $file = $dataUserProfileImage->getValidImagePath();
        $token = $userProfileImage->getLoginToken();
        $response = $userProfileImage->getRequestOptions($file, $token);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertTrue($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->error));

        $this->assertEquals($json_response->error, $dataUserProfileImage->getValidImageErrors());

        $this->assertTrue($checkDataType->checkStringData($json_response->profile_image));

        $this->assertContains("https://thecrowd.storage.googleapis.com/upload/user/user_big_image", $json_response->profile_image);
    }

    public function testUserProfileImageWithInvalidPath()
    {
        $userProfileImage = new userProfileImage();
        $dataUserProfileImage = new dataUserProfileImage();


        $file = $dataUserProfileImage->getInvalidImagePath();
        $token = $userProfileImage->getLoginToken();
        $response = $userProfileImage->getRequestOptions($file, $token);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataUserProfileImage->getInvalidFilePathHttpCode());
    }

    public function testUserProfileImageWithInvalidImageFormat()
    {
        $userProfileImage = new userProfileImage();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $dataUserProfileImage = new dataUserProfileImage();
        $checkDataType = new checkDataType();


        $file = $dataUserProfileImage->getInvalidFormatImagePath();
        $token = $userProfileImage->getLoginToken();
        $response = $userProfileImage->getRequestOptions($file, $token);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->profile_image));

        $this->assertEquals($json_response->profile_image, $dataUserProfileImage->getInvalidImageFormatProfileImage());

        $this->assertTrue($checkDataType->checkStringData($json_response->error));

        $this->assertEquals($json_response->error, $dataUserProfileImage->getInvalidImageFormatError());
    }

    public function testUserProfileImageWithVideo()
    {
        $userProfileImage = new userProfileImage();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $dataUserProfileImage = new dataUserProfileImage();
        $checkDataType = new checkDataType();


        $file = $dataUserProfileImage->getVideoPath();
        $token = $userProfileImage->getLoginToken();
        $response = $userProfileImage->getRequestOptions($file, $token);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->profile_image));

        $this->assertEquals($json_response->profile_image, $dataUserProfileImage->getInvalidImageFormatProfileImage());

        $this->assertTrue($checkDataType->checkStringData($json_response->error));

        $this->assertEquals($json_response->error, $dataUserProfileImage->getInvalidImageFormatError());
    }

    public function testUserProfileImageWithInvalidToken()
    {
        $userProfileImage = new userProfileImage();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $dataUserProfileImage = new dataUserProfileImage();
        $checkDataType = new checkDataType();


        $file = $dataUserProfileImage->getVideoPath();
        $token = $userProfileImage->getLoginToken() . 'test';
        $response = $userProfileImage->getRequestOptions($file, $token);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataUserProfileImage->getInvalidTokenMessage());

        $this->assertTrue($checkDataType->checkIntData($json_response->error_code));

        $this->assertEquals($json_response->error_code, $dataUserProfileImage->getInvalidTokenErrorCode());
    }
}
