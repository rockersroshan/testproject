<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

use PHPUnit\Framework\TestCase;

include_once("src/Company_Save_Founder/companySaveFounder.php");
include_once("Function/commonFunction/commonFunction.php");
include_once("Data/Company_Save_Founder/dataCompanySaveFounder.php");
include_once("Function/callAPI/postAPI.php");
include_once("Verification/checkDataType.php");

class testCompanySaveFounder extends TestCase
{
    public function testCompanySaveFounderWithValidData()
    {
        $companySaveFounder = new companySaveFounder();
        $commonFunction = new commonFunction();
        $dataCompanySaveFounder = new dataCompanySaveFounder();
        $postAPI = new postAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();

        $postFields = $companySaveFounder->getPostFieldsForValidData();
        $token = $commonFunction->getLoginToken($dataCompanySaveFounder->getUsername(), $dataCompanySaveFounder->getPassword());
        $requestOptions = $companySaveFounder->getRequestOptions($token, $postFields);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertTrue($json_response->success);

        $this->assertTrue($checkDataType->checkIntData($json_response->company_founder_id));

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->profile_image_nameError));
        $this->assertEquals($json_response->errors->profile_image_nameError, $dataCompanySaveFounder->getProfileImageNameErrorNullMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testCompanySaveFounderWithBlankRequiredData()
    {
        $companySaveFounder = new companySaveFounder();
        $commonFunction = new commonFunction();
        $dataCompanySaveFounder = new dataCompanySaveFounder();
        $postAPI = new postAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();

        $postFields = $companySaveFounder->getPostFieldsForBlankRequiredData();
        $token = $commonFunction->getLoginToken($dataCompanySaveFounder->getUsername(), $dataCompanySaveFounder->getPassword());
        $requestOptions = $companySaveFounder->getRequestOptions($token, $postFields);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->profile_image_nameError));
        $this->assertEquals($json_response->errors->profile_image_nameError, $dataCompanySaveFounder->getProfileImageNameErrorNullMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->profile_image_nameError));
        $this->assertEquals($json_response->errors->profile_image_nameError, $dataCompanySaveFounder->getProfileImageNameErrorNullMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->linkedin_urlError));
        $this->assertEquals($json_response->errors->linkedin_urlError, $dataCompanySaveFounder->getLinkedinURLErrorNullMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testCompanySaveFounderWithInvalidToken()
    {
        $companySaveFounder = new companySaveFounder();
        $commonFunction = new commonFunction();
        $dataCompanySaveFounder = new dataCompanySaveFounder();
        $postAPI = new postAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();

        $postFields = $companySaveFounder->getPostFieldsForBlankRequiredData();
        $token = $commonFunction->getLoginToken($dataCompanySaveFounder->getUsername(), $dataCompanySaveFounder->getPassword()) . 'test';
        $requestOptions = $companySaveFounder->getRequestOptions($token, $postFields);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataCompanySaveFounder->getInvalidTokenMessage());

        $this->assertTrue($checkDataType->checkIntData($json_response->error_code));
        $this->assertEquals($json_response->error_code, $dataCompanySaveFounder->getErrorCodeForInvalidToken());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }
}
