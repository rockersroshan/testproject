<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

use PHPUnit\Framework\TestCase;

include_once("src/Company_Save_Ancillary/companySaveAncillary.php");
include_once("Function/commonFunction/commonFunction.php");
include_once("Data/Company_Save_Ancillary/dataCompanySaveAncillary.php");
include_once("Function/callAPI/postAPI.php");
include_once("Data/URL/dataCreateApiUrl.php");
include_once("Verification/checkDataType.php");

class testCompanySaveAncillary extends TestCase
{
    public function testCompanySaveAncillaryWithValidData()
    {
        $companySaveAncillary = new companySaveAncillary();
        $commonFunction = new commonFunction();
        $dataCompanySaveAncillary = new dataCompanySaveAncillary();
        $postAPI = new postAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();

        $postFields = $companySaveAncillary->getPostFieldsForValidData();
        $token = $commonFunction->getLoginToken($dataCompanySaveAncillary->getUsername(), $dataCompanySaveAncillary->getPassword());
        $requestOptions = $companySaveAncillary->getRequestOptions($token, $postFields);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertTrue($json_response->success);

        $dataCount = count($json_response->errors);
        $this->assertEquals($dataCount, 0);

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testCompanySaveAncillaryWithBlankAncillaryPublicInformation()
    {
        $companySaveAncillary = new companySaveAncillary();
        $commonFunction = new commonFunction();
        $dataCompanySaveAncillary = new dataCompanySaveAncillary();
        $postAPI = new postAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();

        $postFields = $companySaveAncillary->getPostFieldsForBlankAncillaryPublicInformation();
        $token = $commonFunction->getLoginToken($dataCompanySaveAncillary->getUsername(), $dataCompanySaveAncillary->getPassword());
        $requestOptions = $companySaveAncillary->getRequestOptions($token, $postFields);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->ancillary_public_informationError));
        $this->assertEquals($json_response->errors->ancillary_public_informationError, $dataCompanySaveAncillary->getAncillaryPublicInformationErrorMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testCompanySaveAncillaryWithBlankParameter()
    {
        $companySaveAncillary = new companySaveAncillary();
        $commonFunction = new commonFunction();
        $dataCompanySaveAncillary = new dataCompanySaveAncillary();
        $postAPI = new postAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();

        $postFields = $companySaveAncillary->getPostFieldsForBlankParameter();
        $token = $commonFunction->getLoginToken($dataCompanySaveAncillary->getUsername(), $dataCompanySaveAncillary->getPassword());
        $requestOptions = $companySaveAncillary->getRequestOptions($token, $postFields);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->generalError));
        $this->assertEquals($json_response->generalError, $dataCompanySaveAncillary->getGeneralErrorMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testCompanySaveAncillaryWithInvalidToken()
    {
        $companySaveAncillary = new companySaveAncillary();
        $commonFunction = new commonFunction();
        $dataCompanySaveAncillary = new dataCompanySaveAncillary();
        $postAPI = new postAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();

        $postFields = $companySaveAncillary->getPostFieldsForValidData();
        $token = $commonFunction->getLoginToken($dataCompanySaveAncillary->getUsername(), $dataCompanySaveAncillary->getPassword()) . 'test';
        $requestOptions = $companySaveAncillary->getRequestOptions($token, $postFields);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataCompanySaveAncillary->getInvalidTokenMessage());

        $this->assertTrue($checkDataType->checkIntData($json_response->error_code));
        $this->assertEquals($json_response->error_code, $dataCompanySaveAncillary->getErrorCodeForInvalidToken());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }
}
