<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

use PHPUnit\Framework\TestCase;

include_once("Data/URL/dataCreateApiUrl.php");
include_once("src/Home_Countries/homeCountries.php");
include_once("Function/callAPI/getAPI.php");
include_once("Data/Home_Countries/dataHomeCountries.php");
include_once("Verification/checkDataType.php");

class testHomeCountries extends TestCase
{
    public function testHomeCountriesWithValidData()
    {
        $dataCreateApiUrl = new dataCreateApiUrl();
        $homeCountries = new homeCountries();
        $getAPI = new getAPI();
        $dataHomeCountries = new dataHomeCountries();
        $checkDataType = new checkDataType();

        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $homeCountries->getRequestOptions($token);
        $response = $getAPI->callGETAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertTrue($json_response->success);

        $countriesList = $json_response->countries;

        $this->assertEquals(count($countriesList), $dataHomeCountries->getTotalCountryCount());

        for ($i = 0; $i < count($countriesList); $i++) {
            echo "\nArray Number : " . $i;
            echo "\n\tCountry_id : " . $countriesList[$i]->country_id;
            $this->assertTrue($checkDataType->checkIntData($countriesList[$i]->country_id));
            echo "\n\tCountry Name : " . $countriesList[$i]->country_name;
            $this->assertTrue($checkDataType->checkStringData($countriesList[$i]->country_name));
            $this->assertTrue($checkDataType->checkStringData($countriesList[$i]->active));
            $this->assertEquals($countriesList[$i]->active, $dataHomeCountries->getActiveValue());
            $this->assertTrue($checkDataType->checkStringData($countriesList[$i]->fips));
            $this->assertTrue($checkDataType->checkStringData($countriesList[$i]->iso2));
            $this->assertTrue($checkDataType->checkStringData($countriesList[$i]->iso3));
            $this->assertTrue($checkDataType->checkStringData($countriesList[$i]->ison));
            $this->assertTrue($checkDataType->checkStringData($countriesList[$i]->internet));
            $this->assertTrue($checkDataType->checkStringData($countriesList[$i]->capital));
            $this->assertTrue($checkDataType->checkStringData($countriesList[$i]->map_ref));
            $this->assertTrue($checkDataType->checkStringData($countriesList[$i]->singular));
            $this->assertTrue($checkDataType->checkStringData($countriesList[$i]->plural));
            $this->assertTrue($checkDataType->checkStringData($countriesList[$i]->currency));
            $this->assertTrue($checkDataType->checkStringData($countriesList[$i]->currency_code));
            $this->assertTrue($checkDataType->checkStringData($countriesList[$i]->population));
            $this->assertTrue($checkDataType->checkStringData($countriesList[$i]->title));
            $this->assertTrue($checkDataType->checkStringData($countriesList[$i]->comment));
        }
        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testHomeCountriesWithInvalidToken()
    {
        $dataCreateApiUrl = new dataCreateApiUrl();
        $homeCountries = new homeCountries();
        $getAPI = new getAPI();
        $dataHomeCountries = new dataHomeCountries();
        $checkDataType = new checkDataType();

        $token = $dataCreateApiUrl->getClientpublickey() . 'test';
        $requestOptions = $homeCountries->getRequestOptions($token);
        $response = $getAPI->callGETAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataHomeCountries->getInvalidTokenMessage());

        $this->assertTrue($checkDataType->checkIntData($json_response->error_code));

        $this->assertEquals($json_response->error_code, $dataHomeCountries->getErrorCodeForInvalidToken());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }
}
