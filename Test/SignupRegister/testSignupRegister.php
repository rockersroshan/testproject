<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

use PHPUnit\Framework\TestCase;

include_once("Function/callAPI/postAPI.php");
include_once("src/Signup_Register/signupRegister.php");
include_once("Data/URL/dataCreateApiUrl.php");

class testSignupRegister extends TestCase
{
    public function testSignupRegisterWithValidAllData()
    {
        $postAPI = new postAPI();
        $signupRegister = new signupRegister();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();
        $dataSignupRegister = new dataSignupRegister();

        $postFields = $signupRegister->getPostFieldsForValidAllData();
        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $signupRegister->getRequestOptions($postFields, $token);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertTrue($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataSignupRegister->getValidMessage());

        $this->assertTrue($checkDataType->checkIntData($json_response->data->user_id));

        $errorsCount = count($json_response->errors);
        $this->assertEquals($errorsCount, 0);

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testSignupRegisterWithInvalidEmail()
    {
        $postAPI = new postAPI();
        $signupRegister = new signupRegister();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();
        $dataSignupRegister = new dataSignupRegister();

        $postFields = $signupRegister->getPostFieldsForInvalidEmail();
        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $signupRegister->getRequestOptions($postFields, $token);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataSignupRegister->getInvalidEmailMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->data->user_id));

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->emailError));
        $this->assertEquals($json_response->errors->emailError, $dataSignupRegister->getEmailErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->passwordError));
        $this->assertEquals($json_response->errors->passwordError, $dataSignupRegister->getInvalidEmailPasswordErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->cpasswordError));
        $this->assertEquals($json_response->errors->cpasswordError, $dataSignupRegister->getInvalidEmailCPasswordErrorMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testSignupRegisterWithBlankEmail()
    {
        $postAPI = new postAPI();
        $signupRegister = new signupRegister();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();
        $dataSignupRegister = new dataSignupRegister();

        $postFields = $signupRegister->getPostFieldsForBlankEmail();
        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $signupRegister->getRequestOptions($postFields, $token);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataSignupRegister->getInvalidEmailMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->data->user_id));

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->emailError));
        $this->assertEquals($json_response->errors->emailError, $dataSignupRegister->getBlankEmailErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->passwordError));
        $this->assertEquals($json_response->errors->passwordError, $dataSignupRegister->getInvalidEmailPasswordErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->cpasswordError));
        $this->assertEquals($json_response->errors->cpasswordError, $dataSignupRegister->getInvalidEmailCPasswordErrorMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testSignupRegisterWithInvalidPasswordMin()
    {
        $postAPI = new postAPI();
        $signupRegister = new signupRegister();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();
        $dataSignupRegister = new dataSignupRegister();

        $postFields = $signupRegister->getPostFieldsForMinPassword();
        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $signupRegister->getRequestOptions($postFields, $token);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataSignupRegister->getInvalidEmailMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->data->user_id));

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->emailError));
        $this->assertEquals($json_response->errors->emailError, $dataSignupRegister->getInvalidPasswordErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->passwordError));
        $this->assertEquals($json_response->errors->passwordError, $dataSignupRegister->getMinPasswordPasswordErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->cpasswordError));
        $this->assertEquals($json_response->errors->cpasswordError, $dataSignupRegister->getMinPasswordCPasswordErrorMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testSignupRegisterWithInvalidPasswordMax()
    {
        $postAPI = new postAPI();
        $signupRegister = new signupRegister();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();
        $dataSignupRegister = new dataSignupRegister();

        $postFields = $signupRegister->getPostFieldsForMaxPassword();
        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $signupRegister->getRequestOptions($postFields, $token);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataSignupRegister->getInvalidEmailMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->data->user_id));

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->emailError));
        $this->assertEquals($json_response->errors->emailError, $dataSignupRegister->getInvalidPasswordErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->passwordError));
        $this->assertEquals($json_response->errors->passwordError, $dataSignupRegister->getMaxPasswordPasswordErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->cpasswordError));
        $this->assertEquals($json_response->errors->cpasswordError, $dataSignupRegister->getMaxPasswordCPasswordErrorMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testSignupRegisterWithInvalidPasswordNumberAndCapital()
    {
        $postAPI = new postAPI();
        $signupRegister = new signupRegister();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();
        $dataSignupRegister = new dataSignupRegister();

        $postFields = $signupRegister->getPostFieldsForNumberAndCapitalPassword();
        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $signupRegister->getRequestOptions($postFields, $token);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataSignupRegister->getInvalidEmailMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->data->user_id));

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->emailError));
        $this->assertEquals($json_response->errors->emailError, $dataSignupRegister->getInvalidPasswordErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->passwordError));
        $this->assertEquals($json_response->errors->passwordError, $dataSignupRegister->getNumberAndCapitalPasswordErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->cpasswordError));
        $this->assertEquals($json_response->errors->cpasswordError, $dataSignupRegister->getInvalidEmailCPasswordErrorMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testSignupRegisterWithInvalidPasswordSmallAndSpecial()
    {
        $postAPI = new postAPI();
        $signupRegister = new signupRegister();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();
        $dataSignupRegister = new dataSignupRegister();

        $postFields = $signupRegister->getPostFieldsForSmallAndSpecialPassword();
        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $signupRegister->getRequestOptions($postFields, $token);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataSignupRegister->getInvalidEmailMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->data->user_id));

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->emailError));
        $this->assertEquals($json_response->errors->emailError, $dataSignupRegister->getInvalidPasswordErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->passwordError));
        $this->assertEquals($json_response->errors->passwordError, $dataSignupRegister->getSmallAndSpecialPasswordErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->cpasswordError));
        $this->assertEquals($json_response->errors->cpasswordError, $dataSignupRegister->getInvalidEmailCPasswordErrorMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testSignupRegisterWithPasswordAndCPasswordNotSame()
    {
        $postAPI = new postAPI();
        $signupRegister = new signupRegister();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();
        $dataSignupRegister = new dataSignupRegister();

        $postFields = $signupRegister->getPostFieldsForPasswordAndCPasswordNotSame();
        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $signupRegister->getRequestOptions($postFields, $token);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataSignupRegister->getInvalidEmailMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->data->user_id));

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->emailError));
        $this->assertEquals($json_response->errors->emailError, $dataSignupRegister->getInvalidPasswordErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->passwordError));
        $this->assertEquals($json_response->errors->passwordError, $dataSignupRegister->getInvalidEmailPasswordErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->cpasswordError));
        $this->assertEquals($json_response->errors->cpasswordError, $dataSignupRegister->getPasswordAndCPasswordNotSameCPasswordErrorMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testSignupRegisterWithPasswordAndCPasswordBlank()
    {
        $postAPI = new postAPI();
        $signupRegister = new signupRegister();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();
        $dataSignupRegister = new dataSignupRegister();

        $postFields = $signupRegister->getPostFieldsForPasswordAndCPasswordBlank();
        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $signupRegister->getRequestOptions($postFields, $token);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataSignupRegister->getInvalidEmailMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->data->user_id));

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->emailError));
        $this->assertEquals($json_response->errors->emailError, $dataSignupRegister->getInvalidPasswordErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->passwordError));
        $this->assertEquals($json_response->errors->passwordError, $dataSignupRegister->getBlankPasswordErrorMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->cpasswordError));
        $this->assertEquals($json_response->errors->cpasswordError, $dataSignupRegister->getBlankCPasswordErrorMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testSignupRegisterWithInvalidToken()
    {
        $postAPI = new postAPI();
        $signupRegister = new signupRegister();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();
        $dataSignupRegister = new dataSignupRegister();

        $postFields = $signupRegister->getPostFieldsForValidAllData();
        $token = $dataCreateApiUrl->getClientpublickey() . 'test';
        $requestOptions = $signupRegister->getRequestOptions($postFields, $token);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataSignupRegister->getInvalidTokenMessage());

        $this->assertTrue($checkDataType->checkIntData($json_response->error_code));

        $this->assertEquals($json_response->error_code, $dataSignupRegister->getInvalidTokenErrorCode());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

}
