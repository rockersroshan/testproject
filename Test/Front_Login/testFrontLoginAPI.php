<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

include_once("Function/callAPI/postAPI.php");
include_once("src/Front_Login/frontLogin.php");
include_once("Data/URL/dataCreateApiUrl.php");
include_once("Verification/checkDataType.php");
include_once("Data/Front_Login/dataFrontLogin.php");

use PHPUnit\Framework\TestCase;

class testFrontLoginAPI extends TestCase
{

    /**
     * @Description : This API test case for the valid user name and valid password.
     */
    public function testFrontUserAPIWithValidData()
    {
        $postAPI = new postAPI();
        $frontLogin = new frontLogin();
        $checkDataType = new checkDataType();
        $dataFrontLogin = new dataFrontLogin();

        $postFields = $frontLogin->getPostFields();
        $requestOptions = $frontLogin->getRequestOptions($postFields);
        $responce = $postAPI->callPostAPI($requestOptions);

        $json_responce = json_decode($responce);

        $this->assertEquals($json_responce->info->http_code, $dataFrontLogin->getOkResponseCode());

        $this->assertTrue($checkDataType->checkStringData($json_responce->data->token));

        $this->assertTrue($checkDataType->checkStringData($json_responce->signup_force_redirect));

        $this->assertTrue($checkDataType->checkIntData($json_responce->data->user_id));

        $this->assertEquals(strtolower($json_responce->data->first_name), strtolower($dataFrontLogin->getFirstName()));

        $this->assertTrue($checkDataType->checkStringData($json_responce->data->first_name));

        $this->assertTrue($checkDataType->checkStringData($json_responce->data->last_name));

        $this->assertEquals(strtolower($json_responce->data->last_name), strtolower($dataFrontLogin->getLastName()));

        $this->assertTrue($checkDataType->checkIntData($json_responce->data->user_type));

        $this->assertTrue($checkDataType->checkStringData($json_responce->data->email));

        $this->assertTrue($checkDataType->checkStringData($json_responce->data->registration_status));

        $this->assertTrue($checkDataType->checkStringData($json_responce->data->timezone));

        $this->assertTrue($checkDataType->checkStringData($json_responce->data->image));

        $this->assertEquals($json_responce->data->email, $dataFrontLogin->getUserName());

        $this->assertTrue($checkDataType->checkStringData($json_responce->message));

        $this->assertEquals($json_responce->message, "");

        $this->assertTrue($checkDataType->checkIntData($json_responce->error_code));

        $this->assertEquals($json_responce->error_code, 0);

        echo "\n\n Response Time : " . $json_responce->info->total_time;
    }

    /**
     * @Description : This test case for the blank user name.
     */
    public function testFrontUserAPIWithBlankUserName()
    {
        $postAPI = new postAPI();
        $frontLogin = new frontLogin();
        $checkDataType = new checkDataType();
        $dataFrontLogin = new dataFrontLogin();

        $postFields = $frontLogin->getPostFieldsForBlankEmail();
        $requestOptions = $frontLogin->getRequestOptions($postFields);
        $responce = $postAPI->callPostAPI($requestOptions);

        $json_responce = json_decode($responce);

        $this->assertEquals($json_responce->info->http_code, $dataFrontLogin->getOkResponseCode());


        $this->assertTrue($checkDataType->checkStringData($json_responce->data->token));

        $this->assertFalse($json_responce->success);

        $this->assertTrue($checkDataType->checkStringData($json_responce->message));

        $this->assertEquals(strtolower($json_responce->message), strtolower($dataFrontLogin->getParameterNotDefindMessage()));

        $this->assertTrue($checkDataType->checkIntData($json_responce->error_code));

        $this->assertEquals(strtolower($json_responce->error_code), strtolower($dataFrontLogin->getErrorCodeWhenParameterNotDefind()));

        echo "\n\n Response Time : " . $json_responce->info->total_time;
    }

    /**
     * @Description : This test case for the blank password.
     */
    public function testFrontUserAPIWithBlankPassword()
    {
        $postAPI = new postAPI();
        $frontLogin = new frontLogin();
        $checkDataType = new checkDataType();
        $dataFrontLogin = new dataFrontLogin();

        $postFields = $frontLogin->getPostFieldsForBlankPassword();
        $requestOptions = $frontLogin->getRequestOptions($postFields);
        $responce = $postAPI->callPostAPI($requestOptions);

        $json_responce = json_decode($responce);

        $this->assertEquals($json_responce->info->http_code, $dataFrontLogin->getOkResponseCode());

        $this->assertTrue($checkDataType->checkStringData($json_responce->data->token));

        $this->assertFalse($json_responce->success);

        $this->assertTrue($checkDataType->checkStringData($json_responce->message));

        $this->assertEquals(strtolower($json_responce->message), strtolower($dataFrontLogin->getParameterNotDefindMessage()));

        $this->assertTrue($checkDataType->checkIntData($json_responce->error_code));

        $this->assertEquals(strtolower($json_responce->error_code), strtolower($dataFrontLogin->getErrorCodeWhenParameterNotDefind()));

        echo "\n\n Response Time : " . $json_responce->info->total_time;
    }

    /**
     * @Description : This test case for the invalid email.
     */
    public function testFrontUserAPIWithInvalidEmail()
    {
        $postAPI = new postAPI();
        $frontLogin = new frontLogin();
        $checkDataType = new checkDataType();
        $dataFrontLogin = new dataFrontLogin();

        $postFields = $frontLogin->getPostFieldsForInvalidEmailAddress();
        $requestOptions = $frontLogin->getRequestOptions($postFields);
        $responce = $postAPI->callPostAPI($requestOptions);

        echo $responce;

        $json_responce = json_decode($responce);

        $this->assertEquals($json_responce->info->http_code, $dataFrontLogin->getOkResponseCode());

        $this->assertTrue($checkDataType->checkStringData($json_responce->data->token));

        $this->assertFalse($json_responce->success);

        $this->assertTrue($checkDataType->checkStringData($json_responce->message));

        $this->assertEquals(strtolower($json_responce->message), strtolower($dataFrontLogin->getInvalidParameterValueMessage()));

        $this->assertTrue($checkDataType->checkIntData($json_responce->error_code));

        $this->assertEquals(strtolower($json_responce->error_code), strtolower($dataFrontLogin->getErrorCodeWhenInvalidParameterValueDefind()));

        echo "\n\n Response Time : " . $json_responce->info->total_time;
    }

    /**
     * @Description : This test case for the invalid password.
     */
    public function testFrontUserAPIWithInvalidPassword()
    {
        $postAPI = new postAPI();
        $frontLogin = new frontLogin();
        $checkDataType = new checkDataType();
        $dataFrontLogin = new dataFrontLogin();

        $postFields = $frontLogin->getPostFieldsForInvalidPasswordAddress();
        $requestOptions = $frontLogin->getRequestOptions($postFields);
        $responce = $postAPI->callPostAPI($requestOptions);

        $json_responce = json_decode($responce);

        $this->assertEquals($json_responce->info->http_code, $dataFrontLogin->getOkResponseCode());

        $this->assertTrue($checkDataType->checkStringData($json_responce->data->token));

        $this->assertFalse($json_responce->success);

        $this->assertTrue($checkDataType->checkStringData($json_responce->message));

        $this->assertEquals(strtolower($json_responce->message), strtolower($dataFrontLogin->getInvalidParameterValueMessage()));

        $this->assertTrue($checkDataType->checkIntData($json_responce->error_code));

        $this->assertEquals(strtolower($json_responce->error_code), strtolower($dataFrontLogin->getErrorCodeWhenInvalidParameterValueDefind()));

        echo "\n\n Response Time : " . $json_responce->info->total_time;
    }

    /**
     * @Description : This test case for the not register email address.
     */
    public function testFrontUserAPIWithNotRegisterEmailAddress()
    {
        $postAPI = new postAPI();
        $frontLogin = new frontLogin();
        $checkDataType = new checkDataType();
        $dataFrontLogin = new dataFrontLogin();

        $postFields = $frontLogin->getPostFieldsForNotRegisterEmailAddress();
        $requestOptions = $frontLogin->getRequestOptions($postFields);
        $responce = $postAPI->callPostAPI($requestOptions);

        $json_responce = json_decode($responce);

        $this->assertEquals($json_responce->info->http_code, $dataFrontLogin->getOkResponseCode());

        $this->assertTrue($checkDataType->checkStringData($json_responce->data->token));

        $this->assertFalse($json_responce->success);

        $this->assertTrue($checkDataType->checkStringData($json_responce->message));

        $this->assertEquals(strtolower($json_responce->message), strtolower($dataFrontLogin->getInvalidParameterValueMessage()));

        $this->assertTrue($checkDataType->checkIntData($json_responce->error_code));

        $this->assertEquals(strtolower($json_responce->error_code), strtolower($dataFrontLogin->getErrorCodeWhenInvalidParameterValueDefind()));

        echo "\n\n Response Time : " . $json_responce->info->total_time;
    }
}
