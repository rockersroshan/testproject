<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

use PHPUnit\Framework\TestCase;

include_once("Function/commonFunction/commonFunction.php");
include_once("Data/Investor_Details/dataInvestorDetails.php");
include_once("src/Investor_Details/investorDetails.php");
include_once("Function/callAPI/getAPI.php");
include_once("Data/URL/dataCreateApiUrl.php");

class testInvestorDetails extends TestCase
{
    public function testInvestorDetailsWithValidData()
    {
        $commonFunction = new commonFunction();
        $dataInvestorDetails = new dataInvestorDetails();
        $investorDetails = new investorDetails();
        $getAPI = new getAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();

        $token = $commonFunction->getLoginToken($dataInvestorDetails->getUsername(), $dataInvestorDetails->getPassword());
        $requestOptions = $investorDetails->getRequestOptions($token);
        $response = $getAPI->callGETAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertTrue($json_response->success);

        $this->assertTrue($checkDataType->checkIntData($json_response->user->user_id));
        $this->assertEquals($json_response->user->user_id, $dataInvestorDetails->getUserIDValid());

        $this->assertNull($json_response->user->user_name);

        $this->assertNull($json_response->user->last_name);

        $this->assertTrue($checkDataType->checkStringData($json_response->user->email));
        $this->assertEquals($json_response->user->email, $dataInvestorDetails->getUsername());

        $this->assertNull($json_response->user->phone);

        $this->assertNull($json_response->user->mobile);

        $this->assertNull($json_response->user->image);

        $this->assertNull($json_response->user->gender);

        $this->assertNull($json_response->user->address);

        $this->assertNull($json_response->user->street_address);

        $this->assertNull($json_response->user->city);

        $this->assertNull($json_response->user->state);

        $this->assertNull($json_response->user->country);

        $this->assertNull($json_response->user->nationality);

        $this->assertTrue($checkDataType->checkIntData($json_response->user->active));
        $this->assertEquals($json_response->user->active, $dataInvestorDetails->getActiveValid());

        $this->assertNull($json_response->user->user_about);

        $this->assertNull($json_response->user->user_website);

        $this->assertNull($json_response->user->user_occupation);

        $this->assertNull($json_response->user->user_interest);

        $this->assertNull($json_response->user->user_skill);

        $this->assertNull($json_response->user->facebook_url);

        $this->assertNull($json_response->user->twitter_url);

        $this->assertNull($json_response->user->linkedln_url);

        $this->assertNull($json_response->user->googleplus_url);

        $this->assertNull($json_response->user->bandcamp_url);

        $this->assertNull($json_response->user->youtube_url);

        $this->assertNull($json_response->user->myspace_url);

        $this->assertNull($json_response->user->image_no);

        $this->assertTrue($checkDataType->checkIntData($json_response->user->profile_slug));

        $this->assertNull($json_response->user->date_of_birth);

        $this->assertTrue($checkDataType->checkStringData($json_response->user->timezone));
        $this->assertEquals($json_response->user->timezone, $dataInvestorDetails->getTimezoneValid());

        $this->assertNull($json_response->user->last_activity);

        $this->assertNull($json_response->user->professional_overview);

        $this->assertNull($json_response->user->website);

        $this->assertNull($json_response->user->bio);

        $this->assertNull($json_response->user->id);

        $this->assertNull($json_response->user->legal_name);

        $this->assertNull($json_response->user->ssn_net_income);

        $this->assertNull($json_response->user->ssn_net_worth);

        $this->assertNull($json_response->user->acknowledgement_1);

        $this->assertNull($json_response->user->acknowledgement_2);

        $this->assertNull($json_response->user->acknowledgement_3);

        $this->assertNull($json_response->user->acknowledgement_4);

        $this->assertNull($json_response->user->photo);

        $this->assertNull($json_response->user->quick_bio);

        $this->assertNull($json_response->user->financial_net_worth);

        $this->assertNull($json_response->user->financial_annual_income);

        $this->assertNull($json_response->user->account_number);

        $this->assertNull($json_response->user->routing_info);

        $this->assertNull($json_response->user->social_security_number);

        $this->assertNull($json_response->user->net_income);

        $this->assertNull($json_response->user->net_worth);

        $this->assertNull($json_response->user->is_ssn);

        $this->assertNull($json_response->user->ssn);

        $this->assertNull($json_response->user->is_us_citizen);

        $this->assertNull($json_response->user->is_us_resident);

        $this->assertNull($json_response->user->status_profile);

        $this->assertNull($json_response->user->is_legal_entity);

        $this->assertNull($json_response->user->legal_entities);

        $this->assertNull($json_response->user->confidential);

        $this->assertNull($json_response->user->risk);

        $this->assertNull($json_response->user->no_return);

        $this->assertNull($json_response->user->no_advice);

        $this->assertNull($json_response->user->terms_and_conditions);

        $this->assertNull($json_response->user->user_agreement);

        $this->assertNull($json_response->user->investor_agreement);

        $this->assertNull($json_response->user->risk_disclosure);

        $this->assertNull($json_response->user->electronic_consent_notice);

        $this->assertNull($json_response->user->electronic_delivery_notice);

        $this->assertNull($json_response->user->educational_materials);

        $this->assertNull($json_response->user->compensation_disclosure);

        $this->assertNull($json_response->user->privacy_policy);

        $this->assertNull($json_response->user->employment_status);

        $this->assertNull($json_response->user->job_title);

        $this->assertNull($json_response->user->occupation);

        $this->assertNull($json_response->user->employer);

        $this->assertNull($json_response->user->return_timeframe);

        $this->assertNull($json_response->user->past_investments);

        $this->assertNull($json_response->user->past_investments_type);

        $this->assertNull($json_response->user->past_experience_investments);

        $this->assertNull($json_response->user->affinites);

        $this->assertNull($json_response->user->accredited);

        $this->assertNull($json_response->user->accreditation_income);

        $this->assertNull($json_response->user->expect_this_year);

        $this->assertNull($json_response->user->household_income_expect_this_year);

        $this->assertNull($json_response->user->jointly_net_worth);

        $this->assertTrue($checkDataType->checkIntData($json_response->user->direct_message_alert));
        $this->assertEquals($json_response->user->direct_message_alert, $dataInvestorDetails->getDirectMessageAlertValid());

        $this->assertTrue($checkDataType->checkIntData($json_response->user->replies_alert));
        $this->assertEquals($json_response->user->replies_alert, $dataInvestorDetails->getRepliesAlertValid());

        $this->assertTrue($checkDataType->checkIntData($json_response->user->company_live_session_alert));
        $this->assertEquals($json_response->user->company_live_session_alert, $dataInvestorDetails->getCompanyLiveSessionAlertValid());

        $this->assertTrue($checkDataType->checkIntData($json_response->user->company_funding_round_alert));
        $this->assertEquals($json_response->user->company_funding_round_alert, $dataInvestorDetails->getCompanyFundingRoundAlertValid());

        $this->assertTrue($checkDataType->checkIntData($json_response->user->company_financial_results_alert));
        $this->assertEquals($json_response->user->company_financial_results_alert, $dataInvestorDetails->getCompanyFinancialResultsAlertValid());

        $this->assertTrue($checkDataType->checkIntData($json_response->user->company_update_alert));
        $this->assertEquals($json_response->user->company_update_alert, $dataInvestorDetails->getCompanyUpdateAlertValid());

        $this->assertTrue($checkDataType->checkIntData($json_response->user->company_hits_milestone_alert));
        $this->assertEquals($json_response->user->company_hits_milestone_alert, $dataInvestorDetails->getCompanyHitsMilestoneAlertValid());

        $this->assertTrue($checkDataType->checkIntData($json_response->user->company_updates_profile_alert));
        $this->assertEquals($json_response->user->company_updates_profile_alert, $dataInvestorDetails->getCompanyUpdatesProfileAlertValid());

        $this->assertTrue($checkDataType->checkIntData($json_response->user->company_answers_question_alert));
        $this->assertEquals($json_response->user->company_answers_question_alert, $dataInvestorDetails->getCompanyAnswersQuestionAlertValid());

        $this->assertTrue($checkDataType->checkIntData($json_response->user->important_updates_alert));
        $this->assertEquals($json_response->user->important_updates_alert, $dataInvestorDetails->getImportantUpdatesAlertValid());

        $this->assertTrue($checkDataType->checkIntData($json_response->user->featured_startups_alert));
        $this->assertEquals($json_response->user->featured_startups_alert, $dataInvestorDetails->getFeaturedStartupsAlertValid());

        $this->assertTrue($checkDataType->checkIntData($json_response->user->companies_affinity_criteria_alert));
        $this->assertEquals($json_response->user->companies_affinity_criteria_alert, $dataInvestorDetails->getCompaniesAffinityCriteriaAlertValid());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testInvestorDetailsWithInvalidToken()
    {
        $commonFunction = new commonFunction();
        $dataInvestorDetails = new dataInvestorDetails();
        $investorDetails = new investorDetails();
        $getAPI = new getAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();

        $token = $commonFunction->getLoginToken($dataInvestorDetails->getUsername(), $dataInvestorDetails->getPassword()) . 'test';
        $requestOptions = $investorDetails->getRequestOptions($token);
        $response = $getAPI->callGETAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataInvestorDetails->getInvalidTokenMessage());

        $this->assertTrue($checkDataType->checkIntData($json_response->error_code));
        $this->assertEquals($json_response->error_code, $dataInvestorDetails->getErrorCodeForInvalidToken());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }
}
