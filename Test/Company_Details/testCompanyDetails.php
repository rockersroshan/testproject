<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

use PHPUnit\Framework\TestCase;

include_once("src/Company_Details/companyDetails.php");
include_once("Function/commonFunction/commonFunction.php");
include_once("Data/Company_Details/dataCompanyDetails.php");
include_once("Function/callAPI/getAPI.php");
include_once("Data/URL/dataCreateApiUrl.php");
include_once("Verification/checkDataType.php");

class testCompanyDetails extends TestCase
{
    public function testCompanyDetailsWithValidData()
    {
        $companyDetails = new companyDetails();
        $commonFunction = new commonFunction();
        $dataCompanyDetails = new dataCompanyDetails();
        $getAPI = new getAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();

        $token = $commonFunction->getLoginToken($dataCompanyDetails->getUsername(), $dataCompanyDetails->getPassword());
        $requestOptions = $companyDetails->getRequestOptions($token);
        $response = $getAPI->callGETAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertTrue($json_response->is_success);

        $dataCount = count($json_response->error);
        $this->assertEquals($dataCount, 0);

        $this->assertTrue($checkDataType->checkStringData($json_response->currency_symbol));

        $this->assertTrue($checkDataType->checkIntData($json_response->company_extra_detail->id));

        $this->assertTrue($checkDataType->checkIntData($json_response->company_extra_detail->company_id));

        $this->assertTrue($checkDataType->checkIntData($json_response->company_extra_detail->zip_code));

        $this->assertTrue($checkDataType->checkIntData($json_response->company_extra_detail->low_cost_provider));

        $this->assertTrue($checkDataType->checkIntData($json_response->company_extra_detail->value_added_provider));

        $this->assertTrue($checkDataType->checkIntData($json_response->company_extra_detail->perceived_value_added_provider));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_extra_detail->address));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_extra_detail->city));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_extra_detail->state));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_extra_detail->country));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_extra_detail->litigation));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_extra_detail->intellectual_property));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_extra_detail->patent_receive));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_extra_detail->patent_pending));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_extra_detail->patent_file));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_extra_detail->copyright));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_extra_detail->trademark));


        $this->assertTrue($checkDataType->checkIntData($json_response->user->user_id));

        $this->assertTrue($checkDataType->checkIntData($json_response->user->user_type));

        $this->assertTrue($checkDataType->checkIntData($json_response->user->user_language_id));

        $this->assertTrue($checkDataType->checkIntData($json_response->user->active));

        $this->assertTrue($checkDataType->checkIntData($json_response->user->enable_facebook_stream));

        $this->assertTrue($checkDataType->checkIntData($json_response->user->enable_twitter_stream));

        $this->assertTrue($checkDataType->checkIntData($json_response->user->profile_slug));

        $this->assertTrue($checkDataType->checkIntData($json_response->user->sub_type));

        $this->assertTrue($checkDataType->checkIntData($json_response->user->additional_public_information));

        $this->assertTrue($checkDataType->checkIntData($json_response->user->business_public_information));

        $this->assertTrue($checkDataType->checkIntData($json_response->user->assets_public_information));

        $this->assertTrue($checkDataType->checkIntData($json_response->user->competitive_advantage_public_information));

        $this->assertTrue($checkDataType->checkIntData($json_response->user->financial_public_information));

        $this->assertTrue($checkDataType->checkIntData($json_response->user->valuation_public_information));

        $this->assertTrue($checkDataType->checkIntData($json_response->user->fundraising_public_information));

        $this->assertTrue($checkDataType->checkIntData($json_response->user->service_public_information));

        $this->assertTrue($checkDataType->checkIntData($json_response->user->litigation_public_information));

        $this->assertTrue($checkDataType->checkIntData($json_response->user->other_personnel_public_information));

        $this->assertTrue($checkDataType->checkIntData($json_response->user->entrepreneur_id));

        $this->assertTrue($checkDataType->checkIntData($json_response->user->ancillary_public_information));

        $this->assertTrue($checkDataType->checkStringData($json_response->user->user_name));

        $this->assertTrue($checkDataType->checkStringData($json_response->user->last_name));

        $this->assertTrue($checkDataType->checkStringData($json_response->user->email));

        $this->assertTrue($checkDataType->checkStringData($json_response->user->date_added));

        $this->assertTrue($checkDataType->checkStringData($json_response->user->unique_code));

        $this->assertTrue($checkDataType->checkStringData($json_response->user->timezone));

        $this->assertTrue($checkDataType->checkStringData($json_response->user->last_activity));

        $this->assertTrue($checkDataType->checkStringData($json_response->user->legal_name));


        $this->assertTrue($checkDataType->checkIntData($json_response->company_profile->company_id));

        $this->assertTrue($checkDataType->checkIntData($json_response->company_profile->jurisdiction_incorporation));

        $this->assertTrue($checkDataType->checkIntData($json_response->company_profile->company_category_id));

        $this->assertTrue($checkDataType->checkIntData($json_response->company_profile->user_id));

        $this->assertTrue($checkDataType->checkIntData($json_response->company_profile->phone_number));

        $this->assertTrue($checkDataType->checkIntData($json_response->company_profile->company_size));

        $this->assertTrue($checkDataType->checkIntData($json_response->company_profile->further_board_appointment));

        $this->assertTrue($checkDataType->checkIntData($json_response->company_profile->authorized_behalf_company));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->company_name));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->website_url));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->company_url));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->headquater_country));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->company_logo));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->company_email));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->company_linkedin_url));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->company_facebook_url));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->company_twitter_url));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->company_instagram_url));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->video_url));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->facebook_pixel_id));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->google_analytic_code));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->business_history));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->role_in_company));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->business_story));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->customer));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->partnership));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->competitor));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->business_revenue_model));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->results_of_operation));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->financial_milestone));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->liquidity_and_capital_resource));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->related_party_transaction));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->idebtedness));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->use_of_proceed));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->financial_review_end_date));

        $this->assertTrue($checkDataType->checkStringData($json_response->company_profile->created));


        $this->assertTrue($checkDataType->checkIntData($json_response->equity->equity_id));

        $this->assertTrue($checkDataType->checkIntData($json_response->equity->user_id));

        $this->assertTrue($checkDataType->checkIntData($json_response->equity->terms_service));

        $this->assertTrue($checkDataType->checkIntData($json_response->equity->active_cnt));

        $this->assertTrue($checkDataType->checkIntData($json_response->equity->company_fundraising));

        $this->assertTrue($checkDataType->checkIntData($json_response->equity->executive_summary_status));

        $this->assertTrue($checkDataType->checkIntData($json_response->equity->term_sheet_status));

        $this->assertTrue($checkDataType->checkIntData($json_response->equity->status));

        $this->assertTrue($checkDataType->checkIntData($json_response->equity->is_featured));

        $this->assertTrue($checkDataType->checkIntData($json_response->equity->view_counter));

        $this->assertTrue($checkDataType->checkIntData($json_response->equity->save_the_reason));

        $this->assertTrue($checkDataType->checkIntData($json_response->equity->save_the_reason_inactive));

        $this->assertTrue($checkDataType->checkIntData($json_response->equity->fund_inactive_note));

        $this->assertTrue($checkDataType->checkIntData($json_response->equity->reason_save_aceess));

        $this->assertTrue($checkDataType->checkIntData($json_response->equity->reason_save_interest));

        $this->assertTrue($checkDataType->checkIntData($json_response->equity->return_investment_interest));

        $this->assertTrue($checkDataType->checkIntData($json_response->equity->return_term_length));

        $this->assertTrue($checkDataType->checkIntData($json_response->equity->is_deal_detail));

        $this->assertTrue($checkDataType->checkStringData($json_response->equity->host_ip));

        $this->assertTrue($checkDataType->checkStringData($json_response->equity->date_added));

        $this->assertTrue($checkDataType->checkStringData($json_response->equity->amount_get));

        $this->assertTrue($checkDataType->checkStringData($json_response->equity->equity_currency_code));

        $this->assertTrue($checkDataType->checkStringData($json_response->equity->equity_currency_symbol));

        $this->assertTrue($checkDataType->checkStringData($json_response->equity->end_date));

        $this->assertTrue($checkDataType->checkStringData($json_response->equity->date_round_open));

        $this->assertTrue($checkDataType->checkStringData($json_response->equity->allowed_investor));

        $this->assertTrue($checkDataType->checkStringData($json_response->equity->tax_relief_type));

        $this->assertTrue($checkDataType->checkStringData($json_response->equity->pre_money_valuation));

        $this->assertTrue($checkDataType->checkStringData($json_response->equity->valuation_detail));

        $this->assertTrue($checkDataType->checkStringData($json_response->equity->sales_last_year));

        $this->assertTrue($checkDataType->checkStringData($json_response->equity->short_pitch));

        $this->assertTrue($checkDataType->checkStringData($json_response->equity->pitch_press_article));

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testCompanyDetailsWithInvalidData()
    {
        $companyDetails = new companyDetails();
        $commonFunction = new commonFunction();
        $dataCompanyDetails = new dataCompanyDetails();
        $getAPI = new getAPI();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $checkDataType = new checkDataType();

        $token = $commonFunction->getLoginToken($dataCompanyDetails->getUsername(), $dataCompanyDetails->getPassword()) . 'test';
        $requestOptions = $companyDetails->getRequestOptions($token);
        $response = $getAPI->callGETAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataCompanyDetails->getInvalidTokenMessage());

        $this->assertTrue($checkDataType->checkIntData($json_response->error_code));
        $this->assertEquals($json_response->error_code, $dataCompanyDetails->getErrorCodeForInvalidToken());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }
}
