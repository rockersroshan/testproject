<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

include_once("src/Home_Reset_Password/homeResetPassword.php");
include_once("Function/callAPI/postAPI.php");
include_once("Data/URL/dataCreateApiUrl.php");
include_once("Data/Home_Reset_Password/dataHomeResetPassword.php");
include_once("Verification/checkDataType.php");

use PHPUnit\Framework\TestCase;

class testHomeResetPassword extends TestCase
{
    public function testHomeResetPasswordAPIWithValidData()
    {
        $homeResetPassword = new homeResetPassword();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $postAPI = new postAPI();
        $dataHomeResetPassword = new dataHomeResetPassword();
        $checkDataType = new checkDataType();

        $forgotPasswordEmailInID = $homeResetPassword->getForgotPasswordEmailInID();

        echo "\n ID = " . $forgotPasswordEmailInID;

        $postFields = $homeResetPassword->getPostFieldsForValidData($forgotPasswordEmailInID);
        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $homeResetPassword->getRequestOption($postFields, $token);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertTrue($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataHomeResetPassword->getValidDataMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testHomeResetPasswordAPIWithInvalidTokenData()
    {
        $homeResetPassword = new homeResetPassword();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $postAPI = new postAPI();
        $dataHomeResetPassword = new dataHomeResetPassword();
        $checkDataType = new checkDataType();

        $forgotPasswordEmailInID = $homeResetPassword->getForgotPasswordEmailInID();

        echo "\n ID = " . $forgotPasswordEmailInID;

        $postFields = $homeResetPassword->getPostFieldsForValidData($forgotPasswordEmailInID);
        $token = $dataCreateApiUrl->getClientpublickey() . "test";
        $requestOptions = $homeResetPassword->getRequestOption($postFields, $token);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataHomeResetPassword->getInvalidTokenMessage());

        $this->assertTrue($checkDataType->checkIntData($json_response->error_code));

        $this->assertEquals($json_response->error_code, $dataHomeResetPassword->getErrorCodeWhenInvalidToken());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testHomeResetPasswordAPIWithPasswordAndCPasswordNotSame()
    {
        $homeResetPassword = new homeResetPassword();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $postAPI = new postAPI();
        $dataHomeResetPassword = new dataHomeResetPassword();
        $checkDataType = new checkDataType();

        $forgotPasswordEmailInID = $homeResetPassword->getForgotPasswordEmailInID();

        echo "\n ID = " . $forgotPasswordEmailInID;

        $postFields = $homeResetPassword->getPostFieldsForPasswordAndCPasswordNotSame($forgotPasswordEmailInID);
        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $homeResetPassword->getRequestOption($postFields, $token);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataHomeResetPassword->getPasswordAndCPasswordNotSameMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testHomeResetPasswordAPIWithPasswordMinRange()
    {
        $homeResetPassword = new homeResetPassword();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $postAPI = new postAPI();
        $dataHomeResetPassword = new dataHomeResetPassword();
        $checkDataType = new checkDataType();

        $forgotPasswordEmailInID = $homeResetPassword->getForgotPasswordEmailInID();

        echo "\n ID = " . $forgotPasswordEmailInID;

        $postFields = $homeResetPassword->getPostFieldsForPasswordMinRange($forgotPasswordEmailInID);
        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $homeResetPassword->getRequestOption($postFields, $token);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataHomeResetPassword->getMinPasswordRangeMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testHomeResetPasswordAPIWithPasswordMaxRange()
    {
        $homeResetPassword = new homeResetPassword();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $postAPI = new postAPI();
        $dataHomeResetPassword = new dataHomeResetPassword();
        $checkDataType = new checkDataType();

        $forgotPasswordEmailInID = $homeResetPassword->getForgotPasswordEmailInID();

        echo "\n ID = " . $forgotPasswordEmailInID;

        $postFields = $homeResetPassword->getPostFieldsForPasswordMaxRange($forgotPasswordEmailInID);
        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $homeResetPassword->getRequestOption($postFields, $token);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataHomeResetPassword->getMaxPasswordRangeMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testHomeResetPasswordAPIWithPasswordBlank()
    {
        $homeResetPassword = new homeResetPassword();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $postAPI = new postAPI();
        $dataHomeResetPassword = new dataHomeResetPassword();
        $checkDataType = new checkDataType();

        $forgotPasswordEmailInID = $homeResetPassword->getForgotPasswordEmailInID();

        echo "\n ID = " . $forgotPasswordEmailInID;

        $postFields = $homeResetPassword->getPostFieldsForPasswordBlank($forgotPasswordEmailInID);
        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $homeResetPassword->getRequestOption($postFields, $token);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataHomeResetPassword->getBlankPasswordMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testHomeResetPasswordAPIWithConfirmPasswordBlank()
    {
        $homeResetPassword = new homeResetPassword();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $postAPI = new postAPI();
        $dataHomeResetPassword = new dataHomeResetPassword();
        $checkDataType = new checkDataType();

        $forgotPasswordEmailInID = $homeResetPassword->getForgotPasswordEmailInID();

        echo "\n ID = " . $forgotPasswordEmailInID;

        $postFields = $homeResetPassword->getPostFieldsForConfirmPasswordBlank($forgotPasswordEmailInID);
        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $homeResetPassword->getRequestOption($postFields, $token);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataHomeResetPassword->getBlankConfirmPasswordMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testHomeResetPasswordAPIWithCodeBlank()
    {
        $homeResetPassword = new homeResetPassword();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $postAPI = new postAPI();
        $dataHomeResetPassword = new dataHomeResetPassword();
        $checkDataType = new checkDataType();

        $postFields = $homeResetPassword->getPostFieldsForCodeBlank();
        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $homeResetPassword->getRequestOption($postFields, $token);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataHomeResetPassword->getBlankCodeMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testHomeResetPasswordAPIWithInvalidCode()
    {
        $homeResetPassword = new homeResetPassword();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $postAPI = new postAPI();
        $dataHomeResetPassword = new dataHomeResetPassword();
        $checkDataType = new checkDataType();

        $forgotPasswordEmailInID = $homeResetPassword->getForgotPasswordEmailInID() . "test";

        echo "\n ID = " . $forgotPasswordEmailInID;

        $postFields = $homeResetPassword->getPostFieldsForValidData($forgotPasswordEmailInID);
        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $homeResetPassword->getRequestOption($postFields, $token);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataHomeResetPassword->getInvalidCodeMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testHomeResetPasswordAPIWithOnlyNumberAndCapital()
    {
        $homeResetPassword = new homeResetPassword();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $postAPI = new postAPI();
        $dataHomeResetPassword = new dataHomeResetPassword();
        $checkDataType = new checkDataType();

        $forgotPasswordEmailInID = $homeResetPassword->getForgotPasswordEmailInID();

        echo "\n ID = " . $forgotPasswordEmailInID;

        $postFields = $homeResetPassword->getPostFieldsForOnlyNumberAndCapital($forgotPasswordEmailInID);
        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $homeResetPassword->getRequestOption($postFields, $token);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataHomeResetPassword->getOnlyNumberAndCapitalMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testHomeResetPasswordAPIWithOnlySmallAndSpecial()
    {
        $homeResetPassword = new homeResetPassword();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $postAPI = new postAPI();
        $dataHomeResetPassword = new dataHomeResetPassword();
        $checkDataType = new checkDataType();

        $forgotPasswordEmailInID = $homeResetPassword->getForgotPasswordEmailInID();

        echo "\n ID = " . $forgotPasswordEmailInID;

        $postFields = $homeResetPassword->getPostFieldsOnlySmallAndSpecial($forgotPasswordEmailInID);
        $token = $dataCreateApiUrl->getClientpublickey();
        $requestOptions = $homeResetPassword->getRequestOption($postFields, $token);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataHomeResetPassword->getOnlySmallAndSpecialMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }
}
