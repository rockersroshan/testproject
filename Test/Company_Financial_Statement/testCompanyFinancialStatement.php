<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

use PHPUnit\Framework\TestCase;

include_once("src/Company_Financial_Statement/companyFinancialStatement.php");
include_once("Data/URL/dataCreateApiUrl.php");
include_once("Data/Company_Financial_Statement/dataCompanyFinancialStatement.php");
include_once("Verification/checkDataType.php");
include_once("Function/commonFunction/commonFunction.php");
include_once("Function/callAPI/postAPI.php");

class testCompanyFinancialStatement extends TestCase
{
    public function testCompanyFinancialStatementWithValidData()
    {
        $companyFinancialStatement = new companyFinancialStatement();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $dataCompanyFinancialStatement = new dataCompanyFinancialStatement();
        $checkDataType = new checkDataType();
        $commonFunction = new commonFunction();
        $postAPI = new postAPI();

        $postFields = $companyFinancialStatement->getPostFieldsForValidData();
        $token = $commonFunction->getLoginToken($dataCompanyFinancialStatement->getUsername(), $dataCompanyFinancialStatement->getPassword());
        $requestOptions = $companyFinancialStatement->getRequestOptions($token, $postFields);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertTrue($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->file_path));
        $this->assertContains($dataCompanyFinancialStatement->getFileUploadValidURL(), $json_response->file_path);

        $this->assertTrue($checkDataType->checkIntData($json_response->financial_statement_id));

        $dataCount = count($json_response->errors);
        $this->assertEquals($dataCount, 0);

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testCompanyFinancialStatementWithInvalidFile3()
    {
        $companyFinancialStatement = new companyFinancialStatement();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $dataCompanyFinancialStatement = new dataCompanyFinancialStatement();
        $checkDataType = new checkDataType();
        $commonFunction = new commonFunction();
        $postAPI = new postAPI();

        $postFields = $companyFinancialStatement->getPostFieldsForInvalidFile();
        $token = $commonFunction->getLoginToken($dataCompanyFinancialStatement->getUsername(), $dataCompanyFinancialStatement->getPassword());
        $requestOptions = $companyFinancialStatement->getRequestOptions($token, $postFields);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->file_path));
        $this->assertEquals($json_response->file_path, $dataCompanyFinancialStatement->getFilePathNullMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->financial_statement_id));
        $this->assertEquals($json_response->file_path, $dataCompanyFinancialStatement->getFinancialStatementIDNullMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->financial_statement_titleError));
        $this->assertEquals($json_response->errors->financial_statement_titleError, $dataCompanyFinancialStatement->getFinancialStatementTitleErrorNullMessage());

        $this->assertTrue($checkDataType->checkStringData($json_response->errors->financial_statementError));
        $this->assertEquals($json_response->errors->financial_statementError, $dataCompanyFinancialStatement->getFinancialStatementErroMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testCompanyFinancialStatementWithBlankAllData()
    {
        $companyFinancialStatement = new companyFinancialStatement();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $dataCompanyFinancialStatement = new dataCompanyFinancialStatement();
        $checkDataType = new checkDataType();
        $commonFunction = new commonFunction();
        $postAPI = new postAPI();

        $postFields = $companyFinancialStatement->getPostFieldsForBlankAllData();
        $token = $commonFunction->getLoginToken($dataCompanyFinancialStatement->getUsername(), $dataCompanyFinancialStatement->getPassword());
        $requestOptions = $companyFinancialStatement->getRequestOptions($token, $postFields);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->generalError));
        $this->assertEquals($json_response->generalError, $dataCompanyFinancialStatement->getGeneralErrorMessage());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }

    public function testCompanyFinancialStatementWithInvalidData()
    {
        $companyFinancialStatement = new companyFinancialStatement();
        $dataCreateApiUrl = new dataCreateApiUrl();
        $dataCompanyFinancialStatement = new dataCompanyFinancialStatement();
        $checkDataType = new checkDataType();
        $commonFunction = new commonFunction();
        $postAPI = new postAPI();

        $postFields = $companyFinancialStatement->getPostFieldsForBlankAllData();
        $token = $commonFunction->getLoginToken($dataCompanyFinancialStatement->getUsername(), $dataCompanyFinancialStatement->getPassword()) . 'test';
        $requestOptions = $companyFinancialStatement->getRequestOptions($token, $postFields);
        $response = $postAPI->callPostAPI($requestOptions);

        $json_response = json_decode($response);

        $this->assertEquals($json_response->info->http_code, $dataCreateApiUrl->getOkResponseCode());

        $this->assertFalse($json_response->success);

        $this->assertTrue($checkDataType->checkStringData($json_response->message));

        $this->assertEquals($json_response->message, $dataCompanyFinancialStatement->getInvalidTokenMessage());

        $this->assertTrue($checkDataType->checkIntData($json_response->error_code));
        $this->assertEquals($json_response->error_code, $dataCompanyFinancialStatement->getErrorCodeForInvalidToken());

        echo "\n\n Response Time : " . $json_response->info->total_time;
    }
}
