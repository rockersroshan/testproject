<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class dataInvestSaveInvestment
{
    protected $username = "roshan.rockersinfo@gmail.com";
    protected $password = "Roshan@123";
    protected $isUsCitizenValid = 1;
    protected $isUsResidentValid=1;
    protected $accreditedValid=1;
    protected $ssnValid=200;
    protected $netWorthValid=20000;
    protected $accreditationIncomeValid=20000;
    protected $expectThisYearValid=1;
    protected $householdIncomeExpectThisYear=1;
    protected $jointlyNetWorthValid=1;
    protected $employmentStatusValid="Employed";
    protected $jobTitleValid="test";
    protected $occupationValid="test";
    protected $employerValid="test";
    protected $successBlank = "";
    protected $isUsCitizenErrorMessage="<p>The US citizen field is required.</p>";
    protected $isUsResidentErrorMessage="<p>The US resident field is required.</p>";
    protected $accreditedErrorMessage="<p>The Plan to invest field is required.</p>";
    protected $nullStaringData="";
    protected $employmentStatusErrorMessage="<p>The Employment status field is required.</p>";
    protected $invalidTokenMessage = "There is some problem please try again.";
    protected $errorCodeForInvalidToken = 2013;

    /**
     * @return string
     */
    public function getInvalidTokenMessage()
    {
        return $this->invalidTokenMessage;
    }

    /**
     * @return int
     */
    public function getErrorCodeForInvalidToken()
    {
        return $this->errorCodeForInvalidToken;
    }

    /**
     * @return string
     */
    public function getEmploymentStatusErrorMessage()
    {
        return $this->employmentStatusErrorMessage;
    }

    /**
     * @return string
     */
    public function getNullStaringData()
    {
        return $this->nullStaringData;
    }

    /**
     * @return string
     */
    public function getIsUsCitizenErrorMessage()
    {
        return $this->isUsCitizenErrorMessage;
    }

    /**
     * @return string
     */
    public function getIsUsResidentErrorMessage()
    {
        return $this->isUsResidentErrorMessage;
    }

    /**
     * @return string
     */
    public function getAccreditedErrorMessage()
    {
        return $this->accreditedErrorMessage;
    }

    /**
     * @return string
     */
    public function getSuccessBlank()
    {
        return $this->successBlank;
    }


    /**
     * @return int
     */
    public function getIsUsCitizenValid()
    {
        return $this->isUsCitizenValid;
    }

    /**
     * @return int
     */
    public function getIsUsResidentValid()
    {
        return $this->isUsResidentValid;
    }

    /**
     * @return int
     */
    public function getAccreditedValid()
    {
        return $this->accreditedValid;
    }

    /**
     * @return int
     */
    public function getSsnValid()
    {
        return $this->ssnValid;
    }

    /**
     * @return int
     */
    public function getNetWorthValid()
    {
        return $this->netWorthValid;
    }

    /**
     * @return int
     */
    public function getAccreditationIncomeValid()
    {
        return $this->accreditationIncomeValid;
    }

    /**
     * @return int
     */
    public function getExpectThisYearValid()
    {
        return $this->expectThisYearValid;
    }

    /**
     * @return int
     */
    public function getHouseholdIncomeExpectThisYear()
    {
        return $this->householdIncomeExpectThisYear;
    }

    /**
     * @return int
     */
    public function getJointlyNetWorthValid()
    {
        return $this->jointlyNetWorthValid;
    }

    /**
     * @return string
     */
    public function getEmploymentStatusValid()
    {
        return $this->employmentStatusValid;
    }

    /**
     * @return string
     */
    public function getJobTitleValid()
    {
        return $this->jobTitleValid;
    }

    /**
     * @return string
     */
    public function getOccupationValid()
    {
        return $this->occupationValid;
    }

    /**
     * @return string
     */
    public function getEmployerValid()
    {
        return $this->employerValid;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
}