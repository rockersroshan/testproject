<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class dataHomeResetPassword
{
    protected $emailAddress = "roshan@mailinator.com";
    protected $xtokenForGetForgotPasswordID = "Kprty@9643MHKWQ753";
    protected $minRangePassword = "123";
    protected $maxRangePassword = "123456T890@234t678901";
    protected $validPassword = "Test@123";
    protected $validConfirmPassword = "Test@123";
    protected $onlyNumberAndCapitalPassword = "12345TEST";
    protected $onlySmallAndSpecialPassword = "test@$@$";
    protected $validDataMessage = "";
    protected $invalidTokenMessage = "There is some problem please try again.";
    protected $errorCodeWhenInvalidToken = 2013;
    protected $passwordAndCPasswordNotSameMessage = "<p>The Retype Password field does not match the Password field.</p>\n";
    protected $minPasswordRangeMessage = "<p>The Password field must be at least one lowercase letter.</p>\n<p>The Retype Password field must be at least 8 characters in length.</p>\n";
    protected $maxPasswordRangeMessage = "<p>The Password field cannot exceed 20 characters in length.</p>\n<p>The Retype Password field cannot exceed 20 characters in length.</p>\n";
    protected $blankPasswordMessage = "<p>The Password field is required.</p>\n<p>The Retype Password field does not match the Password field.</p>\n";
    protected $blankConfirmPasswordMessage = "<p>The Retype Password field is required.</p>\n";
    protected $blankCodeMessage = "<p>Invalid token, please try again or contact to admin.</p>";
    protected $invalidCodeMessage = "<p>Invalid token, please try again or contact to admin.</p>";
    protected $onlyNumberAndCapitalMessage ="<p>The Password field must be at least one lowercase letter.</p>\n";
    protected $onlySmallAndSpecialMessage = "<p>The Password field must be at least one uppercase letter.</p>\n";

    /**
     * @return string
     */
    public function getOnlySmallAndSpecialPassword()
    {
        return $this->onlySmallAndSpecialPassword;
    }

    /**
     * @return string
     */
    public function getOnlySmallAndSpecialMessage()
    {
        return $this->onlySmallAndSpecialMessage;
    }

    /**
     * @return string
     */
    public function getOnlyNumberAndCapitalPassword()
    {
        return $this->onlyNumberAndCapitalPassword;
    }

    /**
     * @return string
     */
    public function getOnlyNumberAndCapitalMessage()
    {
        return $this->onlyNumberAndCapitalMessage;
    }

    /**
     * @return string
     */
    public function getInvalidCodeMessage()
    {
        return $this->invalidCodeMessage;
    }

    /**
     * @return string
     */
    public function getBlankCodeMessage()
    {
        return $this->blankCodeMessage;
    }

    /**
     * @return string
     */
    public function getBlankConfirmPasswordMessage()
    {
        return $this->blankConfirmPasswordMessage;
    }

    /**
     * @return string
     */
    public function getBlankPasswordMessage()
    {
        return $this->blankPasswordMessage;
    }

    /**
     * @return string
     */
    public function getMaxRangePassword()
    {
        return $this->maxRangePassword;
    }

    /**
     * @return string
     */
    public function getMaxPasswordRangeMessage()
    {
        return $this->maxPasswordRangeMessage;
    }

    /**
     * @return string
     */
    public function getMinPasswordRangeMessage()
    {
        return $this->minPasswordRangeMessage;
    }

    /**
     * @return string
     */
    public function getMinRangePassword()
    {
        return $this->minRangePassword;
    }

    /**
     * @return string
     */
    public function getPasswordAndCPasswordNotSameMessage()
    {
        return $this->passwordAndCPasswordNotSameMessage;
    }

    /**
     * @return string
     */
    public function getInvalidTokenMessage()
    {
        return $this->invalidTokenMessage;
    }

    /**
     * @return int
     */
    public function getErrorCodeWhenInvalidToken()
    {
        return $this->errorCodeWhenInvalidToken;
    }

    /**
     * @return string
     */
    public function getValidDataMessage()
    {
        return $this->validDataMessage;
    }

    /**
     * @return string
     */
    public function getValidPassword()
    {
        return $this->validPassword;
    }

    /**
     * @return string
     */
    public function getValidConfirmPassword()
    {
        return $this->validConfirmPassword;
    }

    /**
     * @return mixed
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * @return mixed
     */
    public function getXtokenForGetForgotPasswordID()
    {
        return $this->xtokenForGetForgotPasswordID;
    }
}