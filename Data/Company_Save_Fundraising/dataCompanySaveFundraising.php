<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class dataCompanySaveFundraising
{
    protected $username = "maintestroshan@mailinator.com";
    protected $password = "Roshan@123";
    protected $fundraisingPublicInformationValidData = 1;
    protected $companyTotalInvestmentHistoryValidData = 100;
    protected $currentlyFundraisingValidData = 1;
    protected $roundRaisingValidData = 1000;
    protected $completeCurrentFundraisingValidData = "0-3 months";
    protected $fundingAmountTargetedValidData = "Up to {currency_symbol}100,000";
    protected $lengthIntendedCampaignValidData = 11;
    protected $sharesCurrentlyOustandingValidData = 12;
    protected $sharesAuthorizedIssuanceValidData = 32;
    protected $pricePerShareValidData=100;
    protected $minimumRaiseValidData=100;
    protected $maximumRaiseValidData=1000;
    protected $liquidationPreferenceValidData="<p>What is your liquidation preference</p>";
    protected $votingRightValidData="<p>Voting rights</p>";
    protected $informationRightValidData="<p>Information rights</p>";
    protected $communicationFrequencyCommitmentValidData="<p>Communication frequency commitment</p>";
    protected $additionalTermValidData="<p>Additional Terms</p>";
    protected $generalErrorMessage =  "Request parameter is missing.";
    protected $invalidTokenMessage = "There is some problem please try again.";
    protected $errorCodeForInvalidToken = 2013;

    /**
     * @return string
     */
    public function getInvalidTokenMessage()
    {
        return $this->invalidTokenMessage;
    }

    /**
     * @return int
     */
    public function getErrorCodeForInvalidToken()
    {
        return $this->errorCodeForInvalidToken;
    }

    /**
     * @return string
     */
    public function getGeneralErrorMessage()
    {
        return $this->generalErrorMessage;
    }

    /**
     * @return int
     */
    public function getFundraisingPublicInformationValidData()
    {
        return $this->fundraisingPublicInformationValidData;
    }

    /**
     * @return int
     */
    public function getCompanyTotalInvestmentHistoryValidData()
    {
        return $this->companyTotalInvestmentHistoryValidData;
    }

    /**
     * @return int
     */
    public function getCurrentlyFundraisingValidData()
    {
        return $this->currentlyFundraisingValidData;
    }

    /**
     * @return int
     */
    public function getRoundRaisingValidData()
    {
        return $this->roundRaisingValidData;
    }

    /**
     * @return string
     */
    public function getCompleteCurrentFundraisingValidData()
    {
        return $this->completeCurrentFundraisingValidData;
    }

    /**
     * @return string
     */
    public function getFundingAmountTargetedValidData()
    {
        return $this->fundingAmountTargetedValidData;
    }

    /**
     * @return int
     */
    public function getLengthIntendedCampaignValidData()
    {
        return $this->lengthIntendedCampaignValidData;
    }

    /**
     * @return int
     */
    public function getSharesCurrentlyOustandingValidData()
    {
        return $this->sharesCurrentlyOustandingValidData;
    }

    /**
     * @return int
     */
    public function getSharesAuthorizedIssuanceValidData()
    {
        return $this->sharesAuthorizedIssuanceValidData;
    }

    /**
     * @return int
     */
    public function getPricePerShareValidData()
    {
        return $this->pricePerShareValidData;
    }

    /**
     * @return int
     */
    public function getMinimumRaiseValidData()
    {
        return $this->minimumRaiseValidData;
    }

    /**
     * @return int
     */
    public function getMaximumRaiseValidData()
    {
        return $this->maximumRaiseValidData;
    }

    /**
     * @return string
     */
    public function getLiquidationPreferenceValidData()
    {
        return $this->liquidationPreferenceValidData;
    }

    /**
     * @return string
     */
    public function getVotingRightValidData()
    {
        return $this->votingRightValidData;
    }

    /**
     * @return string
     */
    public function getInformationRightValidData()
    {
        return $this->informationRightValidData;
    }

    /**
     * @return string
     */
    public function getCommunicationFrequencyCommitmentValidData()
    {
        return $this->communicationFrequencyCommitmentValidData;
    }

    /**
     * @return string
     */
    public function getAdditionalTermValidData()
    {
        return $this->additionalTermValidData;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
}