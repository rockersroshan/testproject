<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class dataCompanySaveService
{
    protected $username = "maintestroshan@mailinator.com";
    protected $password = "Roshan@123";
    protected $servicePublicInformationValidData = 1;
    protected $nameLawFirmValidData = "Roshan";
    protected $lawyerNameValidData = "Mistry";
    protected $lawFirmAddressValidData = "Shelter House 10 Access Rd, Kansas City, KS 66109, USA";
    protected $lawFirmCityValidData = "Wyandotte County";
    protected $lawFirmStateValidData = "Kansas";
    protected $lawFirmCountryValidData = "United States";
    protected $lawFirmZipCodeValidData = 66109;
    protected $lawFirmPhoneValidData = 6586958585;
    protected $lawFirmEmailValidData = "roshan.rockersinfo@gmail.com";
    protected $lawFirmWebsiteValidData = "http://rockerstech.com/thecrowd/build_company/service";
    protected $nameAccountingFirmValidData = "test";
    protected $accountantNameValidData = "test1";
    protected $accountingFirmAddressValidData = "12455 E 100th St N #300, Owasso, OK 74055, USA";
    protected $accountingFirmCityValidData = "Tulsa County";
    protected $accountingFirmStateValidData = "Oklahoma";
    protected $accountingFirmCountryValidData = "United States";
    protected $accountingFirmZipCodeValidData = 45345435;
    protected $accountingFirmPhoneValidData = 56436346346;
    protected $accountingFirmEmailValidData = "roshan.rockersinfo@gmail.com";
    protected $accountingFirmWebsiteValidData = "http://rockerstech.com/thecrowd/build_company/service";
    protected $generalErrorMessage =  "Request parameter is missing.";
    protected $invalidTokenMessage = "There is some problem please try again.";
    protected $errorCodeForInvalidToken = 2013;

    /**
     * @return string
     */
    public function getGeneralErrorMessage()
    {
        return $this->generalErrorMessage;
    }

    /**
     * @return string
     */
    public function getInvalidTokenMessage()
    {
        return $this->invalidTokenMessage;
    }

    /**
     * @return int
     */
    public function getErrorCodeForInvalidToken()
    {
        return $this->errorCodeForInvalidToken;
    }

    /**
     * @return int
     */
    public function getServicePublicInformationValidData()
    {
        return $this->servicePublicInformationValidData;
    }

    /**
     * @return string
     */
    public function getNameLawFirmValidData()
    {
        return $this->nameLawFirmValidData;
    }

    /**
     * @return string
     */
    public function getLawyerNameValidData()
    {
        return $this->lawyerNameValidData;
    }

    /**
     * @return string
     */
    public function getLawFirmAddressValidData()
    {
        return $this->lawFirmAddressValidData;
    }

    /**
     * @return string
     */
    public function getLawFirmCityValidData()
    {
        return $this->lawFirmCityValidData;
    }

    /**
     * @return string
     */
    public function getLawFirmStateValidData()
    {
        return $this->lawFirmStateValidData;
    }

    /**
     * @return string
     */
    public function getLawFirmCountryValidData()
    {
        return $this->lawFirmCountryValidData;
    }

    /**
     * @return int
     */
    public function getLawFirmZipCodeValidData()
    {
        return $this->lawFirmZipCodeValidData;
    }

    /**
     * @return int
     */
    public function getLawFirmPhoneValidData()
    {
        return $this->lawFirmPhoneValidData;
    }

    /**
     * @return string
     */
    public function getLawFirmEmailValidData()
    {
        return $this->lawFirmEmailValidData;
    }

    /**
     * @return string
     */
    public function getLawFirmWebsiteValidData()
    {
        return $this->lawFirmWebsiteValidData;
    }

    /**
     * @return string
     */
    public function getNameAccountingFirmValidData()
    {
        return $this->nameAccountingFirmValidData;
    }

    /**
     * @return string
     */
    public function getAccountantNameValidData()
    {
        return $this->accountantNameValidData;
    }

    /**
     * @return string
     */
    public function getAccountingFirmAddressValidData()
    {
        return $this->accountingFirmAddressValidData;
    }

    /**
     * @return string
     */
    public function getAccountingFirmCityValidData()
    {
        return $this->accountingFirmCityValidData;
    }

    /**
     * @return string
     */
    public function getAccountingFirmStateValidData()
    {
        return $this->accountingFirmStateValidData;
    }

    /**
     * @return string
     */
    public function getAccountingFirmCountryValidData()
    {
        return $this->accountingFirmCountryValidData;
    }

    /**
     * @return int
     */
    public function getAccountingFirmZipCodeValidData()
    {
        return $this->accountingFirmZipCodeValidData;
    }

    /**
     * @return int
     */
    public function getAccountingFirmPhoneValidData()
    {
        return $this->accountingFirmPhoneValidData;
    }

    /**
     * @return string
     */
    public function getAccountingFirmEmailValidData()
    {
        return $this->accountingFirmEmailValidData;
    }

    /**
     * @return string
     */
    public function getAccountingFirmWebsiteValidData()
    {
        return $this->accountingFirmWebsiteValidData;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
}