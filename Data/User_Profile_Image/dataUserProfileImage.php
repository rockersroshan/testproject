<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class dataUserProfileImage
{
    protected $validImagePath = "C:\\Users\\RW02\\Desktop\\icon-lending-crowdfunding.png";
    protected $invalidImagePath = "C:\\Users\\RW02\\Desktop";
    protected $invalidFormatImagePath = "C:\Users\Public\Desktop\Google Chrome.lnk";
    protected $videoPath = "D:\\Roshan\\video_06_05_02.mp4";
    protected $username = "roshan.rockersinfo@gmail.com";
    protected $password = "Roshan@123";
    protected $validImageErrors = "";
    protected $invalidFilePathHttpCode = 100;
    protected $invalidImageFormatProfileImage = "";
    protected $invalidImageFormatError = "Please upload a .jpg, .png, .gif file";
    protected $invalidTokenMessage = "There is some problem please try again.";
    protected $invalidTokenErrorCode = 2013;

    /**
     * @return string
     */
    public function getInvalidTokenMessage()
    {
        return $this->invalidTokenMessage;
    }

    /**
     * @return int
     */
    public function getInvalidTokenErrorCode()
    {
        return $this->invalidTokenErrorCode;
    }

    /**
     * @return string
     */
    public function getVideoPath()
    {
        $this->videoPath = realpath($this->videoPath);
        return $this->videoPath;
    }

    /**
     * @return string
     */
    public function getInvalidImageFormatError()
    {
        return $this->invalidImageFormatError;
    }

    /**
     * @return string
     */
    public function getInvalidImageFormatProfileImage()
    {
        return $this->invalidImageFormatProfileImage;
    }


    /**
     * @return string
     */
    public function getInvalidFormatImagePath()
    {
        $this->invalidFormatImagePath = realpath($this->invalidFormatImagePath);
        return $this->invalidFormatImagePath;
    }

    /**
     * @return string
     */
    public function getInvalidImagePath()
    {
        $this->invalidImagePath = realpath($this->invalidImagePath);
        return $this->invalidImagePath;
    }

    /**
     * @return int
     */
    public function getInvalidFilePathHttpCode()
    {
        return $this->invalidFilePathHttpCode;
    }

    /**
     * @return string
     */
    public function getValidImageErrors()
    {
        return $this->validImageErrors;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getValidImagePath()
    {
        $file_name_with_full_path = realpath($this->validImagePath);
        return $file_name_with_full_path;
    }
}