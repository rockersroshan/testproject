<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class dataInvestSaveBuildRelationship
{
    protected $username = "roshan.rockersinfo@gmail.com";
    protected $password = "Roshan@123";
    protected $dateOfBirthValid=1;
    protected $homeTownValid=1;
    protected $bornStateValid=1;
    protected $bornCountryValid=1;
    protected $birthOrderValid=1;
    protected $onlyChildValid=1;
    protected $raisedByValid=1;
    protected $orphaned=1;
    protected $parentalAubstanceAbuseValid=1;
    protected $socioEconomicConditionValid=1;
    protected $predominantEnvironmentValid=1;
    protected $employmentYouthValid=1;
    protected $numberOfJobsYouthValid=1;
    protected $mentorInYourYouthValid=1;
    protected $ethnicBackgroundValid=1;
    protected $citizenshipValid=1;
    protected $genderValid=1;
    protected $sexualOrientationValid=1;
    protected $lgbtSupporterValid=1;
    protected $educationalValid=1;
    protected $financialAidValid=1;
    protected $religiousIdentityValid=1;
    protected $maritalStatusValid=1;
    protected $singleParentValid=1;
    protected $childrensValid=1;
    protected $drugAddictionValid=1;
    protected $illegalDrugsValid=1;
    protected $physicalAbuseValid=1;
    protected $sexualAbuseValid=1;
    protected $mentalAbuseValid=1;
    protected $politicalAffiliationValid=1;
    protected $militaryExperienceValid=1;
    protected $cancerSurvivorValid=1;
    protected $chronicDiseaseSuffererValid=1;
    protected $physicalHandicapValid=1;
    protected $immigrantValid=1;
    protected $familyRefugeesValid=1;
    protected $formOfPersecutionValid=1;
    protected $languagesSpokenWrittenValid=1;
    protected $charityWorkValid=1;
    protected $athleteInYouthValid=1;
    protected $everStartedACompanyBeforeValid=1;
    protected $homelessValid=1;
    protected $hoursAWeekWorkValid=1;
    protected $generalErrorForBlankData="Request parameter is missing.";
    protected $invalidTokenMessage = "There is some problem please try again.";
    protected $errorCodeForInvalidToken = 2013;

    /**
     * @return string
     */
    public function getInvalidTokenMessage()
    {
        return $this->invalidTokenMessage;
    }

    /**
     * @return int
     */
    public function getErrorCodeForInvalidToken()
    {
        return $this->errorCodeForInvalidToken;
    }

    /**
     * @return string
     */
    public function getGeneralErrorForBlankData()
    {
        return $this->generalErrorForBlankData;
    }

    /**
     * @return int
     */
    public function getDateOfBirthValid()
    {
        return $this->dateOfBirthValid;
    }

    /**
     * @return int
     */
    public function getHomeTownValid()
    {
        return $this->homeTownValid;
    }

    /**
     * @return int
     */
    public function getBornStateValid()
    {
        return $this->bornStateValid;
    }

    /**
     * @return int
     */
    public function getBornCountryValid()
    {
        return $this->bornCountryValid;
    }

    /**
     * @return int
     */
    public function getBirthOrderValid()
    {
        return $this->birthOrderValid;
    }

    /**
     * @return int
     */
    public function getOnlyChildValid()
    {
        return $this->onlyChildValid;
    }

    /**
     * @return int
     */
    public function getRaisedByValid()
    {
        return $this->raisedByValid;
    }

    /**
     * @return int
     */
    public function getOrphaned()
    {
        return $this->orphaned;
    }

    /**
     * @return int
     */
    public function getParentalAubstanceAbuseValid()
    {
        return $this->parentalAubstanceAbuseValid;
    }

    /**
     * @return int
     */
    public function getSocioEconomicConditionValid()
    {
        return $this->socioEconomicConditionValid;
    }

    /**
     * @return int
     */
    public function getPredominantEnvironmentValid()
    {
        return $this->predominantEnvironmentValid;
    }

    /**
     * @return int
     */
    public function getEmploymentYouthValid()
    {
        return $this->employmentYouthValid;
    }

    /**
     * @return int
     */
    public function getNumberOfJobsYouthValid()
    {
        return $this->numberOfJobsYouthValid;
    }

    /**
     * @return int
     */
    public function getMentorInYourYouthValid()
    {
        return $this->mentorInYourYouthValid;
    }

    /**
     * @return int
     */
    public function getEthnicBackgroundValid()
    {
        return $this->ethnicBackgroundValid;
    }

    /**
     * @return int
     */
    public function getCitizenshipValid()
    {
        return $this->citizenshipValid;
    }

    /**
     * @return int
     */
    public function getGenderValid()
    {
        return $this->genderValid;
    }

    /**
     * @return int
     */
    public function getSexualOrientationValid()
    {
        return $this->sexualOrientationValid;
    }

    /**
     * @return int
     */
    public function getLgbtSupporterValid()
    {
        return $this->lgbtSupporterValid;
    }

    /**
     * @return int
     */
    public function getEducationalValid()
    {
        return $this->educationalValid;
    }

    /**
     * @return int
     */
    public function getFinancialAidValid()
    {
        return $this->financialAidValid;
    }

    /**
     * @return int
     */
    public function getReligiousIdentityValid()
    {
        return $this->religiousIdentityValid;
    }

    /**
     * @return int
     */
    public function getMaritalStatusValid()
    {
        return $this->maritalStatusValid;
    }

    /**
     * @return int
     */
    public function getSingleParentValid()
    {
        return $this->singleParentValid;
    }

    /**
     * @return int
     */
    public function getChildrensValid()
    {
        return $this->childrensValid;
    }

    /**
     * @return int
     */
    public function getDrugAddictionValid()
    {
        return $this->drugAddictionValid;
    }

    /**
     * @return int
     */
    public function getIllegalDrugsValid()
    {
        return $this->illegalDrugsValid;
    }

    /**
     * @return int
     */
    public function getPhysicalAbuseValid()
    {
        return $this->physicalAbuseValid;
    }

    /**
     * @return int
     */
    public function getSexualAbuseValid()
    {
        return $this->sexualAbuseValid;
    }

    /**
     * @return int
     */
    public function getMentalAbuseValid()
    {
        return $this->mentalAbuseValid;
    }

    /**
     * @return int
     */
    public function getPoliticalAffiliationValid()
    {
        return $this->politicalAffiliationValid;
    }

    /**
     * @return int
     */
    public function getMilitaryExperienceValid()
    {
        return $this->militaryExperienceValid;
    }

    /**
     * @return int
     */
    public function getCancerSurvivorValid()
    {
        return $this->cancerSurvivorValid;
    }

    /**
     * @return int
     */
    public function getChronicDiseaseSuffererValid()
    {
        return $this->chronicDiseaseSuffererValid;
    }

    /**
     * @return int
     */
    public function getPhysicalHandicapValid()
    {
        return $this->physicalHandicapValid;
    }

    /**
     * @return int
     */
    public function getImmigrantValid()
    {
        return $this->immigrantValid;
    }

    /**
     * @return int
     */
    public function getFamilyRefugeesValid()
    {
        return $this->familyRefugeesValid;
    }

    /**
     * @return int
     */
    public function getFormOfPersecutionValid()
    {
        return $this->formOfPersecutionValid;
    }

    /**
     * @return int
     */
    public function getLanguagesSpokenWrittenValid()
    {
        return $this->languagesSpokenWrittenValid;
    }

    /**
     * @return int
     */
    public function getCharityWorkValid()
    {
        return $this->charityWorkValid;
    }

    /**
     * @return int
     */
    public function getAthleteInYouthValid()
    {
        return $this->athleteInYouthValid;
    }

    /**
     * @return int
     */
    public function getEverStartedACompanyBeforeValid()
    {
        return $this->everStartedACompanyBeforeValid;
    }

    /**
     * @return int
     */
    public function getHomelessValid()
    {
        return $this->homelessValid;
    }

    /**
     * @return int
     */
    public function hoursAWeekWorkValid()
    {
        return $this->hoursAWeekWorkValid;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
}