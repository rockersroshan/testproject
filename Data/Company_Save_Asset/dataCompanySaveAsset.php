<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class dataCompanySaveAsset
{
    protected $username = "maintestroshan@mailinator.com";
    protected $password = "Roshan@123";
    protected $assetsPublicInformationValidData=1;
    protected $assetsPublicInformationErrorMessage="<p>The Information Access Control field is required.</p>";
    protected $intellectualPropertyErrorNullMessage="";
    protected $copyrightErrorNullMessage="";
    protected $intellectualPropertyValidData="<p>Intellectual Property</p>";
    protected $copyrightValidData="<p>Copyright</p>";
    protected $trademarkValidData="<p>Trademark</p>";
    protected $patentReceivesValidData="Patent Received";
    protected $patentPendingsValidData="Patent Pending";
    protected $patentFilesValidData="Patent Filed";
    protected $generalErrorMessage =  "Request parameter is missing.";
    protected $invalidTokenMessage = "There is some problem please try again.";
    protected $errorCodeForInvalidToken = 2013;

    /**
     * @return string
     */
    public function getGeneralErrorMessage()
    {
        return $this->generalErrorMessage;
    }

    /**
     * @return string
     */
    public function getInvalidTokenMessage()
    {
        return $this->invalidTokenMessage;
    }

    /**
     * @return int
     */
    public function getErrorCodeForInvalidToken()
    {
        return $this->errorCodeForInvalidToken;
    }

    /**
     * @return string
     */
    public function getIntellectualPropertyValidData()
    {
        return $this->intellectualPropertyValidData;
    }

    /**
     * @return string
     */
    public function getCopyrightValidData()
    {
        return $this->copyrightValidData;
    }

    /**
     * @return string
     */
    public function getTrademarkValidData()
    {
        return $this->trademarkValidData;
    }

    /**
     * @return string
     */
    public function getPatentReceivesValidData()
    {
        return $this->patentReceivesValidData;
    }

    /**
     * @return string
     */
    public function getPatentPendingsValidData()
    {
        return $this->patentPendingsValidData;
    }

    /**
     * @return string
     */
    public function getPatentFilesValidData()
    {
        return $this->patentFilesValidData;
    }

    /**
     * @return string
     */
    public function getAssetsPublicInformationErrorMessage()
    {
        return $this->assetsPublicInformationErrorMessage;
    }

    /**
     * @return string
     */
    public function getIntellectualPropertyErrorNullMessage()
    {
        return $this->intellectualPropertyErrorNullMessage;
    }

    /**
     * @return string
     */
    public function getCopyrightErrorNullMessage()
    {
        return $this->copyrightErrorNullMessage;
    }

    /**
     * @return string
     */
    public function getTrademarkErrorNullMessage()
    {
        return $this->trademarkErrorNullMessage;
    }
    protected $trademarkErrorNullMessage="";

    /**
     * @return int
     */
    public function getAssetsPublicInformationValidData()
    {
        return $this->assetsPublicInformationValidData;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
}