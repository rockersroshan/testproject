<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class dataSignupRegister
{
    //invest_capital, build_a_company, offer_services
    protected $validType = "invest_capital";
    protected $validEmail;
    protected $emailForNotSuccessReg = "roshanrockers3562@mailinator.com";
    protected $invalidEmail = "test123";
    protected $validPassword = "Roshan@123";
    protected $invalidPasswordMin = "test";
    protected $maxPassword = "Roshan@123123456789123456";
    protected $numberAndCapitalPassword = "TEST12345";
    protected $smallAndSpecialPassword = "test!@#$%";
    protected $validCPassword = "Roshan@123";
    protected $validMessage = "";
    protected $invalidEmailMessage = "";
    protected $emailErrorMessage = "<p>The Email Address field must contain a valid email address.</p>";
    protected $invalidEmailPasswordErrorMessage = "";
    protected $invalidEmailCPasswordErrorMessage = "";
    protected $invalidPasswordErrorMessage = "";
    protected $smallAndSpecialPasswordErrorMessage = "<p>The Password field must be at least one uppercase letter.</p>";
    protected $minPasswordPasswordErrorMessage = "<p>The Password field must be at least one uppercase letter.</p>";
    protected $minPasswordCPasswordErrorMessage = "<p>The Retype Password field must be at least 8 characters in length.</p>";
    protected $maxPasswordPasswordErrorMessage = "<p>The Password field cannot exceed 20 characters in length.</p>";
    protected $maxPasswordCPasswordErrorMessage = "<p>The Retype Password field cannot exceed 20 characters in length.</p>";
    protected $blankEmailErrorMessage = "<p>The Email Address field is required.</p>";
    protected $numberAndCapitalPasswordErrorMessage = "<p>The Password field must be at least one lowercase letter.</p>";
    protected $passwordAndCPasswordNotSameCPasswordErrorMessage = "<p>The Retype Password field does not match the Password field.</p>";
    protected $blankPasswordErrorMessage = "<p>The Password field is required.</p>";
    protected $blankCPasswordErrorMessage = "<p>The Retype Password field is required.</p>";
    protected $invalidTokenMessage = "There is some problem please try again.";

    /**
     * @return string
     */
    public function getInvalidTokenMessage()
    {
        return $this->invalidTokenMessage;
    }

    /**
     * @return int
     */
    public function getInvalidTokenErrorCode()
    {
        return $this->invalidTokenErrorCode;
    }
    protected $invalidTokenErrorCode = 2013;

    /**
     * @return string
     */
    public function getBlankPasswordErrorMessage()
    {
        return $this->blankPasswordErrorMessage;
    }

    /**
     * @return string
     */
    public function getBlankCPasswordErrorMessage()
    {
        return $this->blankCPasswordErrorMessage;
    }

    /**
     * @return string
     */
    public function getPasswordAndCPasswordNotSameCPasswordErrorMessage()
    {
        return $this->passwordAndCPasswordNotSameCPasswordErrorMessage;
    }

    /**
     * @return string
     */
    public function getSmallAndSpecialPassword()
    {
        return $this->smallAndSpecialPassword;
    }

    /**
     * @return string
     */
    public function getSmallAndSpecialPasswordErrorMessage()
    {
        return $this->smallAndSpecialPasswordErrorMessage;
    }

    /**
     * @return string
     */
    public function getEmailForNotSuccessReg()
    {
        return $this->emailForNotSuccessReg;
    }

    /**
     * @return string
     */
    public function getNumberAndCapitalPassword()
    {
        return $this->numberAndCapitalPassword;
    }

    /**
     * @return string
     */
    public function getNumberAndCapitalPasswordErrorMessage()
    {
        return $this->numberAndCapitalPasswordErrorMessage;
    }

    /**
     * @return string
     */
    public function getMaxPassword()
    {
        return $this->maxPassword;
    }

    /**
     * @return string
     */
    public function getMaxPasswordPasswordErrorMessage()
    {
        return $this->maxPasswordPasswordErrorMessage;
    }

    /**
     * @return string
     */
    public function getMaxPasswordCPasswordErrorMessage()
    {
        return $this->maxPasswordCPasswordErrorMessage;
    }

    /**
     * @return string
     */
    public function getInvalidPasswordErrorMessage()
    {
        return $this->invalidPasswordErrorMessage;
    }

    /**
     * @return string
     */
    public function getMinPasswordPasswordErrorMessage()
    {
        return $this->minPasswordPasswordErrorMessage;
    }

    /**
     * @return string
     */
    public function getMinPasswordCPasswordErrorMessage()
    {
        return $this->minPasswordCPasswordErrorMessage;
    }

    /**
     * @return string
     */
    public function getInvalidPasswordMin()
    {
        return $this->invalidPasswordMin;
    }

    /**
     * @return string
     */
    public function getBlankEmailErrorMessage()
    {
        return $this->blankEmailErrorMessage;
    }

    /**
     * @return string
     */
    public function getEmailErrorMessage()
    {
        return $this->emailErrorMessage;
    }

    /**
     * @return string
     */
    public function getInvalidEmailPasswordErrorMessage()
    {
        return $this->invalidEmailPasswordErrorMessage;
    }

    /**
     * @return string
     */
    public function getInvalidEmailCPasswordErrorMessage()
    {
        return $this->invalidEmailCPasswordErrorMessage;
    }

    /**
     * @return string
     */
    public function getInvalidEmailMessage()
    {
        return $this->invalidEmailMessage;
    }

    /**
     * @return string
     */
    public function getInvalidEmail()
    {
        return $this->invalidEmail;
    }

    /**
     * @return string
     */
    public function getValidMessage()
    {
        return $this->validMessage;
    }

    /**
     * @return string
     */
    public function getValidEmail()
    {
        $randomString = bin2hex(openssl_random_pseudo_bytes(15));
        $this->validEmail = $randomString."@mailinator.com";
        return $this->validEmail;
    }

    /**
     * @return string
     */
    public function getValidPassword()
    {
        return $this->validPassword;
    }

    /**
     * @return string
     */
    public function getValidCPassword()
    {
        return $this->validCPassword;
    }


    /**
     * @return string
     */
    public function getValidType()
    {
        return $this->validType;
    }
}