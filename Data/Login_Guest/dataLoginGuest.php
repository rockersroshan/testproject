<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class dataLoginGuest
{
    protected $apiToken = "WQazAUbwfRYu2//yY322bg==";
    protected $siteNameValidData = "Equity";
    protected $messageValidData = "Success";
    protected $errorCodeNullData = "";
    protected $messageInvalidTokenMessage = "Requested token is mismatch.";
    protected $errorCodeInvalidToken = 1009;

    /**
     * @return string
     */
    public function getMessageInvalidTokenMessage()
    {
        return $this->messageInvalidTokenMessage;
    }

    /**
     * @return int
     */
    public function getErrorCodeInvalidToken()
    {
        return $this->errorCodeInvalidToken;
    }

    /**
     * @return string
     */
    public function getSiteNameValidData()
    {
        return $this->siteNameValidData;
    }

    /**
     * @return string
     */
    public function getMessageValidData()
    {
        return $this->messageValidData;
    }

    /**
     * @return string
     */
    public function getErrorCodeNullData()
    {
        return $this->errorCodeNullData;
    }

    /**
     * @return string
     */
    public function getApiToken()
    {
        return $this->apiToken;
    }
}