<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class dataCompanyPitchSlide
{
    protected $username = "maintestroshan@mailinator.com";
    protected $password = "Roshan@123";
    protected $pitchSlideTitleValidData="QA Roshan";
    protected $fileValidData="C:\\Users\\RW02\\Downloads\\sample.pdf";
    protected $fileInvalidData="C:\\Users\\RW02\\Downloads\\FakeMailList.txt";
    protected $filePathNullData="";
    protected $pitchSlideIDNullData="";
    protected $pitchSlideTitleErrorNullData="";
    protected $pitchSlideErrorMessage="<p>Please upload pdf file.</p>";
    protected $generalErrorMessage = "Request parameter is missing.";
    protected $invalidTokenMessage = "There is some problem please try again.";
    protected $errorCodeForInvalidToken = 2013;


    /**
     * @return string
     */
    public function getGeneralErrorMessage()
    {
        return $this->generalErrorMessage;
    }

    /**
     * @return string
     */
    public function getInvalidTokenMessage()
    {
        return $this->invalidTokenMessage;
    }

    /**
     * @return int
     */
    public function getErrorCodeForInvalidToken()
    {
        return $this->errorCodeForInvalidToken;
    }

    /**
     * @return string
     */
    public function getFilePathNullData()
    {
        return $this->filePathNullData;
    }

    /**
     * @return string
     */
    public function getPitchSlideIDNullData()
    {
        return $this->pitchSlideIDNullData;
    }

    /**
     * @return string
     */
    public function getPitchSlideTitleErrorNullData()
    {
        return $this->pitchSlideTitleErrorNullData;
    }

    /**
     * @return string
     */
    public function getPitchSlideErrorMessage()
    {
        return $this->pitchSlideErrorMessage;
    }

    /**
     * @return string
     */
    public function getFileInvalidData()
    {
        return $this->fileInvalidData;
    }
    protected $primaryPeopleImageNameErrorNullData = "";
    protected $filePathValidData="https://thecrowd.storage.googleapis.com/upload/equity/pitch";

    /**
     * @return string
     */
    public function getFilePathValidData()
    {
        return $this->filePathValidData;
    }

    /**
     * @return string
     */
    public function getPrimaryPeopleImageNameErrorNullData()
    {
        return $this->primaryPeopleImageNameErrorNullData;
    }

    /**
     * @return string
     */
    public function getPitchSlideTitleValidData()
    {
        return $this->pitchSlideTitleValidData;
    }

    /**
     * @return string
     */
    public function getFileValidData()
    {
        return $this->fileValidData;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
}