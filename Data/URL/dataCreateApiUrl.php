<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

include_once ("src/Guest_Login/guestLogin.php");

class dataCreateApiUrl
{
    protected $_domainLiveAPIURL;
    protected $OkResponseCode = 200;

    function __construct()
    {
        $this->_liveDomain = "http://rockerstech.com/thecrowd/";
        $this->_liveApiVersion = "api/";
        $this->_liveTokenkey = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJyb2NrZXJzdGVjaC5jb20iLCJpYXQiOjE1MzE4MDA0MzIsImV4cCI6MTUzMjY2NDQzMiwidXNlcl9pZCI6MCwidXNlcl9zYWx0IjoiZTllMWI0MWYwOTIwZDE5ODNlNzEyMjJhOWNlNTEwMWIiLCJsYW5nIjoiZW4ifQ.Agbhvf6jki1YsDRawZkr0wk31TqRVyaDk9qzDvNkJUA";
    }

    /**
     * @return mixed
     */
    public function getDomainLiveAPIURL()
    {
        $this->_domainLiveAPIURL = $this->getLiveDomain() . $this->getLiveApiVersion();
        return $this->_domainLiveAPIURL;
    }

    /**
     * @return string
     */
    public function getLiveDomain()
    {
        return $this->_liveDomain;
    }

    /**
     * @return string
     */
    public function getLiveApiVersion()
    {
        return $this->_liveApiVersion;
    }

    /**
     * @return string
     */
    public function getClientpublickey()
    {
        $guestLogin = new guestLogin();

        $response = $guestLogin->callGuestLoginAPI();

        $json_response = json_decode($response);

        $token = $json_response->data->token;

        return $token;
    }

    /**
     * @return int
     */
    public function getOkResponseCode()
    {
        return $this->OkResponseCode;
    }
}