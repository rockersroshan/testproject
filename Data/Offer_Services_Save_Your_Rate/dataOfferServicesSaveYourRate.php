<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class dataOfferServicesSaveYourRate
{
    protected $username = "roshanofferservices@mailinator.com";
    protected $password = "Roshan@123";
    protected $invalidTokenMessage = "There is some problem please try again.";
    protected $errorCodeForInvalidToken = 2013;
    protected $userNameValidData = "Roshan";
    protected $lastNameValidData = "Mistry";
    protected $professionalTitleValidData = "Information technology";
    protected $professionalOverviewValidData = "<p>professional_overview</p>";
    protected $englishProficiencyValidData = "Fluent";
    protected $hourlyRateValidData = 10;
    protected $availableForWorkEachWeekValidData = "More than 30 hrs/week";
    protected $addressValidData = "123, Vadodara, Gujarat";
    protected $cityValidData = "Vadodara";
    protected $stateValidData = "Gujarat";
    protected $countryValidData = "India";
    protected $zipCodeValidData = 123456;
    protected $timezoneValidData = "Africa/Asmera";
    protected $generalErrorMessage =  "Request parameter is missing.";

    /**
     * @return string
     */
    public function getGeneralErrorMessage()
    {
        return $this->generalErrorMessage;
    }

    /**
     * @return string
     */
    public function getUserNameValidData()
    {
        return $this->userNameValidData;
    }

    /**
     * @return string
     */
    public function getLastNameValidData()
    {
        return $this->lastNameValidData;
    }

    /**
     * @return string
     */
    public function getProfessionalTitleValidData()
    {
        return $this->professionalTitleValidData;
    }

    /**
     * @return string
     */
    public function getProfessionalOverviewValidData()
    {
        return $this->professionalOverviewValidData;
    }

    /**
     * @return string
     */
    public function getEnglishProficiencyValidData()
    {
        return $this->englishProficiencyValidData;
    }

    /**
     * @return int
     */
    public function getHourlyRateValidData()
    {
        return $this->hourlyRateValidData;
    }

    /**
     * @return string
     */
    public function getAvailableForWorkEachWeekValidData()
    {
        return $this->availableForWorkEachWeekValidData;
    }

    /**
     * @return string
     */
    public function getAddressValidData()
    {
        return $this->addressValidData;
    }

    /**
     * @return string
     */
    public function getCityValidData()
    {
        return $this->cityValidData;
    }

    /**
     * @return string
     */
    public function getStateValidData()
    {
        return $this->stateValidData;
    }

    /**
     * @return string
     */
    public function getCountryValidData()
    {
        return $this->countryValidData;
    }

    /**
     * @return int
     */
    public function getZipCodeValidData()
    {
        return $this->zipCodeValidData;
    }

    /**
     * @return string
     */
    public function getTimezoneValidData()
    {
        return $this->timezoneValidData;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getInvalidTokenMessage()
    {
        return $this->invalidTokenMessage;
    }

    /**
     * @return int
     */
    public function getErrorCodeForInvalidToken()
    {
        return $this->errorCodeForInvalidToken;
    }
}