<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class dataInvestSaveCommunication
{
    protected $username = "roshan.rockersinfo@gmail.com";
    protected $password = "Roshan@123";
    protected $directMessageAlertValid=1;
    protected $repliesAlertValid=1;
    protected $companyLiveSessionAlertValid=1;
    protected $companyFundingRoundAlertValid=1;
    protected $companyFinancialResultsAlertValid=1;
    protected $companyUpdateAlertValid=1;
    protected $companyHitsMilestoneAlertValid=1;
    protected $companyUpdatesProfileAlertValid=1;
    protected $companyAnswersQuestionAlertValid=1;
    protected $importantUpdatesAlertValid=1;
    protected $featuredStartupsAlertValid=1;
    protected $companiesAffinityCriteriaAlertValid=1;
    protected $generalErrorForBlankData="Request parameter is missing.";
    protected $invalidTokenMessage = "There is some problem please try again.";
    protected $errorCodeForInvalidToken = 2013;

    /**
     * @return string
     */
    public function getInvalidTokenMessage()
    {
        return $this->invalidTokenMessage;
    }

    /**
     * @return int
     */
    public function getErrorCodeForInvalidToken()
    {
        return $this->errorCodeForInvalidToken;
    }

    /**
     * @return string
     */
    public function getGeneralErrorForBlankData()
    {
        return $this->generalErrorForBlankData;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return int
     */
    public function getDirectMessageAlertValid()
    {
        return $this->directMessageAlertValid;
    }

    /**
     * @return int
     */
    public function getRepliesAlertValid()
    {
        return $this->repliesAlertValid;
    }

    /**
     * @return int
     */
    public function getCompanyLiveSessionAlertValid()
    {
        return $this->companyLiveSessionAlertValid;
    }

    /**
     * @return int
     */
    public function getCompanyFundingRoundAlertValid()
    {
        return $this->companyFundingRoundAlertValid;
    }

    /**
     * @return int
     */
    public function getCompanyFinancialResultsAlertValid()
    {
        return $this->companyFinancialResultsAlertValid;
    }

    /**
     * @return int
     */
    public function getCompanyUpdateAlertValid()
    {
        return $this->companyUpdateAlertValid;
    }

    /**
     * @return int
     */
    public function getCompanyHitsMilestoneAlertValid()
    {
        return $this->companyHitsMilestoneAlertValid;
    }

    /**
     * @return int
     */
    public function getCompanyUpdatesProfileAlertValid()
    {
        return $this->companyUpdatesProfileAlertValid;
    }

    /**
     * @return int
     */
    public function getCompanyAnswersQuestionAlertValid()
    {
        return $this->companyAnswersQuestionAlertValid;
    }

    /**
     * @return int
     */
    public function getImportantUpdatesAlertValid()
    {
        return $this->importantUpdatesAlertValid;
    }

    /**
     * @return int
     */
    public function getFeaturedStartupsAlertValid()
    {
        return $this->featuredStartupsAlertValid;
    }

    /**
     * @return int
     */
    public function getCompaniesAffinityCriteriaAlertValid()
    {
        return $this->companiesAffinityCriteriaAlertValid;
    }

}