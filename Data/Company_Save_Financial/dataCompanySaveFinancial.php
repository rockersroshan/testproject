<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class dataCompanySaveFinancial
{
    protected $username = "maintestroshan@mailinator.com";
    protected $password = "Roshan@123";
    protected $financialPublicInformationValidData=1;
    protected $salesLastYearValidData="Pre-revenue";
    protected $taxReliefTypeValidData="SEIS";
    protected $financialReviewEndDateValidData="2018-07-12";
    protected $resultsOfOperationValidData="<p>Discussion of Results of Operations</p>";
    protected $financialMilestoneValidData="<p>Discussion of Financial Milestones</p>";
    protected $liquidityAndCapitalResourceValidData="<p>Discussion of Liquidity and Capital Resources</p>";
    protected $relatedPartyTransactionValidData="<p>Discussion of Related Party Transactions</p>";
    protected $idebtednessValidData="<p>Discussion of Idebtedness</p>";
    protected $useOfProceedValidData="<p>Discussion of Use of Proceeds</p>";
    protected $currentTotalAssetsValidData=100;
    protected $currentCashAndCashEquivalentsValidData=200;
    protected $currentAccountsReceivableValidData=111;
    protected $currentShortTermDebtValidData=233;
    protected $currentLongTermDebtValidData=66;
    protected $currentRevenuesSalesValidData=56;
    protected $currentCostOfGoodsSoldValidData=90;
    protected $currentTaxesPaidValidData=45;
    protected $currentNetIncomeValidData=466;
    protected $preTotalAssetsValidData=200;
    protected $preCashAndCashEquivalentsValidData=220;
    protected $preAccountsReceivableValidData=211;
    protected $preShortTermDebtValidData=233;
    protected $preLongTermDebtValidData=26;
    protected $preRevenuesSalesValidData=26;
    protected $preCostOfGoodsSoldValidData=20;
    protected $preTaxesPaidValidData=25;
    protected $preNetIncomeValidData=266;
    protected $currentFiscalYearErrorMullMessage="";
    protected $perFiscalYearErrorNullMessage="";
    protected $financialStatementTitleErrorNullMessage="";
    protected $generalErrorMessage =  "Request parameter is missing.";
    protected $invalidTokenMessage = "There is some problem please try again.";
    protected $errorCodeForInvalidToken = 2013;

    /**
     * @return string
     */
    public function getGeneralErrorMessage()
    {
        return $this->generalErrorMessage;
    }

    /**
     * @return string
     */
    public function getInvalidTokenMessage()
    {
        return $this->invalidTokenMessage;
    }

    /**
     * @return int
     */
    public function getErrorCodeForInvalidToken()
    {
        return $this->errorCodeForInvalidToken;
    }

    /**
     * @return string
     */
    public function getCurrentFiscalYearErrorMullMessage()
    {
        return $this->currentFiscalYearErrorMullMessage;
    }

    /**
     * @return string
     */
    public function getPerFiscalYearErrorNullMessage()
    {
        return $this->perFiscalYearErrorNullMessage;
    }

    /**
     * @return string
     */
    public function getFinancialStatementTitleErrorNullMessage()
    {
        return $this->financialStatementTitleErrorNullMessage;
    }

    /**
     * @return int
     */
    public function getFinancialPublicInformationValidData()
    {
        return $this->financialPublicInformationValidData;
    }

    /**
     * @return string
     */
    public function getSalesLastYearValidData()
    {
        return $this->salesLastYearValidData;
    }

    /**
     * @return string
     */
    public function getTaxReliefTypeValidData()
    {
        return $this->taxReliefTypeValidData;
    }

    /**
     * @return string
     */
    public function getFinancialReviewEndDateValidData()
    {
        return $this->financialReviewEndDateValidData;
    }

    /**
     * @return string
     */
    public function getResultsOfOperationValidData()
    {
        return $this->resultsOfOperationValidData;
    }

    /**
     * @return string
     */
    public function getFinancialMilestoneValidData()
    {
        return $this->financialMilestoneValidData;
    }

    /**
     * @return string
     */
    public function getLiquidityAndCapitalResourceValidData()
    {
        return $this->liquidityAndCapitalResourceValidData;
    }

    /**
     * @return string
     */
    public function getRelatedPartyTransactionValidData()
    {
        return $this->relatedPartyTransactionValidData;
    }

    /**
     * @return string
     */
    public function getIdebtednessValidData()
    {
        return $this->idebtednessValidData;
    }

    /**
     * @return string
     */
    public function getUseOfProceedValidData()
    {
        return $this->useOfProceedValidData;
    }

    /**
     * @return int
     */
    public function getCurrentTotalAssetsValidData()
    {
        return $this->currentTotalAssetsValidData;
    }

    /**
     * @return int
     */
    public function getCurrentCashAndCashEquivalentsValidData()
    {
        return $this->currentCashAndCashEquivalentsValidData;
    }

    /**
     * @return int
     */
    public function getCurrentAccountsReceivableValidData()
    {
        return $this->currentAccountsReceivableValidData;
    }

    /**
     * @return int
     */
    public function getCurrentShortTermDebtValidData()
    {
        return $this->currentShortTermDebtValidData;
    }

    /**
     * @return int
     */
    public function getCurrentLongTermDebtValidData()
    {
        return $this->currentLongTermDebtValidData;
    }

    /**
     * @return int
     */
    public function getCurrentRevenuesSalesValidData()
    {
        return $this->currentRevenuesSalesValidData;
    }

    /**
     * @return int
     */
    public function getCurrentCostOfGoodsSoldValidData()
    {
        return $this->currentCostOfGoodsSoldValidData;
    }

    /**
     * @return int
     */
    public function getCurrentTaxesPaidValidData()
    {
        return $this->currentTaxesPaidValidData;
    }

    /**
     * @return int
     */
    public function getCurrentNetIncomeValidData()
    {
        return $this->currentNetIncomeValidData;
    }

    /**
     * @return int
     */
    public function getPreTotalAssetsValidData()
    {
        return $this->preTotalAssetsValidData;
    }

    /**
     * @return int
     */
    public function getPreCashAndCashEquivalentsValidData()
    {
        return $this->preCashAndCashEquivalentsValidData;
    }

    /**
     * @return int
     */
    public function getPreAccountsReceivableValidData()
    {
        return $this->preAccountsReceivableValidData;
    }

    /**
     * @return int
     */
    public function getPreShortTermDebtValidData()
    {
        return $this->preShortTermDebtValidData;
    }

    /**
     * @return int
     */
    public function getPreLongTermDebtValidData()
    {
        return $this->preLongTermDebtValidData;
    }

    /**
     * @return int
     */
    public function getPreRevenuesSalesValidData()
    {
        return $this->preRevenuesSalesValidData;
    }

    /**
     * @return int
     */
    public function getPreCostOfGoodsSoldValidData()
    {
        return $this->preCostOfGoodsSoldValidData;
    }

    /**
     * @return int
     */
    public function getPreTaxesPaidValidData()
    {
        return $this->preTaxesPaidValidData;
    }

    /**
     * @return int
     */
    public function getPreNetIncomeValidData()
    {
        return $this->preNetIncomeValidData;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
}