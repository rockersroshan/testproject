<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class dataFrontLogin
{
    protected $userName = "pa.rthrockersi.nfo@gmail.com";
    protected $invalidEmail = "pa.rthrockersi.nfo";
    protected $notRegisterEmailAddress = "pa.rthrockersi.nfo12345678926845485484251@gmail.com";
    protected $password = "Parth@123";
    protected $invalidPassword = "Parth";
    protected $firstName = "parth";
    protected $lastName = "gohil";
    protected $OkResponseCode = 200;
    protected $parameterNotDefindMessage = "<p>Request parameter is missing.</p>";
    protected $invalidParameterValueMessage = "<p>Email Address or Password is invalid.</p>";
    protected $errorCodeWhenParameterNotDefind = 1006;
    protected $errorCodeWhenInvalidParameterValueDefind = 3005;

    /**
     * @return string
     */
    public function getNotRegisterEmailAddress()
    {
        echo "\n Not register email address : " . $this->notRegisterEmailAddress;
        return $this->notRegisterEmailAddress;
    }

    /**
     * @return string
     */
    public function getInvalidPassword()
    {
        echo "\n Invalid Password : " . $this->invalidPassword;
        return $this->invalidPassword;
    }

    /**
     * @return int
     */
    public function getErrorCodeWhenInvalidParameterValueDefind()
    {
        return $this->errorCodeWhenInvalidParameterValueDefind;
    }

    /**
     * @return string
     */
    public function getInvalidParameterValueMessage()
    {
        return $this->invalidParameterValueMessage;
    }

    /**
     * @return string
     */
    public function getInvalidEmail()
    {
        echo "\n User Last Name : " . $this->invalidEmail;
        return $this->invalidEmail;
    }

    /**
     * @return int
     */
    public function getErrorCodeWhenParameterNotDefind()
    {
        return $this->errorCodeWhenParameterNotDefind;
    }

    /**
     * @return string
     */
    public function getParameterNotDefindMessage()
    {
        return $this->parameterNotDefindMessage;
    }

    /**
     * @return int
     */
    public function getOkResponseCode()
    {
        return $this->OkResponseCode;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        echo "\n User Last Name : " . $this->lastName;
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        echo "\n User First Name : " . $this->firstName;
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        echo "\n User Password : " . $this->password;
        return $this->password;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        echo "\n User Name : " . $this->userName;
        return $this->userName;
    }
}