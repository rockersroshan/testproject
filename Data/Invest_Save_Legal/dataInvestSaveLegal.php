<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class dataInvestSaveLegal
{
    protected $username = "roshan.rockersinfo@gmail.com";
    protected $password = "Roshan@123";
    protected $confidentialValid = 1;
    protected $riskValid = 1;
    protected $noReturnValid = 1;
    protected $noAdviceValid = 1;
    protected $termsAndConditionsValid = 1;
    protected $userAgreementValid = 1;
    protected $investorAgreementValid = 1;
    protected $riskDisclosureValid = 1;
    protected $electronicConsentNoticeValid = 1;
    protected $electronicDeliveryNoticeValid = 1;
    protected $educationalMaterialsValid = 1;
    protected $compensationDisclosureValid = 1;
    protected $privacyPolicyValid = 1;
    protected $firstFourDataBlankSuccessMessage = "";
    protected $agreement1ErrorMessage = "<p>Please agree all above terms.</p>";
    protected $fiveToAllDataBlankSuccessMessage = "";
    protected $agreement2ErrorMessage = "<p>Please agree all above terms.</p>";
    protected $invalidTokenMessage = "There is some problem please try again.";
    protected $errorCodeForInvalidToken = 2013;

    /**
     * @return string
     */
    public function getInvalidTokenMessage()
    {
        return $this->invalidTokenMessage;
    }

    /**
     * @return int
     */
    public function getErrorCodeForInvalidToken()
    {
        return $this->errorCodeForInvalidToken;
    }

    /**
     * @return string
     */
    public function getFiveToAllDataBlankSuccessMessage()
    {
        return $this->fiveToAllDataBlankSuccessMessage;
    }

    /**
     * @return string
     */
    public function getAgreement2ErrorMessage()
    {
        return $this->agreement2ErrorMessage;
    }

    /**
     * @return string
     */
    public function getFirstFourDataBlankSuccessMessage()
    {
        return $this->firstFourDataBlankSuccessMessage;
    }

    /**
     * @return string
     */
    public function getAgreement1ErrorMessage()
    {
        return $this->agreement1ErrorMessage;
    }

    /**
     * @return int
     */
    public function getConfidentialValid()
    {
        return $this->confidentialValid;
    }

    /**
     * @return int
     */
    public function getRiskValid()
    {
        return $this->riskValid;
    }

    /**
     * @return int
     */
    public function getNoReturnValid()
    {
        return $this->noReturnValid;
    }

    /**
     * @return int
     */
    public function getNoAdviceValid()
    {
        return $this->noAdviceValid;
    }

    /**
     * @return int
     */
    public function getTermsAndConditionsValid()
    {
        return $this->termsAndConditionsValid;
    }

    /**
     * @return int
     */
    public function getUserAgreementValid()
    {
        return $this->userAgreementValid;
    }

    /**
     * @return int
     */
    public function getInvestorAgreementValid()
    {
        return $this->investorAgreementValid;
    }

    /**
     * @return int
     */
    public function getRiskDisclosureValid()
    {
        return $this->riskDisclosureValid;
    }

    /**
     * @return int
     */
    public function getElectronicConsentNoticeValid()
    {
        return $this->electronicConsentNoticeValid;
    }

    /**
     * @return int
     */
    public function getElectronicDeliveryNoticeValid()
    {
        return $this->electronicDeliveryNoticeValid;
    }

    /**
     * @return int
     */
    public function getEducationalMaterialsValid()
    {
        return $this->educationalMaterialsValid;
    }

    /**
     * @return int
     */
    public function getCompensationDisclosureValid()
    {
        return $this->compensationDisclosureValid;
    }

    /**
     * @return int
     */
    public function getPrivacyPolicyValid()
    {
        return $this->privacyPolicyValid;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
}