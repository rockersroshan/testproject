<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class dataCompanySaveBasic
{
    protected $username = "maintestroshan@mailinator.com";
    protected $password = "Roshan@123";
    protected $userNameValidData = "Roshan";
    protected $lastNameValidData = "Mistry";
    protected $companyNameValidData = "Rockers Test";
    protected $companyURLValidData = "rockerstest";
    protected $companyCategoryIdValidData = 20;
    protected $shortPitchValidData = "<p>Written brief service or product description <br></p>";
    protected $companyNameErrorNullMessage = "";
    protected $companyURLErrorNullMessage = "";
    protected $companyCategoryIdErrorNullMessage = "";
    protected $shortPitchErrorNullMessage = "";
    protected $userNameErrorMessage = "<p>The User Name field is required.</p>";
    protected $lastNameErrorMessage = "<p>The Last Name field is required.</p>";
    protected $userNameErrorNullMessage = "";
    protected $lastNameErrorNullMessage = "";
    protected $companyNameErrorMessage = "<p>The Company Name field is required.</p>";
    protected $companyURLErrorMessage = "<p>The Company URL field is required.</p>";
    protected $companyCategoryIdErrorMessage = "<p>The Sector/Industry field is required.</p>";
    protected $shortPitchErrorMessage = "<p>The Product description field is required.</p>";
    protected $generalErrorMessage =  "Request parameter is missing.";
    protected $invalidTokenMessage = "There is some problem please try again.";
    protected $errorCodeForInvalidToken = 2013;

    /**
     * @return string
     */
    public function getInvalidTokenMessage()
    {
        return $this->invalidTokenMessage;
    }

    /**
     * @return int
     */
    public function getErrorCodeForInvalidToken()
    {
        return $this->errorCodeForInvalidToken;
    }

    /**
     * @return string
     */
    public function getGeneralErrorMessage()
    {
        return $this->generalErrorMessage;
    }

    /**
     * @return string
     */
    public function getUserNameErrorNullMessage()
    {
        return $this->userNameErrorNullMessage;
    }

    /**
     * @return string
     */
    public function getLastNameErrorNullMessage()
    {
        return $this->lastNameErrorNullMessage;
    }

    /**
     * @return string
     */
    public function getCompanyNameErrorMessage()
    {
        return $this->companyNameErrorMessage;
    }

    /**
     * @return string
     */
    public function getCompanyURLErrorMessage()
    {
        return $this->companyURLErrorMessage;
    }

    /**
     * @return string
     */
    public function getCompanyCategoryIdErrorMessage()
    {
        return $this->companyCategoryIdErrorMessage;
    }

    /**
     * @return string
     */
    public function getShortPitchErrorMessage()
    {
        return $this->shortPitchErrorMessage;
    }

    /**
     * @return string
     */
    public function getCompanyNameErrorNullMessage()
    {
        return $this->companyNameErrorNullMessage;
    }

    /**
     * @return string
     */
    public function getCompanyURLErrorNullMessage()
    {
        return $this->companyURLErrorNullMessage;
    }

    /**
     * @return string
     */
    public function getCompanyCategoryIdErrorNullMessage()
    {
        return $this->companyCategoryIdErrorNullMessage;
    }

    /**
     * @return string
     */
    public function getShortPitchErrorNullMessage()
    {
        return $this->shortPitchErrorNullMessage;
    }

    /**
     * @return string
     */
    public function getUserNameErrorMessage()
    {
        return $this->userNameErrorMessage;
    }

    /**
     * @return string
     */
    public function getLastNameErrorMessage()
    {
        return $this->lastNameErrorMessage;
    }


    /**
     * @return string
     */
    public function getUserNameValidData()
    {
        return $this->userNameValidData;
    }

    /**
     * @return string
     */
    public function getLastNameValidData()
    {
        return $this->lastNameValidData;
    }

    /**
     * @return string
     */
    public function getCompanyNameValidData()
    {
        return $this->companyNameValidData;
    }

    /**
     * @return string
     */
    public function getCompanyURLValidData()
    {
        return $this->companyURLValidData;
    }

    /**
     * @return int
     */
    public function getCompanyCategoryIdValidData()
    {
        return $this->companyCategoryIdValidData;
    }

    /**
     * @return string
     */
    public function getShortPitchValidData()
    {
        return $this->shortPitchValidData;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
}