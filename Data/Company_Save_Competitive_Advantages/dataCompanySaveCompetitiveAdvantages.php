<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class dataCompanySaveCompetitiveAdvantages
{
    protected $username = "maintestroshan@mailinator.com";
    protected $password = "Roshan@123";
    protected $competitiveAdvantagePublicInformationValidData = 1;
    protected $lowCostProviderValidData = 0;
    protected $valueAddedProviderValidData = 0;
    protected $perceivedValueAddedProviderValidData = 1;
    protected $competitiveAdvantagePublicInformationErrorMessage = "<p>The Information Access Control field is required.</p>";
    protected $lowCostProviderErrorNullMessage = "";
    protected $valueAddedProviderErrorNullMessage = "";
    protected $perceivedValueAddedProviderErrorNullMessage = "";
    protected $competitiveAdvantagePublicInformationErrorNullMessage="";
    protected $lowCostProviderErrorMessage="<p>The Low Cost field is required.</p>";
    protected $valueAddedProviderErrorMessage="<p>The Value Added field is required.</p>";
    protected $perceivedValueAddedProviderErrorMessage="<p>The Perceived Value field is required.</p>";
    protected $generalErrorMessage =  "Request parameter is missing.";
    protected $invalidTokenMessage = "There is some problem please try again.";
    protected $errorCodeForInvalidToken = 2013;

    /**
     * @return string
     */
    public function getGeneralErrorMessage()
    {
        return $this->generalErrorMessage;
    }

    /**
     * @return string
     */
    public function getInvalidTokenMessage()
    {
        return $this->invalidTokenMessage;
    }

    /**
     * @return int
     */
    public function getErrorCodeForInvalidToken()
    {
        return $this->errorCodeForInvalidToken;
    }

    /**
     * @return string
     */
    public function getPerceivedValueAddedProviderErrorMessage()
    {
        return $this->perceivedValueAddedProviderErrorMessage;
    }

    /**
     * @return string
     */
    public function getValueAddedProviderErrorMessage()
    {
        return $this->valueAddedProviderErrorMessage;
    }

    /**
     * @return string
     */
    public function getCompetitiveAdvantagePublicInformationErrorNullMessage()
    {
        return $this->competitiveAdvantagePublicInformationErrorNullMessage;
    }

    /**
     * @return string
     */
    public function getLowCostProviderErrorMessage()
    {
        return $this->lowCostProviderErrorMessage;
    }

    /**
     * @return string
     */
    public function getCompetitiveAdvantagePublicInformationErrorMessage()
    {
        return $this->competitiveAdvantagePublicInformationErrorMessage;
    }

    /**
     * @return string
     */
    public function getLowCostProviderErrorNullMessage()
    {
        return $this->lowCostProviderErrorNullMessage;
    }

    /**
     * @return string
     */
    public function getValueAddedProviderErrorNullMessage()
    {
        return $this->valueAddedProviderErrorNullMessage;
    }

    /**
     * @return string
     */
    public function getPerceivedValueAddedProviderErrorNullMessage()
    {
        return $this->perceivedValueAddedProviderErrorNullMessage;
    }

    /**
     * @return int
     */
    public function getCompetitiveAdvantagePublicInformationValidData()
    {
        return $this->competitiveAdvantagePublicInformationValidData;
    }

    /**
     * @return int
     */
    public function getLowCostProviderValidData()
    {
        return $this->lowCostProviderValidData;
    }

    /**
     * @return int
     */
    public function getValueAddedProviderValidData()
    {
        return $this->valueAddedProviderValidData;
    }

    /**
     * @return int
     */
    public function getPerceivedValueAddedProviderValidData()
    {
        return $this->perceivedValueAddedProviderValidData;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
}