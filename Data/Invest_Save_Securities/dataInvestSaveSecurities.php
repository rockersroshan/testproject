<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class dataInvestSaveSecurities
{
    protected $username = "roshan.rockersinfo@gmail.com";
    protected $password = "Roshan@123";
    protected $returnTimeframeValidData = "7 to 9 years";
    protected $pastInvestmentsTypeValidData = "Early Stage Startups";
    protected $onePastInvestmentsTypeValidData = "Late Stage Private Companies";
    protected $twoPastInvestmentsTypeValidData = "Venture Funds";
    protected $past_investmentsValidData = 100;
    protected $returnTimeframeInvalidData = 1;
    protected $pastInvestmentsTypeInvalidData = 2;
    protected $past_investmentsInvalidData = "7 to 9 years";
    protected $successNull = "";
    protected $pastInvestmentsErrorMessage = "<p>The investments field must only contain digits.</p>";
    protected $returnTimeframeErrorMessageNull = "";
    protected $invalidTokenMessage = "There is some problem please try again.";
    protected $errorCodeForInvalidToken = 2013;

    /**
     * @return string
     */
    public function getInvalidTokenMessage()
    {
        return $this->invalidTokenMessage;
    }

    /**
     * @return int
     */
    public function getErrorCodeForInvalidToken()
    {
        return $this->errorCodeForInvalidToken;
    }

    /**
     * @return string
     */
    public function getReturnTimeframeErrorMessageNull()
    {
        return $this->returnTimeframeErrorMessageNull;
    }

    /**
     * @return string
     */
    public function getPastInvestmentsErrorMessage()
    {
        return $this->pastInvestmentsErrorMessage;
    }

    /**
     * @return int
     */
    public function getReturnTimeframeInvalidData()
    {
        return $this->returnTimeframeInvalidData;
    }

    /**
     * @return int
     */
    public function getPastInvestmentsTypeInvalidData()
    {
        return $this->pastInvestmentsTypeInvalidData;
    }

    /**
     * @return string
     */
    public function getPastInvestmentsInvalidData()
    {
        return $this->past_investmentsInvalidData;
    }

    /**
     * @return string
     */
    public function getSuccessNull()
    {
        return $this->successNull;
    }


    /**
     * @return string
     */
    public function getOnePastInvestmentsTypeValidData()
    {
        return $this->onePastInvestmentsTypeValidData;
    }

    /**
     * @return string
     */
    public function getTwoPastInvestmentsTypeValidData()
    {
        return $this->twoPastInvestmentsTypeValidData;
    }

    /**
     * @return string
     */
    public function getReturnTimeframeValidData()
    {
        return $this->returnTimeframeValidData;
    }

    /**
     * @return string
     */
    public function getPastInvestmentsTypeValidData()
    {
        return $this->pastInvestmentsTypeValidData;
    }

    /**
     * @return int
     */
    public function getPastInvestmentsValidData()
    {
        return $this->past_investmentsValidData;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
}