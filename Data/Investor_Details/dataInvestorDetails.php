<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class dataInvestorDetails
{
    protected $username = "investoruser@mailinator.com";
    protected $password = "Roshan@123";
    protected $userIDValid = 135;
    protected $activeValid = 1;
    protected $timezoneValid = "GMT+0";
    protected $directMessageAlertValid = 0;
    protected $repliesAlertValid = 0;
    protected $companyLiveSessionAlertValid=0;
    protected $companyFundingRoundAlertValid=0;
    protected $companyFinancialResultsAlertValid=0;
    protected $companyUpdateAlertValid=0;
    protected $companyHitsMilestoneAlertValid=0;
    protected $companyUpdatesProfileAlertValid=0;
    protected $companyAnswersQuestionAlertValid=0;
    protected $importantUpdatesAlertValid=0;
    protected $featuredStartupsAlertValid=0;
    protected $companiesAffinityCriteriaAlertValid=0;
    protected $invalidTokenMessage = "There is some problem please try again.";
    protected $errorCodeForInvalidToken = 2013;

    /**
     * @return string
     */
    public function getInvalidTokenMessage()
    {
        return $this->invalidTokenMessage;
    }

    /**
     * @return int
     */
    public function getErrorCodeForInvalidToken()
    {
        return $this->errorCodeForInvalidToken;
    }

    /**
     * @return int
     */
    public function getDirectMessageAlertValid()
    {
        return $this->directMessageAlertValid;
    }

    /**
     * @return int
     */
    public function getRepliesAlertValid()
    {
        return $this->repliesAlertValid;
    }

    /**
     * @return int
     */
    public function getCompanyLiveSessionAlertValid()
    {
        return $this->companyLiveSessionAlertValid;
    }

    /**
     * @return int
     */
    public function getCompanyFundingRoundAlertValid()
    {
        return $this->companyFundingRoundAlertValid;
    }

    /**
     * @return int
     */
    public function getCompanyFinancialResultsAlertValid()
    {
        return $this->companyFinancialResultsAlertValid;
    }

    /**
     * @return int
     */
    public function getCompanyUpdateAlertValid()
    {
        return $this->companyUpdateAlertValid;
    }

    /**
     * @return int
     */
    public function getCompanyHitsMilestoneAlertValid()
    {
        return $this->companyHitsMilestoneAlertValid;
    }

    /**
     * @return int
     */
    public function getCompanyUpdatesProfileAlertValid()
    {
        return $this->companyUpdatesProfileAlertValid;
    }

    /**
     * @return int
     */
    public function getCompanyAnswersQuestionAlertValid()
    {
        return $this->companyAnswersQuestionAlertValid;
    }

    /**
     * @return int
     */
    public function getImportantUpdatesAlertValid()
    {
        return $this->importantUpdatesAlertValid;
    }

    /**
     * @return int
     */
    public function getFeaturedStartupsAlertValid()
    {
        return $this->featuredStartupsAlertValid;
    }

    /**
     * @return int
     */
    public function getCompaniesAffinityCriteriaAlertValid()
    {
        return $this->companiesAffinityCriteriaAlertValid;
    }

    /**
     * @return string
     */
    public function getTimezoneValid()
    {
        return $this->timezoneValid;
    }

    /**
     * @return int
     */
    public function getActiveValid()
    {
        return $this->activeValid;
    }


    /**
     * @return int
     */
    public function getUserIDValid()
    {
        return $this->userIDValid;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
}