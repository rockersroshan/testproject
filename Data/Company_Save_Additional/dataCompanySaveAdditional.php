<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class dataCompanySaveAdditional
{
    protected $username = "maintestroshan@mailinator.com";
    protected $password = "Roshan@123";
    protected $validImagePath = "C:\\Users\\RW02\\Desktop\\icon-lending-crowdfunding.png";
    protected $additionalPublicInformationValidData = 1;
    protected $addressValidData = "Vadodara, Gujarat, India";
    protected $stateValidData = "Gujarat";
    protected $cityValidData = "Vadodara";
    protected $zipCodeValidData = 21325;
    protected $countryValidData = "India";
    protected $timezoneValidData = "Africa/Asmera";
    protected $headquaterCountryValidData = "India";
    protected $jurisdictionIncorporationValidData = 1;
    protected $phoneNumberValidData = 564654531352;
    protected $companyEmailValidData = "roshan@mailinator.com";
    protected $websiteURLValidData = "http://rockerstech.com/thecrowd/api/web/build_company/save_additional";
    protected $dateValidData = "2017-03";
    protected $eventValidData = "event 2";
    protected $pressAticlesValidData = "http://rockerstech.com/thecrowd/api/web/build_company/save_additional";
    protected $authorizedBehalfCompanyValidData = 1;
    protected $roleInCompanyValidData = "dev";
    protected $legalNameValidData = "Test Roshan";
    protected $currencyCodeIDValidData=18;
    protected $companyFacebookURLValidData="https://www.facebook.com/test";
    protected $companyTwitterURLValidData="https://www.twitter.com/test";
    protected $companyLinkedinURLValidData="https://www.linkedin.com/test";
    protected $companyInstagramURLValidData="https://www.instagram.com/test";
    protected $googleAnalyticCodeValidData="test12345468test25468";
    protected $facebookPixelIDValidData="test123456";
    protected $videoURLValidData="https://www.youtube.com/watch?v=E5yFcdPAGv0";
    protected $generalErrorMessage =  "Request parameter is missing.";
    protected $invalidTokenMessage = "There is some problem please try again.";
    protected $errorCodeForInvalidToken = 2013;

    /**
     * @return string
     */
    public function getGeneralErrorMessage()
    {
        return $this->generalErrorMessage;
    }

    /**
     * @return string
     */
    public function getInvalidTokenMessage()
    {
        return $this->invalidTokenMessage;
    }

    /**
     * @return int
     */
    public function getErrorCodeForInvalidToken()
    {
        return $this->errorCodeForInvalidToken;
    }

    /**
     * @return int
     */
    public function getCurrencyCodeIDValidData()
    {
        return $this->currencyCodeIDValidData;
    }

    /**
     * @return string
     */
    public function getCompanyFacebookURLValidData()
    {
        return $this->companyFacebookURLValidData;
    }

    /**
     * @return string
     */
    public function getCompanyTwitterURLValidData()
    {
        return $this->companyTwitterURLValidData;
    }

    /**
     * @return string
     */
    public function getCompanyLinkedinURLValidData()
    {
        return $this->companyLinkedinURLValidData;
    }

    /**
     * @return string
     */
    public function getCompanyInstagramURLValidData()
    {
        return $this->companyInstagramURLValidData;
    }

    /**
     * @return string
     */
    public function getGoogleAnalyticCodeValidData()
    {
        return $this->googleAnalyticCodeValidData;
    }

    /**
     * @return string
     */
    public function getFacebookPixelIDValidData()
    {
        return $this->facebookPixelIDValidData;
    }

    /**
     * @return string
     */
    public function getVideoURLValidData()
    {
        return $this->videoURLValidData;
    }

    /**
     * @return int
     */
    public function getAdditionalPublicInformationValidData()
    {
        return $this->additionalPublicInformationValidData;
    }

    /**
     * @return string
     */
    public function getAddressValidData()
    {
        return $this->addressValidData;
    }

    /**
     * @return string
     */
    public function getStateValidData()
    {
        return $this->stateValidData;
    }

    /**
     * @return string
     */
    public function getCityValidData()
    {
        return $this->cityValidData;
    }

    /**
     * @return int
     */
    public function getZipCodeValidData()
    {
        return $this->zipCodeValidData;
    }

    /**
     * @return string
     */
    public function getCountryValidData()
    {
        return $this->countryValidData;
    }

    /**
     * @return string
     */
    public function getTimezoneValidData()
    {
        return $this->timezoneValidData;
    }

    /**
     * @return string
     */
    public function getHeadquaterCountryValidData()
    {
        return $this->headquaterCountryValidData;
    }

    /**
     * @return int
     */
    public function getJurisdictionIncorporationValidData()
    {
        return $this->jurisdictionIncorporationValidData;
    }

    /**
     * @return int
     */
    public function getPhoneNumberValidData()
    {
        return $this->phoneNumberValidData;
    }

    /**
     * @return string
     */
    public function getCompanyEmailValidData()
    {
        return $this->companyEmailValidData;
    }

    /**
     * @return string
     */
    public function getWebsiteURLValidData()
    {
        return $this->websiteURLValidData;
    }

    /**
     * @return string
     */
    public function getDateValidData()
    {
        return $this->dateValidData;
    }

    /**
     * @return string
     */
    public function getEventValidData()
    {
        return $this->eventValidData;
    }

    /**
     * @return string
     */
    public function getPressAticlesValidData()
    {
        return $this->pressAticlesValidData;
    }

    /**
     * @return int
     */
    public function getAuthorizedBehalfCompanyValidData()
    {
        return $this->authorizedBehalfCompanyValidData;
    }

    /**
     * @return string
     */
    public function getRoleInCompanyValidData()
    {
        return $this->roleInCompanyValidData;
    }

    /**
     * @return string
     */
    public function getLegalNameValidData()
    {
        return $this->legalNameValidData;
    }

    /**
     * @return string
     */
    public function getValidImagePath()
    {
        return $this->validImagePath;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
}