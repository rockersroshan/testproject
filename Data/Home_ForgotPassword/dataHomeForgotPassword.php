<?php
/*********************************************************************************
 * This is Crowd Funding Platform by fundraisingscript.com a part of Rockers Technology Inc. is paid software. It is released under the terms of
 * the following BSD License.
 *
 *   Rockers Technology Inc(Head Office)
 *   53 cedar st apt 3416
 *   Woburn, MA- 01801, USA
 *   E-mail Address : nishu@rockersinfo.com
 *
 * Copyright@2012-2020 by Rockers Technology Inc a domestic profit corporation has been
 * duly incorporated under
 * the laws of the state of Georgia , USA. www.rockersinfo.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

class dataHomeForgotPassword
{
    protected $validRegisterEmailAddress = "roshan@mailinator.com";
    protected $notValidEmailAddress = "roshan";
    protected $notRegisterEmailAddress = "roshan123456789784655186121@mailinator.com";
    protected $invalidTokenMessage = "There is some problem please try again.";
    protected $invalidTokenAndInvalidEmailAddressMessage = "There is some problem please try again.";
    protected $notValidEmailAddressMessage = "<p>The Email Address field must contain a valid email address.</p>";
    protected $validDataMessage = "";
    protected $notRegisterEmailAddressMessage = "<p>Email Address Not Found.</p>";
    protected $errorCodeWhenInvalidToken = 2013;
    protected $errorCodeWhenInvalidTokenAndEmailAddress = 2013;

    /**
     * @return int
     */
    public function getErrorCodeWhenInvalidTokenAndEmailAddress()
    {
        return $this->errorCodeWhenInvalidTokenAndEmailAddress;
    }

    function __construct()
    {
    }

    /**
     * @return string
     */
    public function getInvalidTokenAndInvalidEmailAddressMessage()
    {
        return $this->invalidTokenAndInvalidEmailAddressMessage;
    }

    /**
     * @return string
     */
    public function getNotRegisterEmailAddressMessage()
    {
        return $this->notRegisterEmailAddressMessage;
    }

    /**
     * @return string
     */
    public function getNotRegisterEmailAddress()
    {
        return $this->notRegisterEmailAddress;
    }

    /**
     * @return string
     */
    public function getNotValidEmailAddressMessage()
    {
        return $this->notValidEmailAddressMessage;
    }

    /**
     * @return string
     */
    public function getNotValidEmailAddress()
    {
        return $this->notValidEmailAddress;
    }

    /**
     * @return string
     */
    public function getNotValidRegisterEmailAddress()
    {
        return $this->notValidEmailAddress;
    }

    /**
     * @return int
     */
    public function getErrorCodeWhenInvalidToken()
    {
        return $this->errorCodeWhenInvalidToken;
    }

    /**
     * @return string
     */
    public function getInvalidTokenMessage()
    {
        return $this->invalidTokenMessage;
    }

    /**
     * @return string
     */
    public function getValidDataMessage()
    {
        return $this->validDataMessage;
    }

    /**
     * @return string
     */
    public function getValidRegisterEmailAddress()
    {
        echo "\nRegister email Address : " . $this->validRegisterEmailAddress;
        return $this->validRegisterEmailAddress;
    }
}